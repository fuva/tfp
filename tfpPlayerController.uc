/**
 * tfpPlayerController.uc
 */
 class tfpPlayerController extends UTPlayerController
	config(TFP);

//================================================================================================================================================
// BEGIN VARIABLES
//================================================================================================================================================

struct ErrorInfo
{
	var string Text;
	var float Life; // time to stay on screen, -1 = forever.
};

var array<ErrorInfo> OnScreenErrors;

// Better handling of fixed view & support for a tracked view.
var bool bFixedView, bFixedTracking;

// The controller is no longer bound to the location or view of the pawn.
var bool bDetachedView;
var Pawn PreDetachedPawn;

// Keep track of what we want to pickup.
var Actor CurrentPickup;

var string ClientChar;



var Sapitu sapitu; // character saving and loading manager

var SV_ItemDB ItemDB; // Item,buildings etc database loader and saving
var SV_ItemArchetype ItemToDrop;

/**
 * The current character
 */
var SapituCharacter char;


/*
==============
Constant LEVEL UP- RPG- Variables go here
==============
*/

const MAX_LEVEL = 80;
const XP_INCREMENT = 700;


/*
==================
QTE / Quick Time Events

==================
*/

struct QTE
{
    var name QTEName;
	var float Duration;
    var name KeyToPress;
	var int IconIndex;
	var bool Hold; // should we hold a key i nthis QTE?
	var bool Tap; // is this QTE tap-based
};

var array<QTE> QTEActions;
var name QTEToPress;
var bool bHoldQTE;


/*
=================
Player Sounds , Sound Notifications etc

==================
*/

var array<SoundCue> ThirstySounds;
var array<SoundCue> HungrySounds;
var array<SoundCue> SleepySounds;


/*
===================

POST PROCESS Interpolator

=====================
*/

var float CurValue;
var float InterpTo;
var bool bShouldInterpolate;
var MaterialEffect MF;
var MaterialInstanceConstant EffectMat;
var name CurEffect;


// Special variable , just to initiate player stats after he is ingame

var int InitCount;

// replication variables
var bool bUpdateClientHUD;

var repnotify int RepInt;

/*
===========
Misc helper stuff 

============
*/
var tfpPawn P;


/*
==============
Player replication info class

===============
*/

var SVPlayerReplicationInfo SVPRIS;

/*
===============
Kick values

=================
*/

var bool bDoingKick;
var name KickSock;


/*
===========

building stuffp
=============
*/

var repnotify SV_BuildingArchetype CurrentBuild;
var float Distance; // distacen between player and the structure
var bool bIsBuilding;


//================================================================================================================================================
// END VARIABLES
//================================================================================================================================================



/*
==========================
BEGIN HELPER FUNCTIONS!

============================
*/

function vector GetItemSpawnLoc()
{

local vector StartLoc,EndLoc;
local vector CamLoc;
local rotator CamRot;

 GetPlayerViewPoint(CamLoc,CamRot); // get the point where we will drop the item

  StartLoc = Pawn.Location;
  StartLoc.Z = CamLoc.Z;
  EndLoc = StartLoc + 100 *vector(CamRot);
  return EndLoc;

}

function vector GetBuildSpawnLoc()
{

local vector StartLoc,EndLoc;
local vector CamLoc;
local rotator CamRot;

GetPlayerViewPoint(CamLoc,CamRot); // get the point where we will drop the item

  StartLoc = Pawn.Location;
  StartLoc.Z = CamLoc.Z;
  EndLoc = StartLoc + 100 *vector(CamRot);
  return EndLoc;



}




/*
ServerMove
	KRIS - Slight update & clean up to support different swimming.
*/
unreliable server function ServerMove(float TimeStamp, vector InAccel, vector ClientLoc, byte MoveFlags, byte ClientRoll, int View)
{
	local float DeltaTime;
	local rotator DeltaRot, Rot, ViewRot;
	local vector Accel;
	local int maxPitch, ViewPitch, ViewYaw;

	// If this move is outdated, discard it.
	if (CurrentTimeStamp >= TimeStamp)
	{
		return;
	}

	if (AcknowledgedPawn != Pawn)
	{
		InAccel = vect(0,0,0);
		GivePawn(Pawn);
	}

	// View components
	ViewPitch	= (View & 65535);
	ViewYaw		= (View >> 16);

	// Acceleration was scaled by 10x for replication, to keep more precision since vectors are rounded for replication
	Accel = InAccel * 0.1;
	// Save move parameters.
	DeltaTime = GetServerMoveDeltaTime(TimeStamp);

	CurrentTimeStamp = TimeStamp;
	ServerTimeStamp = WorldInfo.TimeSeconds;
	ViewRot.Pitch = ViewPitch;
	ViewRot.Yaw = ViewYaw;
	ViewRot.Roll = 0;

	if (InAccel != vect(0,0,0))
		LastActiveTime = WorldInfo.TimeSeconds;

	SetRotation(ViewRot);

	if (AcknowledgedPawn != Pawn)
		return;

	if (Pawn != None)
	{
		Rot.Roll	= 256 * ClientRoll;
		Rot.Yaw		= ViewYaw;

		// Unless we're flying, 0 out the pitch.
		if (Pawn.Physics == PHYS_Flying)
		{
			maxPitch = 2;
			if ((ViewPitch > maxPitch * Pawn.MaxPitchLimit) && (ViewPitch < 65536 - maxPitch * Pawn.MaxPitchLimit))
			{
				if (ViewPitch < 32768)
					Rot.Pitch = maxPitch * Pawn.MaxPitchLimit;
				else
					Rot.Pitch = 65536 - maxPitch * Pawn.MaxPitchLimit;
			}
		}
			
		DeltaRot = (Rotation - Rot);
		Pawn.FaceRotation(Rot, DeltaTime);
	}

	// Perform actual movement
	if ((WorldInfo.Pauser == None) && (DeltaTime > 0))
		MoveAutonomous(DeltaTime, MoveFlags, Accel, DeltaRot);

	ServerMoveHandleClientError(TimeStamp, Accel, ClientLoc);
	//`log("Server moved stamp "$TimeStamp$" location "$Pawn.Location$" Acceleration "$Pawn.Acceleration$" Velocity "$Pawn.Velocity);
}

// include it in the replication block so it’s caught in the ReplicatedEvent function


replication
{
   // Things the server should send to the client.
  

      if(Role == Role_Authority) // if we are server
      	clientChar,RepInt,CurrentBuild; // send character


      
    
}



simulated event PostBeginPlay()
{
	super.PostBeginPlay();
	sapitu = tfpGame(WorldInfo.Game).sapitu;
	RepInt = 10; // the value we will replicate is 10
	ItemDB = tfpGame(WorldInfo.Game).DB;
	CalculateLevelProgress();
	SetTimer(10,true,'MemoryTimer');
}




function BeginGame()
{
//createChar(" i am not xarxor0");
//InitNeeds();
P = tfpPawn(Pawn);

}





/*
======================== BEGIN SERVER FUNCTIONS =================================
*/




exec function CCreateChar(string Character)
{
	local SVPlayerReplicationInfo SVPRI;
	SVPRI = SVPlayerReplicationInfo(PlayerReplicationInfo);

		if (Role < Role_Authority) // if client
		{

        SVPRI.char.CharacterName = Character;
        SVPRI.char.XP = 0;
        SVPRI.char.Level = 1;
// basic stats

       SVPRI.char.Stamina = 100;
       SVPRI.char.Hunger = 100;
       SVPRI.char.HungerRate = 10;
       SVPRI.char.Thirst = 100;
       SVPRI.char.ThirstRate = 10;
       SVPRI.char.Energy = 100;
       SVPRI.char.EnergyRate = 100;

       SVPRI.LogChar();
       SVPRIS = SVPRI;
        InitNeeds(); // get our needs up 

		}
		else
		{
			// we are server
			char = sapitu.createCharacter(Character);
	        char.CharacterName = Character;
	        char.level = 1;
	        char.XP = 0;
	        char.Hunger = 100;
	        char.Thirst = 100;
	        char.Energy = 100;
	        char.MaxHunger = 100;
	        char.MaxThirst = 100;
	        char.MaxEnergy = 100;
	        char.HungerRate = 5;
	        char.ThirstRate = 5;
	        char.EnergyRate = 5;
	        char.Stamina = 100;
	        char.MaxStamina = 100;
		}

  	

}

simulated exec function GetCharInfo()
{
	`log(" character name is "$SVPlayerReplicationInfo(PlayerReplicationInfo).Char.characterName);
}


/*
=========================================== END SERVER FUNCTIONS =====================================================
*/


/**
 * Create a new character
 */
exec function SapituCharacter createChar(string charName)
{
	local SapituCharacter returnTo;
	if (len(charName) == 0)
	{
		TeamMessage(none, "The character must have a name", 'none');
		return none;
	}
	if (char != none)
	{
		TeamMessage(none, "Discarding previous character: "$char.CharacterName$" (id:"$char.Name$")", 'none');
	}
	char = sapitu.createCharacter(charName);
	char.CharacterName = charName;
	char.level = 1;
	char.XP = 0;
	char.Hunger = 100;
	char.Thirst = 100;
	char.Energy = 100;
	char.MaxHunger = 100;
	char.MaxThirst = 100;
	char.MaxEnergy = 100;
	char.HungerRate = 5;
	char.ThirstRate = 5;
	char.EnergyRate = 5;
	char.Stamina = 100;
	char.MaxStamina = 100;
	returnTo = char;

	TeamMessage(none, "New character created", 'none');
	
	return returnTo;
	
}



/**
 * Save the current character
 */
exec function saveChar()
{
	if (char != none)
	{
		char.save();
		TeamMessage(none, "Current character saved", 'none');
	}
}

/**
 * Load a given character
 */
exec function loadChar(String charId)
{
	if (len(charId) == 0)
	{
		TeamMessage(none, "No character id given", 'none');
		return;
	}
	if (char != none)
	{
		TeamMessage(none, "Discarding previous character: "$char.CharacterName$" (id:"$char.Name$")", 'none');
	}
	char = sapitu.loadCharacter(charId);
	if (char == none)
	{
		TeamMessage(none, "No character found with id: "$charId, 'none');
	}
	else {
		TeamMessage(none, "Character loaded", 'none');
	
	}
}



/* =============================== END CHARACTER LOADING AND SAVING  ==============================*/


/*
============

XP SYSTEM BEGINS HERE

============
*/

exec function gvp()
{
	givexp(300);
}

public function GiveXP(int amount)
{
local int Chance;

   if (Role < Role_Authority) // if client
   {
   	`log("CONTROLLER: GIVE CLIENT XP");
   GiveClientXP(Amount);
   }
   else

   if(char != none) 
   {
	char.XP += amount;

	CalculateLevelProgress();

	while (char.XPGatheredForNextLevel >= char.XPRequiredForNextLevel && char.Level < MAX_LEVEL)
	{
		Chance = Rand(4);
		char.Level++;
		
		// Recalculate level progress after leveling up
		CalculateLevelProgress();
		if(Chance == 1)
		{
			GiveNewBuild();
		}
	}
}
}

function GiveNewBuild();

 private function CalculateLevelProgress()
{
	local int xpToCurrentLevel; // Total amount of XP gathered with current and previous levels
	if(char != none)
	{
	xpToCurrentLevel = 0.5*char.Level*(char.Level-1)*XP_INCREMENT;
	char.XPGatheredForNextLevel = char.XP - xpToCurrentLevel;
	char.XPRequiredForNextLevel = char.Level * XP_INCREMENT;
    }
}

function GiveClientXP(int amountt)
{

	local svplayerreplicationinfo svp;

		`log("IM ON GIVECLINTXP");
		if(Role < Role_Authority)
	{
	svp = svplayerreplicationinfo(playerreplicationinfo);
	//SVPlayerReplicationInfo(PlayerReplicationInfo).GiveXP(amountt);
	svp.char.XP += amountt;
	svp.logxp();

	CalculateClientLevelProgress();

	while (svp.char.XPGatheredForNextLevel >= svp.char.XPRequiredForNextLevel && svp.char.Level < MAX_LEVEL)
	{
		svp.char.Level++;
		
		// Recalculate level progress after leveling up
		CalculateClientLevelProgress();
	}
}
}

function CalculateClientLevelProgress()
{
	local int xpToCurrentLevel;
	local svplayerreplicationinfo svp;
	if(Role < Role_Authority)
	{
	svp = svplayerreplicationinfo(playerreplicationinfo);

	xpToCurrentLevel = 0.5*svp.char.Level*(svp.char.Level-1)*XP_INCREMENT;
	svp.char.XPGatheredForNextLevel = svp.char.XP - xpToCurrentLevel;
	svp.char.XPRequiredForNextLevel = svp.char.Level * XP_INCREMENT;
	svp.logxp();
    }
}



event PlayerTick(float DeltaTime)
{
	super.PlayerTick(DeltaTime);

    if(InitCount < 10) // special to initiate all the player stats
    {
        InitCount++;
        if(InitCount >= 10)
         BeginGame();
    }

   if(SVPlayerReplicationInfo(PlayerReplicationInfo).Char.CharacterName != "")
   {
   	if (Role < Role_Authority) // if client, update HIS hud
   	{
   		SVHudWrapper(MyHUD).HUDMovie.HUDTest();
   	}

   }


      if(bIsBuilding && CurrentBuild != none)
      {
      	  UpdateBuildPosition();
      }


   


}


/*
==============
Player Memory
=============
*/

exec simulated function MemoryTimer()
{
	local int Chance;
	local int Object;
	local int i;
	local SV_BuildingArchetype BuildToAdd;
	Chance = Rand(2);

	`log("netmode is "$WorldInfo.NetMode);


	 if(Chance == 1)
	 {
	 	 Object = Rand(2);
	 	 if(WorldInfo.NetMode == NM_Client)
	 	 {

	 	 }
	 	 else
	 	 BuildToAdd = ItemDB.GetStructureRand(Object);

	 	 
                 
                
                if(WorldInfo.NetMode != NM_ListenServer  && WorldInfo.NetMode != NM_Standalone) // if not SP or listen server then its client
                {
                
                     if(BuildToAdd.BuildName != "")
                 ClientMemoryReset(BuildToAdd);
                }
                
                  if(Role < Role_Authority ) // if client
	 	          {
	 	          	//return; // relevant part of the client was done alredy go on
	 	          }
	 	     else
	 	     {
	 	     	
	 	             if(BuildToAdd.BuildName != "")
	 	  			char.GiveBuild(BuildToAdd);
	 	  			`log("SERVER: Gave build in memory timer");
	 	  			return;
	 	  	}
	 	  	
	 
	}
}

unreliable client function ClientMemoryReset(SV_BuildingArchetype b)
{
	`log("CLIENT : Build To Add was"$b.BuildName);
	SVPlayerReplicationInfo(PlayerReplicationInfo).AddBuild(b);
}

/*
=====================

Player Needs

====================
*/

function InitNeeds()
{

	if (Role == Role_Authority) // if the server is goign to init nieeds
	{
	SetTimer(char.HungerRate,true,nameOf(DecHunger));
	SetTimer(char.ThirstRate,true,nameOf(DecThirst));
	SetTimer(char.EnergyRate,true,nameOf(DecEnergy));

	}

	   else
	   {
		
	
	   SetTimer(SVPRIS.char.HungerRate,true,nameOf(DecHunger));
	   SetTimer(SVPRIS.char.ThirstRate,true,nameOf(DecThirst));
	   SetTimer(SVPRIS.char.EnergyRate,true,nameOf(DecEnergy));
       }

}

function DecHunger()
{

    if (Role == Role_Authority) // if the server is goign to init nieeds
	{
	 char.Hunger -= 10;
	}
	else
	SVPRIS.char.Hunger -= 10;


}

function DecThirst()
{
	if (Role == Role_Authority) // if the server is goign to init nieeds
	{
	  char.Thirst -= 10;
	}
	else
      SVPRIS.char.Thirst -= 10;


}

function DecEnergy()
{
	if (Role == Role_Authority) // if the server is goign to init nieeds
	{
		char.Energy -= 10;
	}
	else
     SVPRIS.char.Energy -= 10;

}







/*
========================

Item dropping and giving

=======================
*/


exec function inv()
{
	SVHUDWRAPPER(MyHUD).OpenInventory();
}

exec function inv2()
{
	SVHUDWRAPPER(MyHUD).ToggleMouse(true);
}


function bool HasItem(string Item)
{
	local int i;

	   if (Role == Role_Authority) // if server loop trough local character
       {
       	  
       	  for (i = 0; i <char.inventory.length && i < 32; i++)
          	if(Item == char.inventory[i].itemName)
          	return true;

          	return false;


       }



}



reliable client function GiveItem(SV_ItemArchetype Actor)
{
	//SVPRIS.char.inventory.AddItem(Actor);
	SVPlayerReplicationInfo(PlayerReplicationInfo).char.inventory.AddItem(actor);
	SVPlayerReplicationInfo(PlayerReplicationInfo).loginv();
	char.inventory.additem(actor);

	
    //SVPlayerReplicationInfo(PlayerReplicationInfo).test(Actor);
  	
  	//test(itm);
}

exec function giveitem2()
{
	local sv_itemarchetype coco;
	coco = ItemDB.GetItem("Coconout");
	char.inventory.AddItem(coco);
}


function DropItem(string ToDrop)
{
 local int i;
 local vector StartLoc,EndLoc;
local vector CamLoc;
local rotator CamRot;

 GetPlayerViewPoint(CamLoc,CamRot); // get the point where we will drop the item

  StartLoc = Pawn.Location;
  StartLoc.Z = CamLoc.Z;
  EndLoc = StartLoc + 100 *vector(CamRot);

 if (Role == Role_Authority) // if server loop trough local character
 {

   for (i = 0; i <char.inventory.length && i < 32; i++)
   {

   	if(char.inventory[i].itemName == ToDrop) // make sure we have the item
   	{

      Spawn(class'SV_ItemArchetype', self,, EndLoc,,char.inventory[i]); 
      char.inventory.removeItem(char.inventory[i]);
      break; // no need to go further just return here
   	}
  }
}

   if(Role < Role_Authority) // in the other hand if we are a client
   {
	`log("client drop call");
	//ClientDrop(ToDrop);
	SVPlayerReplicationInfo(PlayerReplicationInfo).SendDropToServer(ToDrop);
   }

}


reliable server function ServerSpawnItem(string hm,svplayerreplicationinfo pri)
{
	local SV_ItemArchetype ToSpawn;
	local vector SpawnLoc;

	SpawnLoc = GetItemSpawnLoc();


	`log("value i received was"$hm);
	ToSpawn = ItemDB.GetItem(hm);
Spawn(class'SV_ItemArchetype',self,,SpawnLoc,,ToSpawn);

}


/*
===================
Items Eating,Using,Drinking and dropping

====================
*/

function DrinkItem(SV_ItemArchetype Item)
{

  char.Thirst += Item.OnUseAmount;
  Pawn.Mesh.AttachComponentToSocket(Item.StaticMeshComponent, 'Hand');
  P.TopHalfAnimSlot.PlayCustomAnim('DrinkItem',1.0,0.5,0.5,false,false);
  settimer(tfpPawn(Pawn).TopHalfAnimSlot.GetCustomAnimNodeSeq().GetTimeLeft(),false,'FinishDrink');
	//RemoveInventoryItem(item.itemName,1);
}

function FinishDrink();

function EatFood(SV_ItemArchetype Item)
{
  char.Hunger += Item.OnUseAmount;
  Pawn.Mesh.AttachComponentToSocket(Item.StaticMeshComponent, 'Hand');
  P.TopHalfAnimSlot.PlayCustomAnim('DrinkItem',1.0,0.5,0.5,false,false);
  settimer(tfpPawn(Pawn).TopHalfAnimSlot.GetCustomAnimNodeSeq().GetTimeLeft(),false,'FinishEat');

}

function FinishEat();



/*
==============

Buildings!

Building Giving,Spawning, and dropping?

=================
*/


exec function GiveBuildT(string Build)
{
	local sv_buildingarchetype t;
	
	if(Role == ROLE_Authority)
	{
		t = ItemDB.GetStructure(Build);
		SendStringToClient(t);
		`log("server string is" $Build);
	}

	//if (Role < Role_Authority)
	//{
		//SendStringToClient(t);
	//}
}

reliable client function SendStringToClient(sv_buildingarchetype p)
{
	`log("Value or thing in client is"$ p.buildname);
	svplayerreplicationinfo(playerreplicationinfo).char.StructureList.AddItem(p);
}

exec function GiveBuild(SV_BuildingArchetype Actor)
{
	SVPlayerReplicationInfo(PlayerReplicationInfo).char.StructureList.AddItem(actor);
	char.StructureList.AddItem(actor);
	`log("gave build succesffuly");
}

function IncreaseBuildDistance(float Val)
{
Distance += Val;
}

function DecreaseBuildDistance(float Val)
{
Distance -= Val;
}


exec function SpawnBuild(string Buildd)
{
local vector SpawnLoc;
local SV_BuildingArchetype BuildToSpawn;

BuildToSpawn = ItemDB.GetStructure(Buildd);

SpawnLoc = GetBuildSpawnLoc();

	if(Role == ROLE_Authority) // if we are the server we can use normal methds
	{

		if(HasItem(BuildToSpawn.recitems[BuildToSpawn.recitems.length].buildName))
		{
             
		 CurrentBuild = Spawn(class'SV_BuildingArchetype', self,, SpawnLoc,,BuildToSpawn); 
         bIsBuilding = true;
         `log("Went past has item");

		}
	}

	`log("Spawned build");

}

function UpdateBuildPosition()
{
	  local Vector BuildPos;
      BuildPos = Pawn.Location + Vector(pawn.rotation) * Distance;
      CurrentBuild.CheckLocation(BuildPos);
}

exec function StartFire(optional byte FireModeNum)
{
	if(bIsBuilding)
	{
      bIsBuilding = false;
      CurrentBuild.SetPlaced();
      CurrentBuild = None;

	}
	else
	{
		super.StartFire(FireModeNum);
	}

}



/*
BehindView
	Slight update to support more camera modes.
*/
exec function BehindView()
{
	if (WorldInfo.NetMode == NM_Standalone)
	{
		// will get set back to true below, if necessary
		bDebugFreeCam = False;
		SetBehindView(!bBehindView);
	}
}

/*
PrevWeapon
	Slight update to support free cam mode.
*/
exec function PrevWeapon()
{
	`log("prev weapon");
	if ((Vehicle(Pawn) != None) || (Pawn == None) || bDebugFreeCam)
		AdjustCameraScale(true);
	else if (!Pawn.IsInState('FeigningDeath'))
    
   // if(bIsBuilding == true)
    //{
    	//IncreaseBuildDistance(1);
    	//`log("prev weapon and is building");
    	//return;
   // }
		Super.PrevWeapon();

 `log("distance is "$Distance);
}

/*
NextWeapon
	Slight update to support free cam mode.
*/
exec function NextWeapon()
{
	`log("nweap");
	if ((Vehicle(Pawn) != None) || (Pawn == None) || bDebugFreeCam)
		AdjustCameraScale(false);
	else if (!Pawn.IsInState('FeigningDeath'))

	   // if(bIsBuilding == true)
      // {
       	//`log("nweap dec");
    	//DecreaseBuildDistance(1);
    	//return;
       //}
		Super.NextWeapon();
}

/*
SetCameraMode
	Cleaned up & more camera modes.
*/
function SetCameraMode(name NewCamMode)
{
	local tfpPawn P;
	local bool bWasFixedView, bWasFixedTracking, bWasBehindView;
	
	bWasFixedView = bFixedView;
	bWasFixedTracking = bFixedTracking;
	bWasBehindView = bBehindView;
	
	P = tfpPawn(Pawn);
	
	if (P == None)
	{
		Super.SetCameraMode(NewCamMode);
		return;
	}

	bDetachedView = ((NewCamMode == 'Detached') || (NewCamMode == 'Detach'));
	if (bDetachedView)
		GotoState('PlayerDetached');

	bFixedTracking = ((NewCamMode == 'FixedTracking') && !bDetachedView);
	bFixedView = (((NewCamMode == 'Fixed') || bFixedTracking) && !bDetachedView);
	if ((bWasFixedView != bFixedView) || (bWasFixedTracking != bFixedTracking))
		P.SetFixedView(bFixedView, bFixedTracking);

	bDebugFreeCam = (((NewCamMode == 'FreeCam') || (NewCamMode == 'Free')) && !bDetachedView);
	if (bDebugFreeCam)
		DebugFreeCamRot = Rotation;

	bBehindView = (((NewCamMode == 'ThirdPerson') || (NewCamMode == 'Third') || bDebugFreeCam) && !bDetachedView);
	if (bWasBehindView != bBehindView)
		SetBehindView(bBehindView);

	P.SetHeadVisibility();

	if (PlayerCamera != None)
		Super(UDKPlayerController).SetCameraMode(NewCamMode);
	
	ClientMessage("Camera '"$NewCamMode$"'");
	`log(GetEnum(enum'ENetMode', WorldInfo.NetMode) @ self @ GetFuncName() @ "NewCamMode:" @ NewCamMode);
}

/*
ValidPickup
	Since PickupFactories, DroppedPickups etc don't share and common parent class, 
	we need this function to check what they are and if they can currently be picked up.
*/
function bool ValidPickup(Actor Pickup)
{
    if (DroppedPickup(Pickup) != None)
		return (!DroppedPickup(Pickup).bFadeOut);
	else if (UTWeaponLocker(Pickup) != None)
		return false;
    else if (PickupFactory(Pickup) != None)
        return (PickupFactory(Pickup).bPickupHidden == false);
    else if (UTCarriedObject(Pickup) != None)
		return ((UTCarriedObject(Pickup).bHome == true) || (UTCarriedObject(Pickup).IsInState('Dropped')));
	else if(SV_ItemArchetype(Pickup) != none)
	return true;
		//else if (SV_ItemArchetype(Pickup) != none)
	//return true;

    // not a valid pickup
    return false;
}

/*
FindPickup
	Finds a pickup (dropped, factory etc) within a limited radius.
	Returns true if its valid.
*/
function bool FindPickup()
{
	local Actor Pickup, Best;
	local vector ViewDir, PawnLoc2D, PickupLoc2D;
	local float NewDot, BestDot;

    if (Pawn == None)
        return false;

    CurrentPickup = None;

    // Are we standing on one?
    if ((Pawn.Base != None) && ValidPickup(Pawn.Base))
	{
        CurrentPickup = Pawn.Base;
		return true;
	}

	// Pick best nearby item.
	PawnLoc2D = Pawn.Location;
	PawnLoc2D.Z = 0;
	ViewDir = vector(Pawn.Rotation);

	ForEach Pawn.OverlappingActors(class'Actor', Pickup, Pawn.VehicleCheckRadius)
	{
		//if(SV_ItemArchetype(Pickup) != none )
		//{

		//}
        if (ValidPickup(Pickup))
        {
            // Prefer items that Pawn is facing
			PickupLoc2D = Pickup.Location;
            PickupLoc2D.Z = 0;
            NewDot = Normal(PickupLoc2D-PawnLoc2D) Dot ViewDir;
            if ((Best == None) || (NewDot > BestDot))
            {
                // check that item is visible
                if (FastTrace(Pickup.Location, Pawn.Location))
                {
                    Best = Pickup;
                    BestDot = NewDot;
                }
            }
		}
	}

    // Remember what we found.
    CurrentPickup = Best;

    // If Best is not NULL, we found something.
	return (Best != None);
}

/*
PerformedUseAction
	Add pickup check and touch routine.
	Allows us to pickup item when we hit USE.
*/
simulated function bool PerformedUseAction()
{
	local UTCarriedObject Flag;
	local Actor Original_MoveTarget;


	// If the level is paused...
	if (WorldInfo.Pauser == PlayerReplicationInfo)
	{
		// ... unpause and move on.
		if (Role == Role_Authority)
			SetPause(false);
		return true;
	}

	// No pawn, can't use or feigning death? No can usey stuff.
	if ((Pawn == None) || Pawn.IsInState('FeigningDeath'))
		return true;

	// Have a weapon that is firing or can't be swapped? No can usey stuff.
	if ((Pawn.Weapon != None) && (Pawn.Weapon.IsFiring() || Pawn.Weapon.DenyClientWeaponSet()))
		return true;

	// Already picking something up.
	if (Pawn.IsTimerActive('PickupFinished'))
		return true;

	bJustFoundVehicle = false;

	if (Vehicle(Pawn) == None)
	{
		ForEach Pawn.TouchingActors(class'UTCarriedObject', Flag)
		{
			if (Flag.FlagUse(self))
				return true;
		}
	}

	// Below is only on server.
	if (Role < Role_Authority)
		return false;

	// Leave vehicle if currently in one.
	if (Vehicle(Pawn) != None)
		return Vehicle(Pawn).DriverLeave(false);

	// If we find a valid pickup, touch it.
	if (FindPickup())
	{
		Original_MoveTarget = MoveTarget;
		MoveTarget = CurrentPickup;
		//CurrentPickup.Touch(Pawn, None, CurrentPickup.Location, Normal(CurrentPickup.Location-Pawn.Location));
		if(CurrentPickup.IsA('SV_ItemArchetype') == true)
		{
			//char.inventory.AddItem(SV_ItemArchetype(CurrentPickup));
			GiveItem(SV_ItemArchetype(CurrentPickup));

			CurrentPickup.Destroy();
			//SVPRIS.logchar();
		}
		else
		CurrentPickup.Touch(Pawn, None, CurrentPickup.Location, Normal(CurrentPickup.Location-Pawn.Location));
			
		
		MoveTarget = Original_MoveTarget;
		return true;
	}

	// Try to find a vehicle to drive.
	if (FindVehicleToDrive())
		return true;

	// Try to interact with triggers.
	return TriggerInteracted();
}


/*
AddOnScreenError
*/
simulated function AddOnScreenError(string ErrorText, optional float Life = 10.0f)
{
	local ErrorInfo NewError;
	local int i;

	NewError.Text = ErrorText;
	if (Life == -1)
		NewError.Life = -1;
	else
		NewError.Life = WorldInfo.TimeSeconds + Life;

	for (i = 0; i < OnScreenErrors.length; i++)
	{
		if (OnScreenErrors[i].Text == NewError.Text)
		{
			OnScreenErrors[i].Life = NewError.Life;
			return;
		}
	}
	
	OnScreenErrors.AddItem(NewError);
}

/*
DrawOnScreenErrors
*/
simulated function DrawOnScreenErrors(HUD H)
{
	local Canvas Canvas;
	local float BestXL, CurYL, XL, YL;
	local int i;

	if (OnScreenErrors.length <= 0)
		return;

	Canvas = H.Canvas;

	Canvas.Font = H.GetFontSizeIndex(2);
	for (i = 0; i < OnScreenErrors.length && i < 32; i++)
	{
		Canvas.TextSize(OnScreenErrors[i].Text, XL, YL);
		CurYL += YL;
		if (XL > BestXL)
			BestXL = XL;
	}

	// Draw a rectangle behind it all.
	Canvas.SetPos(Canvas.ClipX - (BestXL + 8), 4);
	Canvas.DrawRect((BestXL + 8), (CurYL + 8));

	CurYL = 8;
	for (i = 0; i < OnScreenErrors.length && i < 32; i++)
	{
		Canvas.TextSize(OnScreenErrors[i].Text, XL, YL);
		Canvas.SetPos(Canvas.ClipX - (XL + 4), CurYL);
		CurYL += YL;
		Canvas.SetDrawColor(255, 255, 0, 255);
		Canvas.DrawText(OnScreenErrors[i].Text, FALSE);
	}

	for (i = 0; i < OnScreenErrors.length; i++)
	{
		if ((OnScreenErrors[i].Life != -1) && (OnScreenErrors[i].Life < WorldInfo.TimeSeconds))
			OnScreenErrors.RemoveItem(OnScreenErrors[i]);
	}
}

/*
AddSomeError
*/
exec function AddSomeError()
{
	AddOnScreenError("ERROR: Time is " $ WorldInfo.TimeSeconds);
}

/*
DrawHUD
	Added "USE" button display if a pickup is found nearby
	Added DrawOnScreenErrors.
*/
function DrawHUD( HUD H )
{
	local string FoundPickupText, KeyStr;
	local vector2d txtPos, fScale;
	local color txtColor;
	local class<Inventory> PickupItemClass;
	local string ItemName;

	DrawOnScreenErrors(H);
		
	Super.DrawHud(H);

	// If we find a pickup, display the "USE" button
	if (Pawn != None && IsLocalPlayerController())
	{
		// Have a weapon that is firing or can't be swapped? No can usey stuff.
		if ((Pawn.Weapon != None) && (Pawn.Weapon.IsFiring() || Pawn.Weapon.DenyClientWeaponSet()))
			return;
			
		// Already picking something up.
		if (Pawn.IsTimerActive('PickupFinished'))
			return;

		if (FindPickup())
		{
			//Get the fully localized version of the key binding
			BoundEventsStringDataStore.GetStringWithFieldName("GBA_Use", KeyStr);
			if (KeyStr == "")
				KeyStr = "<unbound>";

			if (CurrentPickup.IsA('UTDroppedPickup'))
				PickupItemClass = UTDroppedPickup(CurrentPickup).InventoryClass;
			else if (CurrentPickup.IsA('UTItemPickupFactory'))
				ItemName = UTItemPickupFactory(CurrentPickup).Default.PickupMessage;
			else if (CurrentPickup.IsA('PickupFactory'))
				PickupItemClass = PickupFactory(CurrentPickup).InventoryType;
			
			if (PickupItemClass != None)
				ItemName = PickupItemClass.Default.PickupMessage;
			
			if ((Pawn.Weapon != None) && (class<Weapon>(PickupItemClass) != None) && (Pawn.FindInventoryType(PickupItemClass) == None)
			 && (tfpInventoryManager(Pawn.InvManager) != None) && (tfpInventoryManager(Pawn.InvManager).WeaponCount() >= tfpInventoryManager(Pawn.InvManager).MaxWeaponCount)) 
				FoundPickupText = "Press '"$KeyStr$"' to swap "$Pawn.Weapon.ItemName$" for "$ItemName;
			else
				FoundPickupText = "Press '"$KeyStr$"' to pickup "$ItemName;
				
			txtPos.Y = H.Canvas.ClipY / 2;
			fScale.X = 1.0f;
			fScale.Y = 1.0f;
			txtColor.R = 255;
			txtColor.G = 255;
			txtColor.B = 0;
			txtColor.A = 255;
			H.DrawText(FoundPickupText, txtPos, H.GetFontSizeIndex(2), fScale, txtColor);
		}
	}
}


/*
Dead
	Removed forced third person on death.
*/
state Dead
{
	function BeginState(Name PreviousStateName)
	{
		local UTWeaponPickupFactory WF;

		LastAutoObjective = None;

		Super(UDKPlayerController).BeginState(PreviousStateName);

		if ( LocalPlayer(Player) != None )
		{
			ForEach WorldInfo.AllNavigationPoints(class'UTWeaponPickupFactory',WF)
				WF.NotifyLocalPlayerDead(self);
		}

		if ((Role == ROLE_Authority) && (UTGame(WorldInfo.Game) != None) && UTGame(WorldInfo.Game).ForceRespawn())
			SetTimer(MinRespawnDelay, true, 'DoForcedRespawn');
	}
	
	function EndState(name NextStateName)
	{
		Super.EndState(NextStateName);

		bDetachedView = false;
		bFixedView = false;
		bFixedTracking = false;
	}

	exec function StopFire(optional byte FireModeNum){}
}

/*
RoundEnded
*/
state RoundEnded
{
	exec function StopFire(optional byte FireModeNum){}
}

/*
PlayerSwimming
	KRIS - updated to fix swimming issues and support TFP better.
*/
state PlayerSwimming
{
ignores SeePlayer, HearNoise, Bump;
	/*
	NotifyPhysicsVolumeChange
		Since we no longer leave the water volume unless we're jumping out, 
		I moved jumping out of the water mechanics to the pawn.
		See tfpPawn::UpdateWaterLevel().
	*/
	event NotifyPhysicsVolumeChange( PhysicsVolume NewVolume )
	{
		if (!Pawn.bCollideActors)
			GotoState(Pawn.LandMovementState);

		if (Pawn.Physics != PHYS_RigidBody)
		{
			if (!Pawn.bUpAndOut)
				Pawn.SetPhysics(PHYS_Swimming);
		}
		else if (!NewVolume.bWaterVolume)
		{
			// if in rigid body, go to appropriate state, but don't modify pawn physics
			GotoState(Pawn.LandMovementState);
		}
	}

	/*
	ProcessMove
		The pawns pitch is no longer set when swimming, so make sure the remote pitch is set.
		FIXME - Figure out how to match the older style later?
	*/
	function ProcessMove(float DeltaTime, vector NewAccel, eDoubleClickDir DoubleClickMove, rotator DeltaRot)
	{
		if (Pawn == None)
			return;

		// Update ViewPitch for remote clients
		if (Role == ROLE_Authority)
			Pawn.SetRemoteViewPitch( Rotation.Pitch );

		Pawn.Acceleration = NewAccel;
	}

	/*
	PlayerMove
		Updated to new swimming.
	*/
	function PlayerMove(float DeltaTime)
	{
		local rotator oldRotation;
		local vector X,Y,Z, NewAccel;

		if (Pawn == None)
		{
			GotoState('Dead');
		}
		else
		{
			GetAxes(Rotation,X,Y,Z);

			NewAccel = PlayerInput.aForward * X + PlayerInput.aStrafe * Y + PlayerInput.aUp * vect(0,0,1);
			NewAccel = Pawn.AccelRate * Normal(NewAccel);
			
			if (tfpPawn(Pawn) != None)
			{
				// If we're at the water level and stil trying to move up,
				// don't let us, but remember that we wanted too.
				if (tfpPawn(Pawn).bAtWaterLevel && (NewAccel.Z > 0.0f))
				{
					NewAccel.Z = 0.0f;
					tfpPawn(Pawn).bJumpAtWaterLevel = true;
				}
				else
					tfpPawn(Pawn).bJumpAtWaterLevel = false;
			}	
			
			// Update rotation.
			oldRotation = Rotation;
			UpdateRotation(DeltaTime);

			if (Role < ROLE_Authority) // then save this move and replicate it
				ReplicateMove(DeltaTime, NewAccel, DCLICK_None, OldRotation - Rotation);
			else
				ProcessMove(DeltaTime, NewAccel, DCLICK_None, OldRotation - Rotation);
			bPressedJump = false;
		}
	}

	event BeginState(Name PreviousStateName)
	{
		if (Pawn.Physics != PHYS_RigidBody)
			Pawn.SetPhysics(PHYS_Swimming);
	}

Begin:
}

/*
PlayerDetached
	New 'camera' mode, for lack of a bette term.
	Lets you ghost around the level, quickly view other players/bots and return to you body if need be.
*/
state PlayerDetached extends BaseSpectating
{
	function SetCameraMode(name NewCamMode)
	{
		if ((NewCamMode == 'Detached') || (NewCamMode == 'Detach'))
			return;
			
		StopAltFire();
		Global.SetCameraMode(NewCamMode);
	}
	
	function ViewAPlayer(int dir)
	{
		local vector CurLoc;
		local Rotator CurRot;
		local PlayerReplicationInfo PRI;

		PRI = GetNextViewablePlayer(dir);

		if ( PRI != None )
		{
			SetViewTarget(PRI);
			
			if ((Controller(PRI.Owner) != None) && (Controller(PRI.Owner).Pawn != None))
			{
				Controller(PRI.Owner).Pawn.GetActorEyesViewPoint(CurLoc, CurRot);
				SetLocation(CurLoc);
				CurRot.Roll = 0;
				SetRotation(CurRot);
			}
		}
	}

	simulated event GetPlayerViewPoint( out vector out_Location, out Rotator out_Rotation )
	{
		out_Location = Location;
		out_Rotation = Rotation;
	}

	exec function StartFire( optional byte FireModeNum )
	{
		ServerViewObjective();
	}
	
	exec function StartAltFire( optional byte FireModeNum )
	{
	}
	
	exec function StopAltFire( optional byte FireModeNum )
	{
		ServerViewSelf();
		if (PreDetachedPawn != None)
		{
			if (PreDetachedPawn.bTearOff || (PreDetachedPawn.Health <= 0))
			{
				if (!WorldInfo.Game.PlayerCanRestart( Self ) )
					return;
		
				WorldInfo.Game.RestartPlayer(Self);
			}
			else
				Possess(PreDetachedPawn, false);
		}
	}
	
	exec function PrevWeapon()
	{
		ServerViewPrevPlayer();
	}

	exec function NextWeapon()
	{
		ServerViewNextPlayer();
	}
	
	function BeginState(Name PreviousStateName)
	{
		local vector CurLoc;
		local Rotator CurRot;

		bDetachedView = true;
		bCollideWorld = false;
		
		Global.GetPlayerViewPoint(CurLoc, CurRot);

		if (Pawn != None)
		{
			PreDetachedPawn = Pawn;
			Pawn.Acceleration = vect(0,0,0);

			// Update ViewPitch for remote clients
			if (Role == ROLE_Authority)
				Pawn.SetRemoteViewPitch(CurRot.Pitch);

			UnPossess();
		}
		
		SetLocation(CurLoc);
		CurRot.Roll = 0;
		SetRotation(CurRot);
	}
}

/*
DisplayDebug
	Updated to display FaceAPI info, if desired.
*/
simulated function DisplayDebug(HUD HUD, out float out_YL, out float out_YPos)
{
	Super.DisplayDebug(HUD, out_YL, out_YPos);
	
	
}


/*
===============
Kick attaaack

===============
*/


exec function GBA_StartKick()
{
	local float Timer;

 if(bDoingKick == false) // only start an kick IF we can
 {
   SetTimer(0.5,false,'KickTrace');
  tfpPawn(Pawn).FullBodyAnimSlot.PlayCustomAnim('Kick',1.0,0.1,0.1,false,false);
  Timer = tfpPawn(Pawn).FullBodyAnimSlot.GetCustomAnimNodeSeq().GetTimeLeft();
    bDoingKick = true;
   // TakeStamina(30);
	IgnoreMoveInput(true); // Dont allow the player to move while kicking
 }

 settimer(Timer - 0.1,false,'KickFinished');
 
}

function KickTrace()
{

local Vector StartLoc2,EndLoc,HitLocation,HitNormal,impulse;
local actor Result;
local tfppawn hitpawn;
local int fallchance;

fallchance = rand(8);

 Pawn.Mesh.GetSocketWorldLocationAndRotation(KickSock, StartLoc2); // get start and end locations for the trace
 Pawn.Mesh.GetSocketWorldLocationAndRotation('R_JB_End', EndLoc);
 
  Result = Trace(HitLocation, HitNormal, StartLoc2, EndLoc,true,,,TRACEFLAG_Bullet);
  DrawDebugLine(StartLoc2, EndLoc,255,0,0,true);
  hitpawn = tfpPawn(result);


  
   if(Result != none )
   {
   	impulse = Vector(Pawn.Rotation) * 800.0;
    Result.TakeDamage( 10,self,HitLocation,HitNormal,class'DamageType',,);
    Result.Velocity += HitNormal * 800.0f;
      

	//Result.CollisionComponent.AddImpulse(Startloc2,HitLocation);
	
   `log("hit actor is"$Result);
   
   }
   
   if(hitpawn != none && fallChance == 1)
   {
   impulse = Vector(Pawn.Rotation) * 600.0;
   hitpawn.turnragdollon();
   Hitpawn.Mesh.AddImpulse(impulse,EndLoc);
   }
}


function KickFinished()
{
bDoingKick = false;
IgnoreMoveInput(false);  // once the kick is finished restore movement
}



defaultproperties
{
	Name="Default__tfpPlayerController"
	MatineeCameraClass=class'Engine.Camera'
	CameraClass=None
	InputClass=class'TFP.tfpPlayerInput'
	CheatClass=class'tfpCheatManager'
	RemoteRole = ROLE_AutonomousProxy
	  KickSock = R_JB

	  Distance = 200;
	 bIsBuilding = false;


}