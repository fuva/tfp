class MeleeWeaponBase extends tfpWeapon;

/**
 * Normal Sword Attack values
 */

var float SwordAttackHitTime[3]; // Start time for tracing and hurting after an attack started
var float SwordAttackDuration[3]; // Duration of a sword attack


var float AttackHitDamage[3]; // Damage of a normal sword attack if it hits

/**
 * Heavy Attack values
 */
var name HeavyAttackAnim; // Name of the Heavy Attack animation

var float HeavyAttackChargeTime; // Time needed holding the attack button to charge a Heavy attack
var float HeavyAttackHitTime; // Start time for tracing and hurting after the Heavy attack started
var float HeavyAttackDuration; // Duration of the Heavy attack

var float HeavyAttackHitDamage; // Damage of the Heavy attack if it hits

var bool bHeavyAttack; // True if did a Heavy attack in the same click as click being released.

/**
 * Combo attack Values
 */
var float SwordCooldownTime; // Sword cooldown time after ending an attack
var float LastSwordAttackTime; // Time the player stopped attacking

var int MaxCombo; // Maximum combo hits
var int ComboIndex; // Index of the current combo

var float NextComboTime[2]; // Time until you can start queuing the next combo

var vector EndSockStart, EndSockEnd, MidSockStart, MidSockEnd, StartSockStart, StartSockEnd ;
var bool bSwinging;

var vector LastFrameStartSockEnd;
var vector LastFrameEndSockEnd;
var FLOAT numWeaponTraces;


var float AltWeaponDurability; // This is used for the shader
var int WeaponDurability;
var int WeaponDurabilityModifier;


var array<Actor> SwingHurtList;//Array of pawns that have been hit per swing

/*
=============
sound stuff
===========
*/
var AudioComponent SoundComponent;
var array<SoundCue> SwingSounds;


function bool CheckHurtList(Pawn newEntry)
{
		local int i;

		i = SwingHurtList.Find(newEntry);
		
		if(SwingHurtList[i] != none) // if the actor has been alredy hit
		{
		return false;
		}
		else
		SwingHurtList.AddItem(newEntry);
		return true;
		

		
}


simulated function Tick(float DeltaTime)
{
	local int i;
	local vector tracestart,traceend;


 if ( bSwinging )
 {
    tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.GetSocketWorldLocationAndRotation('EndControl', EndSockEnd,, );
	tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.GetSocketWorldLocationAndRotation('StartControl', StartSockEnd,, );  

	 if ( bHeavyAttack == true )
    {
    DoHeavyAttack();
    }
    else
    {
    	
    

	     for (i=0; i<numWeaponTraces; i++)
		{
			TraceStart = VLerp(StartSockEnd, EndSockEnd, i/numWeaponTraces);
			TraceEnd = VLerp(LastFrameStartSockEnd, LastFrameEndSockEnd, i/numWeaponTraces);
			DrawDebugLine(TraceStart, TraceEnd, 255,0,0, true);

			// now do the trace

		}

		LastFrameStartSockEnd = StartSockEnd;
		LastFrameEndSockEnd = EndSockEnd;
    }
   

    
 }

}

// Shouldn't refire automatically, only register one attack push.
simulated function bool ShouldRefire()
{
	return false;
}

// Start trying to do a HeavyAttack(releasing attack will result in a normal attack if possible)
simulated function FireAmmunition()
{
	local float ChargeTime;

	// Can't do a Heavy attack if pressing secondary attack or if at final combo
	if(CurrentFireMode != 0 || ComboIndex == MaxCombo - 1)
		return;

	// Stop trying to attack or else this will be called multiple times
	ClearPendingFire(CurrentFireMode);

	// Just set normal charge first
	ChargeTime = HeavyAttackChargeTime;

	// If we haven't passed the cooldown yet, increase the charge time
	if(WorldInfo.TimeSeconds - LastSwordAttackTime < SwordCooldownTime)
		ChargeTime += WorldInfo.TimeSeconds - LastSwordAttackTime;

	// Start up the Heavy attack
	SetTimer(ChargeTime,, nameOf(StartHeavyAttack));
}

// Start a Heavy attack
function StartHeavyAttack()
{
	// Play the animation

		tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.GetSocketWorldLocationAndRotation('EndControl', EndSockEnd,, );
		tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.GetSocketWorldLocationAndRotation('StartControl', StartSockEnd,, );  

		LastFrameStartSockEnd = StartSockEnd;
		LastFrameEndSockEnd = EndSockEnd;
    // Mark as having Heavyned, prevents normal attack from happening when attack key is released
	bHeavyAttack = true;

    bSwinging=true;




	// Set the end time
	SetTimer(HeavyAttackDuration,, nameOf(EndAttack));
}

// Apply damage for a Heavy attack
function DoHeavyAttack()
{
	
}

// Overriden to start a normal attack when the mouse is released(if allowed to)
simulated function EndFire(Byte FireModeNum)
{
	local name StartAttackName, EndAttackName;

	// Handle actual EndFire logic
	super.EndFire(FireModeNum);

	// Only primary attack is accepted
	if(FireModeNum != 0)
		return;

	// We're doing a normal attack, because we didn't hold attack long enough for a Heavy attack.
	ClearTimer(nameOf(StartHeavyAttack));

	// If we Heavyned in the same click before, we should'n do a normal attack
	if(bHeavyAttack)
	{
		// Reset Heavyned
		bHeavyAttack = false;
		return;
	}

	StartAttackName = nameOf(StartAttack);
	EndAttackName = nameOf(EndAttack);

	// If we're not in the last combo and not in attack start up then we can try doing an attack
	if(ComboIndex < MaxCombo - 1 && !IsTimerActive(StartAttackName))
	{
		// If there's a normal attack active
		if(IsTimerActive(EndAttackName))
		{
			// And the time since the timer was started has exceeded the next combo time, then we can start a new combo attack
			if(GetTimerCount(EndAttackName) >= NextComboTime[ComboIndex])
			{
				// Start up the timer first, queued to start when end hit should have been called
				SetTimer(GetTimerRate(EndAttackName) - GetTimerCount(EndAttackName),, StartAttackName);

				// Disable end attack, because we've got a new hit coming up
				ClearTimer(EndAttackName);

				// Start a new combo next
				ComboIndex++;
			}
		}
		else
		{
			// If we haven't passed the cooldown yet, queue up the attack
			if(WorldInfo.TimeSeconds - LastSwordAttackTime < SwordCooldownTime)
				SetTimer(WorldInfo.TimeSeconds - LastSwordAttackTime,, StartAttackName);
			else
				StartAttack(); // Start up an attack right away
		}
	}
}


function PlaySwingSound()
{

	//FleshHitsound[Rand(Fleshhitsound.Length)];
  SoundComponent.SoundCue =SwingSounds[Rand(SwingSounds.Length)];
 SoundComponent.FadeIn(0.2f,1.0f);
 tfpPlayerController(Instigator.Controller).PlayCameraAnim(CameraAnim'SV-CameraAnims.Melee.melee1',0.4,1.0,0.2,0.2,false,true);
  
   
}

// Start a normal attack
function StartAttack()
{
	local float AnimTime;
    // Play the animation

		tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.GetSocketWorldLocationAndRotation('EndControl', EndSockEnd,, );
		tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.GetSocketWorldLocationAndRotation('StartControl', StartSockEnd,, );  

		LastFrameStartSockEnd = StartSockEnd;
		LastFrameEndSockEnd = EndSockEnd;
    
	// Start moving during the attack

    // Set the bool, so tick() will know to track the blade socket(s)
    bSwinging = true;

    SwingHurtList.Remove(0,SwingHurtList.Length); // reset all the enemies we have hit


	if(ComboIndex == 0)
	{
	
		AnimTime = tfpPawn(Instigator).PlayMeleeAnim(0);
		PlaySwingSound();

	}

	if(ComboIndex == 1)
	{
		AnimTime = tfpPawn(Instigator).PlayMeleeAnim(1);
				PlaySwingSound();
	}

	// Set the end time
	SetTimer(AnimTime - 0.03,, nameOf(EndAttack));
}



// Apply damage for a normal attack
exec function DoAttack(vector Start,vector End)
{
 
	local Vector HitLocation, HitNormal;
	local Pawn HitActor;
	local PrimitiveComponent TmpComp;
	local TraceHitInfo HitInfo;
	local UTPhysicalMaterialProperty PhysicalProperty;
	local tfpplayercontroller PC;
	PC = tfpplayercontroller(Instigator.Controller);

	if(WeaponDurability == 0) // if our weapon has reached ti max durability aka its broken now
	{
	PC.Pawn.TossInventory(PC.Pawn.Weapon);
    }
	

	ForEach Worldinfo.AllPawns(class'Pawn', HitActor) //ForEach TraceComponent(class'Actor', HitActor, HitLocation, HitNormal, EndSockEnd, EndSockStart, Extent)
	{

    
       if(HitActor != none)
       {
    
	       if (HitActor != none)
	       {
            TmpComp = tfppawn(HitActor).Mesh;
           }

	 	
	if(TraceComponent(HitLocation, HitNormal,TmpComp, Start, End) == true)
	{
	//`Log("ON TRACE COMPONENT"$TmpComp);
		// Skip the pawn using the sword
		
		if(HitActor != Owner && CheckHurtList(HitActor) == true)	   
        {
		   //SoundComponent.SoundCue = FleshHitsound[Rand(Fleshhitsound.Length)];

		// Apply damage with a momentum direction from Instigator location to HitActor location
		Hitactor.TakeDamage( AttackHitDamage[ComboIndex], Instigator.Controller,
						HitActor.Location, Normal(HitActor.Location - Instigator.Location) * 50,
						InstantHitDamageTypes[CurrentFireMode],, self);
//						     ToggleHitEffects(HitLocation,HitActor);
						 takedurability(20);


						
	   }
	}
	
}
}
  
}

function TakeDurability(int Dur)
{
	
}




// End any ongoing attack
function EndAttack()
{
	bSwinging = false;



	// Reset combo
	ComboIndex = 0;

	// Mark the time we finished the attack
	LastSwordAttackTime = WorldInfo.TimeSeconds;
}

DefaultProperties
{


	Begin Object class=AnimNodeSequence Name=MeshSequenceA
		bCauseActorAnimEnd=true
	End Object



	SwordAttackAnim(0)=Swing
	SwordAttackAnim(1)=Swing2

	SwordAttackHitTime(0)=0.75f
	SwordAttackHitTime(1)=0.5f
	SwordAttackHitTime(2)=0.75f

	SwordAttackDuration(0)=1.f
	SwordAttackDuration(1)=1.f
	SwordAttackDuration(2)=1.f




	AttackHitDamage(0)=15.f
	AttackHitDamage(1)=25.f


	HeavyAttackAnim=HeavyAttack

	HeavyAttackChargeTime=0.75f
	HeavyAttackHitTime=0.6f
	HeavyAttackDuration=0.9f

	HeavyAttackHitDamage=25.f

	SwordCooldownTime=0.6f

	MaxCombo=2

	NextComboTime(0)=0.35f // First to second combo
	NextComboTime(1)=0.35f // Second to last combo

	FireInterval(0)=0.05f // Should be really fast to better register clicks for combo's

	InstantHitDamageTypes(0)=class'FFDmgType'
	ShotCost(0)=0

	MaxAmmoCount=1
	AmmoCount=1
}