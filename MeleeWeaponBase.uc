class MeleeWeaponBase extends tfpWeapon;


/*
=================
COMBO

Damage per combo,stamina etc

=================
*/

var float SwordAttackHitDamage[3]; // Damage the weapon will dead based in combo index 
var name SwordAttackAnim[3]; // Names of the animation to play based in the index
var float AttackStamina[3]; // how much stamina takes each attack is defined in that
var int MaxCombo; // Maximum combo hits
var int ComboIndex; // Index of the current combo
var array<Actor> SwingHurtList;//Array of pawns that have been hit per swing
var float SwordCooldownTime; // Sword cooldown time after ending an attack
var float LastSwordAttackTime; // Time the player stopped attacking
var float NextComboTime; // Time until you can start queuing the next combov
var bool CanGoToNextCombo;
var vector LastFrameStartSockEnd;
var vector LastFrameEndSockEnd;
var FLOAT numWeaponTraces;
var bool bCanProceedToCombo;



/*

==============
Weapon Stats
Durability,Strength 

===============
*/

var float Strength; // physical impact of the weapon and also impulse it adds into the enemy
var float MaxDurability;
var float AltWeaponDurability; // This is used for the shader
var int WeaponDurability;
var int WeaponDurabilityModifier;
var MaterialInstanceConstant WeaponMat;//weapon material
/*
=============
misc melee stuff
hiteffect
=============
*/
var ParticleSystem HitEffect [3]; // Particle system to play when hit someting
var CameraAnim SwingShake[3];

var int NumberOfTraces;



/*
=================
Weapon Profile
can cut, block, is heavy etc

==================
*/


var bool bCanCutTrees; // If the weapon can cut trees this variable must be true
var bool bCanAttack;
var bool bCanApplyDamage;
var bool bDrawTraces;
var bool bBeingWield;
var bool bCanBuild; // Can this weapon build structures? like the hammer
var bool bCanMine; // Can we mine rocks with this weapon?
var bool bFists;



/*
heavy attack
*/

var name EndAttackName;
var bool bOnHeavyAttack;

/*
=============
weapon specs

==========
*/
var bool WoodCutting; // are we wood cutting alredy?
var float AttackSpeed; // new speed
var float WCSpeed;
var int WoodCuttingFactor;



var vector EndSockStart, EndSockEnd, MidSockStart, MidSockEnd, StartSockStart, StartSockEnd,Interval1Start,Interval2Start,Interval3Start,Interval1End,Interval2End,Interval3End ;
var bool bSwinging;
/**
 * attachments
 */

var bool bCanTrace;
var bool bHitEnemy; 

var tfpPlayerController PC;



var(Animations) array<name> SwordAnims;
var tfpPawn tfpPawn;


/*
=============
sound stuff
===========
*/
var AudioComponent SoundComponent;
var array<SoundCue> SwingSounds;

/*
==========
Every surface has its own, name,sound,particle system AND decal
==========
*/

struct HitSoundInfo
{
	var name MaterialType;
	var SoundCue Sound;
	var ParticleSystem PS;
	var Material Decal;
	var int MaxSoundIndex; 
};


var array<HitSoundInfo> HitSounds;


struct EnemySoundInfo
{

	var SoundCue Sound;
	var ParticleSystem PS;
	var Material Decal;
};

var array<EnemySoundInfo> FleshHitSounds;


/*
==========
Blocking functionality
==========
*/

var bool Blocking;




/*
=========
TOGGLE HIT EFFECTS
Handles the hit sound and particle system creation
=========
*/



function PlayHitSounds(name MaterialHit,vector EffectLoc)
{
	local int soundindex;

	SoundIndex=HitSounds.Find('MaterialType', MaterialHit);

	//SoundComponent.SoundCue = HitSounds[SoundIndex].Sound;
	//SoundComponent.FadeIn(0.2,1.0);
	//SoundIndex=Rand(HitSounds.Length);

	PlaySound(HitSounds[SoundIndex].Sound);
	WorldInfo.MyEmitterPool.SpawnEmitter(HitSounds[SoundIndex].PS,EffectLoc,,,); // do the effects
}

function PlayPawnHitSounds(actor Enemy,vector Location)
{
	local int soundindex;

	SoundIndex=Rand(FleshHitSounds.Length);
	PlaySound(FleshHitSounds[SoundIndex].Sound);
	WorldInfo.MyEmitterPool.SpawnEmitter(HitSounds[SoundIndex].PS,Location,,,); // do the effects
}



function Material GetDecalToSpawn()
{

}


function PlaySwingSound()
{

	//FleshHitsound[Rand(Fleshhitsound.Length)];
  SoundComponent.SoundCue =SwingSounds[Rand(SwingSounds.Length)];
 SoundComponent.FadeIn(0.2f,1.0f);
 tfpPlayerController(Instigator.Controller).PlayCameraAnim(CameraAnim'SV-CameraAnims.Melee.melee1',0.4,1.0,0.2,0.2,false,true);
  
   
}


simulated function StartAltFire(byte FireModeNum)
{
	
		`log("ALT FIRE IS BEING PERFORMED ");
		ToggleBlock(true);
	

}

exec function StopAltFire( optional byte FireModeNum )
{
	
		ToggleBlock(false);
	
}


/*
=========
HELPER FUNCTIONS

============
*/



function CheckNewCombo() // called when we can go into a new attack
{
	NextComboTime = 0;

	//settimer(0.6,false,'ResetCombo');
}




exec function TakeDurability(float Amount)
{
AltWeaponDurability += Amount;

//WeaponDurability -= Amount; // take out the amount first

WeaponMat.SetScalarParameterValue('Durability',AltWeaponDurability/MaxDurability); // and now modify the shader
    `log("scalar parameter is "$AltWeaponDurability/MaxDurability);
	`log("AltWeaponDurability is "$AltWeaponDurability);


	 
	 if(WeaponDurability == 0)
	 {
	 tfpPlayerController(Instigator.Controller).Pawn.TossInventory(tfpPlayerController(Instigator.Controller).Pawn.Weapon);
	 
	 
	 }

}

/** END HELPER ** */


/*
========
Blocking FUNCTIONALITY
=========
*/

function ToggleBlock(bool val)
{
	local tfppawn p;
	p = tfppawn(instigator);

    if(val == true)
    {
      Blocking = true;
      p.PlaySlotAnims(p.TorsoAnimSlots, 'Block', 1.0, true, true, 0.1f, 0.1f);
    }


    if(val == false)
    {
     p.StopPlaySlotAnims(p.TorsoAnimSlots);
    }

}


simulated event PostBeginPlay()
{

super.postbeginplay();
PC = tfpPlayerController(Instigator.Controller);
}


function InitWeaponMaterial()
{
WeaponMat = new(None) Class'MaterialInstanceConstant';
WeaponMat.SetParent(tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.GetMaterial(0)); // set the parent
tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.SetMaterial(0,weaponmat);
WeaponMat = MaterialInstanceConstant(tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.GetMaterial(0)); // set the parent
}


function bool CheckHurtList(Pawn newEntry)
{
		local int i;

		i = SwingHurtList.Find(newEntry);
		
		if(SwingHurtList[i] != none) // if the actor has been alredy hit
		{
		return false;
		}
		else
		SwingHurtList.AddItem(newEntry);
		return true;
		

		
}

function bool CheckHurtList2(Actor newEntry)
{
		local int i;

		i = SwingHurtList.Find(newEntry);
		
		if(SwingHurtList[i] != none) // if the actor has been alredy hit
		{
		return false;
		}
		else
		SwingHurtList.AddItem(newEntry);
		return true;
			
}

simulated function Tick(float DeltaTime)
{
	local int i;
	local vector tracestart,traceend;




	Super.Tick(DeltaTime);

	// some other Tick() stuff


	if(bswinging)
	{

		tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.GetSocketWorldLocationAndRotation('EndControl', EndSockEnd,, );
		tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.GetSocketWorldLocationAndRotation('StartControl', StartSockEnd,, );  

		for (i=0; i<numWeaponTraces; i++)
		{
			TraceStart = VLerp(StartSockEnd, EndSockEnd, i/numWeaponTraces);
			TraceEnd = VLerp(LastFrameStartSockEnd, LastFrameEndSockEnd, i/numWeaponTraces);
			DrawDebugLine(TraceStart, TraceEnd, 255,0,0, true);

			// now do the trace
			DoAttack(TraceStart,TraceEnd); // The trace
		}

		LastFrameStartSockEnd = StartSockEnd;
		LastFrameEndSockEnd = EndSockEnd;
    }

}


// Shouldn't refire automatically, only register one attack push.
simulated function bool ShouldRefire()
{
	return false;
}

// Start trying to do a SpinAttack(releasing attack will result in a normal attack if possible)
simulated function FireAmmunition()
{

}

function PlayChargeAnim()
{
	
}






// Overriden to start a normal attack when the mouse is released(if allowed to)
simulated function EndFire(Byte FireModeNum)
{
   local name StartAttackName;
   super.EndFire(FireModeNum);
	StartAttackName = nameOf(StartAttack);
	EndAttackName = nameOf(EndAttack);
    bCanApplyDamage = true;
	
	





 if(Blocking == true)
 {
  return; // we cant attack while blocking
 }



   if(bOnHeavyAttack) // we cannot do anything until the heavy attack is done
   {
   	return;
   }
	
      else
      {


     
       
		if(IsTimerActive(EndAttackName) && ComboIndex == 1)
		{
		   if(bCanProceedTocOMBO)
		   {
		   	ClearTimer(EndAttackName);
		     ComboIndex--;
		     bCanProceedToCombo = false;
				
				StartAttack();
		   }
		}



	// If we're not in the last combo and not in attack start up then we can try doing an attack
	if(ComboIndex < MaxCombo) //--if(ComboIndex < MaxCombo - 1 && !IsTimerActive(StartAttackName))
	{
		// If there's a normal attack active
		if(IsTimerActive(EndAttackName))
		{
			// And the time since the first attack has exceed the next combo time then do a second combo
			if(bCanProceedToCombo) // true
			{
				// Start up the timer first, queued to start when end hit should have been called
				//SetTimer(GetTimerRate(EndAttackName) - GetTimerCount(EndAttackName),, StartAttackName);

				// Disable end attack, because we've got a new hit coming up
				ClearTimer(EndAttackName);
				// Start a new combo next
				ComboIndex++;
				StartAttack();
				bCanProceedToCombo = false;

					`Log("Melee: STARTED NEW ATTACK WITH INCREASED COMBO INDEX");
			}
		}
		
		
		
		else
		{
				StartAttack(); // Start up an attack right away	
		}
		
		
		
	
	}
}


`Log("combo index is"$ComboIndex);

}




/*
=========
Normal 2 combo attack
==========
*/

// Start a normal attack
exec function StartAttack()
{
local float AnimTime;
local int i;
CanGoToNextCombo = true;



		tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.GetSocketWorldLocationAndRotation('EndControl', EndSockEnd,, );
		tfpPawn(Instigator).CurrentWeaponAttachment.Mesh.GetSocketWorldLocationAndRotation('StartControl', StartSockEnd,, );  

		LastFrameStartSockEnd = StartSockEnd;
		LastFrameEndSockEnd = EndSockEnd;
    
   bSwinging = true;

    SwingHurtList.Remove(0,SwingHurtList.Length); // reset all the enemies we have hit


	if(ComboIndex == 0)
	{
	
		AnimTime = tfpPawn(Instigator).PlayMeleeAnim(0);
		PlaySwingSound();

	}

	if(ComboIndex == 1)
	{
		AnimTime = tfpPawn(Instigator).PlayMeleeAnim(1);
				PlaySwingSound();
	}
    
   NextComboTime = 16;
 

    
	
	


	// Set the end time
	SetTimer(AnimTime -0.1,, nameOf(EndAttack));

}




// Apply damage for a normal attack
exec function DoAttack(vector Start,vector End)
{
 
	local Vector HitLocation, HitNormal;
	local Pawn HitActor;


	local PrimitiveComponent TmpComp;

	//local array<Actor> HurtList;
	local TraceHitInfo HitInfo;
	local UTPhysicalMaterialProperty PhysicalProperty;

	local vector ImpulseVec;



	ImpulseVec = vector (Instigator.Rotation) * Strength;


	

	//tfpPawn(Instigator).bTrueHeadRotation = true;


if(bCanApplyDamage == true)
{    
 
	if(WeaponDurability == 0) // if our weapon has reached ti max durability aka its broken now
	{
	PC.Pawn.TossInventory(PC.Pawn.Weapon);
    }
	

	  
	  
	
    /*************************************** EndControl Trace ********************************************/
	ForEach Worldinfo.AllPawns(class'Pawn', HitActor) //ForEach TraceComponent(class'Actor', HitActor, HitLocation, HitNormal, EndSockEnd, EndSockStart, Extent)
	{

	
	
    
       if(HitActor != none)
       {
      
	   


	       if (HitActor != none)
	       {
            TmpComp = tfppawn(HitActor).Mesh;
           }
    
      //TmpComp = HitActor.CollisionComponent;

  	
	
	
	
    /*************************************** MidControl Trace ********************************************/



	
	
	
	 	
	if(TraceComponent(HitLocation, HitNormal,TmpComp, Start, End) == true)
	{
	//`Log("ON TRACE COMPONENT"$TmpComp);
		// Skip the pawn using the sword
		
		if(HitActor != Owner && CheckHurtList(HitActor) == true)	   
        {
		   //SoundComponent.SoundCue = FleshHitsound[Rand(Fleshhitsound.Length)];

		// Apply damage with a momentum direction from Instigator location to HitActor location
		Hitactor.TakeDamage( SwordAttackHitDamage[ComboIndex], Instigator.Controller,
						HitActor.Location, Normal(HitActor.Location - Instigator.Location) * 50,
						InstantHitDamageTypes[CurrentFireMode],, self);
//						     ToggleHitEffects(HitLocation,HitActor);
						 takedurability(20);


						
	   }
	}


	 	

	


	       /*************************************** Interval1 Trace ********************************************/
	
	
	
	

	

	
}
}
  
}
}













/*
========
Specialization, special actions etc

=========
*/
function setwoodcutting(bool val)
{
  if(val == true)
  {
//   WCSpeed = tfpPlayerController(Instigator.Controller).GetSpecStat("Woodwork","ActionSpeed");
   AttackSpeed = WCSpeed;
  }

   if(val == false)
   {
    AttackSpeed = 1.0F;
   }
	
}



function SetAttackTrue()
{
bCanAttack = true;

ComboIndex = 0;

}










// End any ongoing attack
function EndAttack()
{
	bSwinging = false;
ComboIndex = 0;
bOnHeavyAttack = false;


	// Mark the time we finished the attack
	LastSwordAttackTime = WorldInfo.TimeSeconds;
	
}










DefaultProperties
{


	// Pickup staticmesh
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'GDC_Materials.Meshes.SK_ExportSword2'
	End Object

	Begin Object class=AnimNodeSequence Name=MeshSequenceA
		//bCauseActorAnimEnd=true
	End Object

	Begin Object Class=AudioComponent Name=MeleeSoundComp
	End Object
	SoundComponent=MeleeSoundComp
	Components.Add(MeleeSoundComp);


	


    hiteffect=ParticleSystem'Envy_Effects2.Particles.P_Robot_Gib_Spark'
    PlayerAnimSets(0)=AnimSet'CH_TFP_Male.Anims.Anim_Sword'
	SwordAttackAnim(0)=AxeSwing
	SwordAttackAnim(1)=AxeSwing2
	SwordAttackAnim(2)=AxeSwing3

	SwordAttackHitDamage(0)=15.f
	SwordAttackHitDamage(1)=25.f
	SwordAttackHitDamage(2)=35.f


	SwordCooldownTime=0.7f

	MaxCombo=1 // 2

	SwingSounds[0]=SoundCue'SV_Sounds.Weapons.Swing_Air_I_Cue'
	SwingSounds[1]=SoundCue'SV_Sounds.Weapons.Swing_Air_I_Cue'
	SwingSounds[2]=SoundCue'SV_Sounds.Weapons.Swing_Air_I_Cue'

	HitSounds[0]=(MaterialType=Wood,Sound=SoundCue'SV_Machete.Sounds.Machete_HitWood_I_Cue',PS=ParticleSystem'SV_FX.Effects.P_FX_TreeHit',Decal=DecalMaterial'T_FX.DecalMaterials.M_FX_BloodDecal_FadeViaDissolving',MaxSoundIndex = 2)
    HitSounds[1]=(MaterialType=Wood,Sound=SoundCue'SV_Machete.Sounds.Machete_HitWood_II_Cue',PS=ParticleSystem'SV_FX.Effects.P_FX_TreeHit',Decal=DecalMaterial'T_FX.DecalMaterials.M_FX_BloodDecal_FadeViaDissolving',MaxSoundIndex=2)
    HitSounds[2]=(MaterialType=Wood,Sound=SoundCue'SV_Machete.Sounds.Machete_HitWood_III_Cue',PS=ParticleSystem'SV_FX.Effects.P_FX_TreeHit',Decal=DecalMaterial'T_FX.DecalMaterials.M_FX_BloodDecal_FadeViaDissolving',MaxSoundIndex=2)


    FleshHitSounds[0]=(Sound=SoundCue'SV_Machete.Sounds.Machete_HitFlesh_I_Cue',PS=ParticleSystem'VH_Cicada.Effects.P_VH_Cicada_Decoy_Explo',Decal=DecalMaterial'T_FX.DecalMaterials.M_FX_BloodDecal_FadeViaDissolving')

    FleshHitSounds[1]=(Sound=SoundCue'SV_Machete.Sounds.Machete_HitFlesh_II_Cue',PS=ParticleSystem'VH_Cicada.Effects.P_VH_Cicada_Decoy_Explo',Decal=DecalMaterial'T_FX.DecalMaterials.M_FX_BloodDecal_FadeViaDissolving')


    FleshHitSounds[2]=(Sound=SoundCue'SV_Machete.Sounds.Machete_HitFlesh_III_Cue',PS=ParticleSystem'VH_Cicada.Effects.P_VH_Cicada_Decoy_Explo',Decal=DecalMaterial'T_FX.DecalMaterials.M_FX_BloodDecal_FadeViaDissolving')

	//NextComboTime(0)=4f // First to second combo

	AltWeaponDurability = 0;
	WeaponDurability = 100;
	MaxDurability = 100.f;

	FireInterval(0)=0.01f // its 5 Should be really fast to better register clicks for combo's



	DefaultAnimSpeed=0.9f




	//PivotTranslation=(Y=-25.0)
      bCanCutTrees = true
	ShotCost(0)=0
//	bCanSwing = false;
	

	bCanApplyDamage = false;
    
	MaxAmmoCount=1
	AmmoCount=1
	
	AttackSpeed = 1.0f
	
	
	// swing sound stuff
	
	
	// flesh hit sounds
	

	bCanAttack = true;
	numWeaponTraces=8.0;
	
	
}