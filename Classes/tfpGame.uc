class tfpGame extends UTGame;


	var Sapitu sapitu;
	var () SV_ItemDB DBArchetype;
    var () editconst SV_ItemDB DB;

/*
InitGame
	Slip a base mutator in to replace stuff.
*/
event InitGame( string Options, out string ErrorMessage )
{
	AddMutator("TFP.tfpUT3SupportMutator", false);
	Super.InitGame(Options, ErrorMessage);
}

simulated event PreBeginPlay()
{
	super.PreBeginPlay();
	db = new class'SV_ItemDB' (DBArchetype);

	sapitu = new class'Sapitu';
	sapitu.Init(DB);

}

simulated event PostBeginPlay()
{
	CreateFoliageCollision();
	super.PostBeginPlay();
}




static final function vector MatrixGetScale(Matrix TM) 
{ 
    local Vector s; 
    s.x = sqrt(TM.XPlane.X**2 + TM.XPlane.Y**2 + TM.XPlane.Z**2); 
    s.y = sqrt(TM.YPlane.X**2 + TM.YPlane.Y**2 + TM.YPlane.Z**2); 
    s.z = sqrt(TM.ZPlane.X**2 + TM.ZPlane.Y**2 + TM.ZPlane.Z**2); 
    return s; 
} 

function CreateFoliageCollision() 
{ 
    local InstancedFoliageActor ac; 
    local InstancedStaticMeshComponent comp; 
    local vector loc, scale; 
    local Rotator rot; 
    local int i, j; 
    local SV_TreeArchetype Tree;
	local SV_TreeArchetype SpawnRef;

    //look for the InstancedFoliageActor 
    foreach AllActors(class'InstancedFoliageActor',ac) 
    { 
        //iterate through the various foliage components 
        for(i=0; i<ac.InstancedStaticMeshComponents.Length; i++) 
        { 
            comp = ac.InstancedStaticMeshComponents[i]; 
            Tree = DB.GetTree(comp.StaticMesh);
            if(Tree.IdentifyMesh == ac.InstancedStaticMeshComponents[i].StaticMesh) // equal to roots
            {
            	ac.InstancedStaticMeshComponents[i].SetHidden(true);
            }
           // ac.InstancedStaticMeshComponents[i].SetHidden(true);
                 // ac.InstancedStaticMeshComponents[i].Test();

            //iterate through the various meshes in this component 
            for (j=0; j<comp.PerInstanceSMData.Length; j++) 
            { 
                //decompose the instance's transform matrix 
                loc = MatrixGetOrigin(comp.PerInstanceSMData[j].Transform); 
                rot = MatrixGetRotator(comp.PerInstanceSMData[j].Transform); 
                scale = MatrixGetScale(comp.PerInstanceSMData[j].Transform); 
                DrawDebugBox(loc,vect(200,200,200),255,0,0,true);

                if(Tree != none)
                {
                SpawnRef = Spawn(class'SV_TreeArchetype',,,Loc,Rot,Tree); 
                SpawnRef.SetDrawScale3D(scale);
                }

                  




            } 
        } 
    }
 
} 

function SpawnTree(InstancedFoliageActor AC,StaticMesh Mesh,vector Loc,rotator Rot)
{
	local SV_TreeArchetype Tree;
	local SV_TreeArchetype SpawnRef;
	Tree = DB.GetTree(Mesh);


	if(Tree != none)
	{
		SpawnRef = Spawn(class'SV_TreeArchetype',,,Loc,Rot,Tree); 
	}
	AC.Destroy();


} 

// Function that is executed after each kill
function ScoreKill(Controller Killer, Controller Other)
{
	local tfpPlayerController PC;
	`log("GAMEINFOOOOOOOOO!!!!!!!!!!!!!!!!!! I SCORED KEEEEEEEEEEEEEEEEEEEEEEEEEEL");

	super.ScoreKill(Killer, Other);

	// Cast to the custom MyPlayerController class
	PC = tfpPlayerController(Killer);
	// Give XP through our custom function to our PlayerController, change 100 to whatever amount you want
	PC.GiveXP(100);
	`log("GAVE XP!");
}

defaultproperties
{
	Acronym="SV"
	MapPrefixes[0]="SV"
	Name="Default__tfpGame"
	PlayerControllerClass=class'tfpPlayerController'
	DefaultPawnClass=class'tfpPawn'
	BotClass=class'tfpBot'
	HUDType = class'TFP.SVHudWrapper'
	bWeaponStay=false
	bRestartLevel=false
	bDelayedStart=false
	bUseClassicHUD = true;
	PlayerReplicationInfoClass=Class'TFP.SVPlayerReplicationInfo'
	DBArchetype = SV_ItemDB'SV_Items.DB.ItemDB'
}