class GFxCharacterFrame extends GFxMoviePlayer;

var array<SV_ItemSlot> armorSlots;
var GFxObject XP,Spec1,Spec2,Memory;
var tfpplayercontroller P;
var array<SV_ItemSlot> iSlots;



function Init(optional LocalPlayer LocalPlayer)
{
  // Initialize the ScaleForm movie
  Super.Init(LocalPlayer);

  Start();
  Advance(0);

  XP = GetVariableObject("_root.XP");
  Spec1 = GetVariableObject("_root.Spec1");
  Spec2 = GetVariableObject("_root.Spec2");
  Memory = GetVariableObject("_root.Memory");
  P = tfpplayercontroller(GetPC());
  Populate();
  AddCaptureKey('Escape');


//  PopulateArmors();
}

SIMULATED event bool FilterButtonInput(int ControllerId, name ButtonName, EInputEvent InputEvent)
{
   if(ButtonName == 'Escape')
   {
    Close(true);
   }

  return true;
}

event bool WidgetInitialized(name WidgetName, name WidgetPath, GFxObject Widget)
{

    

    switch(WidgetName)
    {             

      case ('head'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      armorSlots[0] = SV_ItemSlot(Widget);
      armorSlots[0].AddEventListener('CLIK_drop',Drop);
      break;

      case ('chest'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      armorSlots[1] = SV_ItemSlot(Widget);
      armorSlots[1].AddEventListener('CLIK_drop',Drop);
      break;

      case ('larm'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      armorSlots[2] = SV_ItemSlot(Widget);
      armorSlots[2].AddEventListener('CLIK_drop',Drop);
      break;

      case ('rarm'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      armorSlots[3] = SV_ItemSlot(Widget);
      armorSlots[3].AddEventListener('CLIK_drop',Drop);
      break;

      case ('lleg'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      armorSlots[4] = SV_ItemSlot(Widget);
      armorSlots[4].AddEventListener('CLIK_drop',Drop);
      break;

      case ('rleg'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      armorSlots[5] = SV_ItemSlot(Widget);
      armorSlots[5].AddEventListener('CLIK_drop',Drop);
      break;


      case ('slot0'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      iSlots[0] = SV_ItemSlot(Widget);
      break;

       case ('slot1'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      iSlots[1] = SV_ItemSlot(Widget);
      break;

       case ('slot2'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      iSlots[2] = SV_ItemSlot(Widget);
      break;

       case ('slot3'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      iSlots[3] = SV_ItemSlot(Widget);
      break;

       case ('slot4'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      iSlots[4] = SV_ItemSlot(Widget);
      break;

       case ('slot5'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      iSlots[5] = SV_ItemSlot(Widget);
      break;

       case ('slot6'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      iSlots[6] = SV_ItemSlot(Widget);
      break;

       case ('slot7'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
      iSlots[7] = SV_ItemSlot(Widget);
      break;

   
    }
    return true;
}


function Populate()
{

  local int i;
  local SV_ArmorItem itm;  


  for (i = 0; i < P.char.inventory.Length; i++)
  {
    itm = SV_ArmorItem(p.char.inventory[i]);

      if(itm != none && itm.Equipped) // AND ITS EQUIPPED
      {
       armorSlots[i].SetSlotData(p.char.inventory[i].Type,p.char.inventory[i].assetName,p.char.inventory[i].itemName); // we only gonna show armors
      }

      if(itm != none && itm.Equipped == false)
      {
        iSlots[i].SetSlotData(p.char.inventory[i].Type,itm.assetName,itm.itemName);
      }

  }
}

function Drop(GFxClikWidget.EventData ev)
{
  local GFxObject Data;
  local string Item;
  local SV_ArmorItem armor;

  Data = ev.target.GetObject("data");
  Item = Data.getstring("Item");;
  Armor = SV_ArmorItem(P.ItemDB.GetItem(Item));

 // armorSlots[ev.index].SetSlotData(Type,Asset,Item); // we got our 3 vals we can set it up now
  P.EquipArmor(Armor);


}




defaultproperties
{

  bDisplayWithHudOff=false
  MovieInfo=SwfMovie'SURVIVORFlash.CharacterFrame'
  bPauseGameWhileActive=false
  bCaptureInput = true

    WidgetBindings.Add((WidgetName="head",WidgetClass=class'SV_ItemSlot'))
    WidgetBindings.Add((WidgetName="chest",WidgetClass=class'SV_ItemSlot'))
    WidgetBindings.Add((WidgetName="leftarm",WidgetClass=class'SV_ItemSlot'))
    WidgetBindings.Add((WidgetName="rightarm",WidgetClass=class'SV_ItemSlot'))
    WidgetBindings.Add((WidgetName="rleg",WidgetClass=class'SV_ItemSlot'))
    WidgetBindings.Add((WidgetName="lleg",WidgetClass=class'SV_ItemSlot'))

    WidgetBindings.Add((WidgetName="slot0",WidgetClass=class'SV_ItemSlot'))
    WidgetBindings.Add((WidgetName="slot1",WidgetClass=class'SV_ItemSlot'))
    WidgetBindings.Add((WidgetName="slot2",WidgetClass=class'SV_ItemSlot'))
    WidgetBindings.Add((WidgetName="slot3",WidgetClass=class'SV_ItemSlot'))
    WidgetBindings.Add((WidgetName="slot4",WidgetClass=class'SV_ItemSlot'))
    WidgetBindings.Add((WidgetName="slot5",WidgetClass=class'SV_ItemSlot'))
    WidgetBindings.Add((WidgetName="slot6",WidgetClass=class'SV_ItemSlot'))
    WidgetBindings.Add((WidgetName="slot7",WidgetClass=class'SV_ItemSlot'))



}