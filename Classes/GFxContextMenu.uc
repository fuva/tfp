class GFxContextMenu extends GFxMoviePlayer;

var tfpPlayerController PC;
var GFxClikWidget Character,Quests,Talents,Specs,Inventory,SA,Creation;
var GFxObject root;



var SVHUDWrapper HUD;

var name PendingFrame;


function Init(optional LocalPlayer LP)
{
	super.Init();
	Advance(0);
	root = GetVariableObject("_root");
	AddCaptureKey('LeftAlt');
	AddFocusIgnoreKey('Escape');

}

function InitMovie()
{

}






event bool WidgetInitialized(name WidgetName, name WidgetPath, GFxObject Widget)
{
    

    switch(WidgetName)
    {                 
		

		case ('Character'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    Character = GFxClikWidget(Widget);
		Character.AddEventListener('CLIK_Press',ToggleCharFrame);
		break;
		
		case ('Inventory'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    Inventory = GFxClikWidget(Widget);
		Inventory.AddEventListener('CLIK_Press',ToggleInventory);
		break;

		case ('Specs'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    Specs = GFxClikWidget(Widget);
		Specs.AddEventListener('CLIK_Press',ToggleSpecs);
		break;

		case ('Quests'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    Quests = GFxClikWidget(Widget);
		Quests.AddEventListener('CLIK_Press',ToggleQuestLog);
		break;

		case ('Talents'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    Talents = GFxClikWidget(Widget);
		Talents.AddEventListener('CLIK_Press',ToggleTalents);
		break;

		case ('SelfAid'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    SA = GFxClikWidget(Widget);
		SA.AddEventListener('CLIK_Press',ToggleSA);
		break;

		case ('Creation'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    Creation = GFxClikWidget(Widget);
		Creation.AddEventListener('CLIK_Press',ToggleCreation);
		break;


		
		
        default:
		
            break;
    }

    return true;
}

function ToggleCharFrame(GFxClikWidget.EventData ev)
{
	//HUD.ToggleCharFrame();
	PendingFrame = 'ToggleCharFrame';
	PendingFrame = nameof(HUD.ToggleCharFrame);
	CloseMenu();


}

function ToggleInventory(GFxClikWidget.EventData ev)
{

	PendingFrame = nameof(HUD.OpenInventory);
	CloseMenu();
	
}

function ToggleSpecs(GFxClikWidget.EventData ev)
{
	PendingFrame = ''; // FIXME: Add specialization menu
	CloseMenu();
	
}

function ToggleQuestLog(GFxClikWidget.EventData ev)
{
	    PendingFrame = ''; //FIXME: Add quest frame
		CloseMenu();
}

function ToggleTalents(GFxClikWidget.EventData ev)
{
	PendingFrame = ''; //FIXME: Add Talent Tree
	CloseMenu();
	
}

function ToggleSA(GFxClikWidget.EventData ev)
{
	PendingFrame = '';
	//HUD.ToggleWoundStatus(); //FIXME: Add self aid movies
	CloseMenu();
}

function ToggleCreation(GFxClikWidget.EventData ev)
{
        PendingFrame =nameof(HUD.ToggleBuild);
        CloseMenu();
	 //Ask eric to see how they will diversify
	
}

/*
============
Used to close the movie itself

=============
*/

function CloseMenu()
{
	root = GetVariableObject("_root.rootS");
	root.GotoAndPlay("FadeOut");
	root = GetVariableObject("_root");
	root.GotoAndPlay("FadeOut");
}

function FadeOutComplete()
{


	HUD.OpenNewFrame(PendingFrame);
	`log("FDAEOUTCOMPLETE");
	Close(true);
}




defaultproperties
{   
     WidgetBindings.Add((WidgetName="Character",WidgetClass=class'GFxClikWidget'))
      WidgetBindings.Add((WidgetName="Creation",WidgetClass=class'GFxClikWidget'))
	 WidgetBindings.Add((WidgetName="Inventory",WidgetClass=class'GFxClikWidget'))
	 WidgetBindings.Add((WidgetName="Talents",WidgetClass=class'GFxClikWidget'))
	 WidgetBindings.Add((WidgetName="Specs",WidgetClass=class'GFxClikWidget'))
	 WidgetBindings.Add((WidgetName="Quests",WidgetClass=class'GFxClikWidget'))
	 WidgetBindings.Add((WidgetName="SelfAid",WidgetClass=class'GFxClikWidget'))



	 /** If TRUE, this movie player will be allowed to accept input events. Defaults to TRUE */
	bAllowInput = true;

	/** If TRUE, this movie player will be allowed to accept focus events. Defaults to TRUE */
	bAllowFocus = true;

	/** If TRUE, the game will pause while this scene is up */
	//bPauseGameWhileActive = true;

	/** If TRUE, only the LocalPlayerOwner's input can be directed here */
	bOnlyOwnerFocusable = true;

	/** If TRUE, this movie player will capture input */
	bCaptureInput = true;

	/** IF TRUE, this movie player will ignore mouse input */
	bIgnoreMouseInput = false;
	bDisplayWithHUDOFF = false;
	MovieInfo=SwfMovie'SURVIVORFlash.ContextMenu'
	
	
}