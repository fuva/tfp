class DismemberPawn extends UTPawn
placeable;




simulated event TakeDamage(int Damage, Controller InstigatedBy, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{

local Vector Impulse, HitLoc;
local int LODIdx;

Mesh.BreakConstraint(Impulse, HitLoc, 'Tribe_lf_upArm_jnt');
Mesh.BreakConstraint(Impulse, HitLoc, 'Tribe_rt_upArm_jnt');
Mesh.BreakConstraint(Impulse, HitLoc, 'JNT_L_Hip');
Mesh.BreakConstraint(Impulse, HitLoc, 'JNT_R_Hip');

for (LODIdx=0; LODIdx < Mesh.LODInfo.length; LODIdx++ )
{
Mesh.ToggleInstanceVertexWeights(true, LODIdx);
}
Mesh.PlayFaceFXAnim(FaceFXAnimSet'SV_Characters.tribesman.Tribesman_SkelMesh_FaceFX_AnimSet',"welcome","Default",SoundCue'SV_Characters.tribesman.welcome_test');

`trace("dis pamb took damage",`green);
super.TakeDamage(Damage,InstigatedBy,HitLocation,Momentum,DamageType,HitInfo,DamageCauser);

}

defaultproperties
{  
  Begin Object Name=WPawnSkeletalMeshComponent
		bOwnerNoSee=False
		AnimTreeTemplate=AnimTree'SV_Characters.tribesman.Tribesman_AnimTree'
		SkeletalMesh=SkeletalMesh'SV_Characters.tribesman.Tribesman_SkelMesh'
		PhysicsAsset=PhysicsAsset'SV_Characters.tribesman.Tribesman_SkelMesh_Physics'
		AnimSets(0)=AnimSet'SV_Characters.tribesman.Tribesman_AnimSet'
		bUpdateSkelWhenNotRendered=true
		bIgnoreControllersWhenNotRendered=false
		bTickAnimNodesWhenNotRendered=true
		bUpdateKinematicBonesFromAnimation=true
	End Object
	Mesh=WPawnSkeletalMeshComponent
	Components.Add(WPawnSkeletalMeshComponent)

	Begin Object Class=AudioComponent Name=FaceAudioComponent
  End Object
  FacialAudioComp=FaceAudioComponent
  Components.Add(FaceAudioComponent)
	 

	

}