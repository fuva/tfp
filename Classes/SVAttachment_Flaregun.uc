class SVAttachment_Flaregun extends SVWeaponAttachment;










defaultproperties
{ 
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'WP_Flaregun.Mesh.WP_SK_Flaregun'
		AnimSets(0)=AnimSet'WP_Flaregun.Mesh.WP_SK_Flaregun_Anims'
		AnimTree=AnimTree'WP_Flaregun.Animations.AT_Flaregun'
		Scale=1.0
	End Object

	Begin Object Name=MagComp
	SkeletalMesh=SkeletalMesh'WP_Flaregun.Mesh.Ammo_Flare'
	End Object;
		
	WeaponClass=class'SVWeap_Flaregun'
	MuzzleFlashSocket=MuzzleFlashSocket
	
	
	
}