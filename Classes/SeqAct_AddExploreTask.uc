class SeqAct_AddExploreTask extends SequenceAction;
 
 var() string Quest; // Amount of XP this action will give
 var() int TriggerIndex;

event Activated()
{
local tfpPlayerController PC;

PC = tfpPlayerController(GetWorldInfo().GetALocalPlayerController());

 PC.Quests.AddExploreTask(Quest,TriggerIndex);

}
	
DefaultProperties
 {
 	// Name that will apear in the Kismet Editor
 	ObjName="Add Explore Task"
 	 	ObjCategory="Quests "

 }