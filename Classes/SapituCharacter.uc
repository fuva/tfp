/**
 * Savegames Are Possible In The UDK. Well, sort of.
 *
 * By Michiel 'elmuerte' Hendriks for Epic Games, Inc.
 *
 * You are free to use this example as you see fit, as long as you
 * properly attribute the origin.
 */
class SapituCharacter extends Object config(Sapitu) perobjectconfig;

/**
 * The name of the character
 */

 /*
 ================

Basic Player Data
XP,Name,SkillPoints

==================
*/

var config string CharacterName;

var config int XP;
var config int XPGatheredForNextLevel;
var config int XPRequiredForNextLevel;
var config int xpToCurrentLevel;
var config int level;
var config int SkillPoints;
//RPG STATS

var config int Thirst;
var config int Energy;
var config int Hunger;

var config int HungerRate,EnergyRate;
var config int ThirstRate;

var config int MaxThirst,MaxHunger,MaxEnergy;
// NEEDS

var config float Temperature;
var config int Health;
var config int maxHealth;

//stamina
var config float Stamina;
var config float MaxStamina;
var config float SprintMultiplier;
var config float StaminaRate;
var config float StaminaRegenRate;

// inventory vars
var float Weight;
var float MaxWeight;



// buffs
var config int Buffs;


/**
 * The identifiers of the inventory items.
 */


var config array<string> inventoryRecords;
var array<SV_ItemArchetype> inventory;


var config array<string> buffsRecord;
var config array<string> buildingRecords;

/*
===========
buildings

===========
*/
var array<SV_BuildingArchetype> StructureList; 

/*
======
recipes
====
*/

var array<SV_Recipe> Recipes; 
var config array<string> recipesRecord;

/*
=======
quests
=======
*/

var array<Quest> quests;
var config array<string> questsRecord;

/*
=======
Skills
========
*/

var array<SV_Skills> Skills;
var config array<string> skillsRecord;



/*
=======

Player Limbs
=======
*/

struct Limb
{
  var string LimbName;
  var int Health;
  var string Status;
  var string WoundType;
  var bool Bleeding,Broken,Infected;
  var bool Treated;

};

var array<Limb> Limbs;
var config array<string> limbsRecord;





struct SpecRecord
{
var string SName; // spec name
var int Level; // spec level
var int XP; // XP
var int XPToLevel; // xp required to next level
var int ModifyStat; // stat to modify
var int WoodChance;
var bool Active; // is this our current specialization?
var int ActionSpeed; // The action to modify of the specialization action
var string Icon; // icon to show on HUD
var bool IsMainSpec;
var float Time;
};


var config array<SpecRecord> Specs;

var SpecRecord Spec1,Spec2;



exec function InitLimbs()
{
  local Limb L;

  L.LimbName = "Head";
  L.Health = 100;
  L.Status = "Healthy";

  Limbs.AddItem(L);

  L.LimbName = "Left Arm";
  L.Health = 100;
  L.Status = "Healthy";

  Limbs.AddItem(L);

  L.LimbName = "Right Arm";
  L.Health = 100;
  L.Status = "Healthy";

  Limbs.AddItem(L);

  L.LimbName = "Chest";
  L.Health = 100;
  L.Status = "Healthy";

  Limbs.AddItem(L);

  L.LimbName = "Left Leg";
  L.Health = 100;
  L.Status = "Healthy";

  Limbs.AddItem(L);

  L.LimbName = "Right Leg";
  L.Health = 100;
  L.Status = "Healthy";

  Limbs.AddItem(L);

}

exec function InitSpecs()
{
local SpecRecord Spec;

  // wood work
  Spec.SName = "Woodwork";
  Spec.Level = 0;
  Spec.XP = 0;
  Spec.XPToLevel = 120;
  Spec.ActionSpeed = 2.0; // rate to speed up the action
  Spec.Active = true;
  specs.AddItem(Spec);
  
  //cooking
  Spec.SName = "Cooking";
  Spec.Level = 0;
  Spec.XP = 0;
  Spec.XPToLevel = 120;
  Spec.Active = false;
  specs.AddItem(Spec);
  
  Spec.SName = "Smithing";
  Spec.Level = 0;
  Spec.XP = 0;
  Spec.XPToLevel = 120;
  Spec.Active = false;
  specs.AddItem(Spec);
  
  Spec.SName = "Mining";
  Spec.Level = 0;
  Spec.XP = 0;
  Spec.XPToLevel = 120;
  Spec.Active = false;
  specs.AddItem(Spec);
  
  Spec.SName = "Hunting";
  Spec.Level = 0;
  Spec.XP = 0;
  Spec.XPToLevel = 120;
  Spec.Active = false;
  specs.AddItem(Spec);
  
  Spec.SName = "Gathering";
  Spec.Level = 0;
  Spec.XP = 0;
  Spec.XPToLevel = 120;
  Spec.Active = false;
  specs.AddItem(Spec);
  
  Spec.SName = "Medicine";
  Spec.Level = 0;
  Spec.XP = 0;
  Spec.XPToLevel = 120;
  Spec.Active = false;
  specs.AddItem(Spec);
  
  Spec.SName = "Building";
  Spec.Level = 0;
  Spec.XP = 0;
  Spec.XPToLevel = 30;
  Spec.Active = false;
  specs.AddItem(Spec);
  
  
  
}

/**
 * Save this character and it's inventory. Do not use SaveConfig() to save
 * the character, because it won't save its inventory.
 */
function save()
{
	local int i;
	for (i = 0; i < inventory.Length; i++)
	{
		inventory[i].SaveConfig();
	}
	SaveConfig();
}

/**
 * Add an item to the inventory
 *
 * @param item
 *		The item to add.
 * @return
 *		True if the item was added
 */


function bool GiveItem(SV_ItemArchetype item)
{
	local int i;
	i = Inventory.find(item);
	if (i != INDEX_NONE) return false;
	Inventory.addItem(item);
	inventoryRecords.addItem(item.ItemName);
	return true;
}

/**
 * Remove an item from the inventory
 * @param item
 *		The item to remove
 * @return
 *		True if the item was removed
 */
function bool removeItem(SV_ItemArchetype item)
{
 
	local int i;
	i = inventory.find(item);
	if (i == INDEX_NONE) return false;
	inventory.removeItem(item);
	inventoryRecords.removeItem(item.ItemName);
	return true;
}


/**
* Add an structure to the player list
*
*
*/

function bool GiveBuild(SV_BuildingArchetype item)
{
  local int i;
  i = StructureList.find(item);
  if (i != INDEX_NONE) return false;
  StructureList.addItem(item);
  buildingRecords.addItem(item.BuildName);
  return true;
}



/*
=========
Adds an recipe to the player recipe list
===========
*/

function bool GiveRecipe(SV_Recipe item)
{
  local int i;
  i = Recipes.find(item);
  if (i != INDEX_NONE) return false;
  Recipes.addItem(item);
  recipesRecord.addItem(item.recipename);
  return true;
}


/*
=============
give quest, or well handles quest save and stuff

===============
*/

function bool addQuest(Quest item)
{
  local int i;
  i = quests.find(item);
  if (i != INDEX_NONE) return false;
  quests.addItem(item);
  questsRecord.addItem(string(item.name));
  return true;
}


/*
======
Skills!

Skill giving 

==========
*/

function bool giveSkill(SV_Skills skill)
{
  local int i;
  i = skills.find(skill);
  if (i != INDEX_NONE) return false;
 skills.addItem(skill);
  skillsRecord.addItem(skill.SkillName);
  return true;
}

DefaultProperties
{

}