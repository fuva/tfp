class SVButtonMovie extends GFxMoviePlayer;


function Init(optional LocalPlayer LocalPlayer)
{
  // Initialize the ScaleForm movie
  Super.Init(LocalPlayer);

  Start();
  Advance(0);
}


defaultproperties
{

  bDisplayWithHudOff=false
  TimingMode=TM_Game
  bPauseGameWhileActive=false

}