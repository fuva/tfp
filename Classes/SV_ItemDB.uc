class SV_ItemDB extends Object;


var(Items) array<SV_ItemArchetype> ItemsList;
var(Structures) array<SV_BuildingArchetype> BuildingsList;
var(Recipes) array<SV_Recipe> RecipeList;
var(Skills) array<SV_Skills> SkillList;
var(Trees) array<SV_TreeArchetype> TreeList;




function SV_ItemArchetype GetItem(string ItemName)
{
 local int i;
 
  for(i=0;i<ItemsList.length;i++)
  
    if(ItemName == ItemsList[i].ItemName)
	return ItemsList[i];
	return none;
  
	
}

function SV_Recipe GetRecipe(string RName)
{
 local int i;
 
  for(i=0;i<RecipeList.length;i++)
  
    if(RName == RecipeList[i].RecipeName)
	return RecipeList[i];
	return none;
  
	
}


function SV_BuildingArchetype GetStructure(string BuildName)
{
 local int i;
 
  for(i=0;i<BuildingsList.length;i++)
  
    if(BuildName == BuildingsList[i].BuildName)
	return BuildingsList[i];
	return none;
  
	
}


function SV_BuildingArchetype GetStructureRand(int Index)
{
 local int i;
 
  for(i=0;i<Index;i++)
	return BuildingsList[i];
	return none;
  
	
}

function SV_TreeArchetype GetTree(StaticMesh Tree)
{
	local int i;
	  for(i=0;i<TreeList.length;i++)
		
		if(TreeList[i].IdentifyMesh == Tree)
		return TreeList[i];
		return none;
}


function SV_Skills GetSkill(string sName)
{
 local int i;
 
  for(i=0;i<SkillList.length;i++)
  
    if(sName == SkillList[i].skillName)
	return SkillList[i];
	return none;
  
	
}

function SV_Skills GiveRandomSkill()
{
	local int i;
	i = Rand(SkillList.Length);

	return SkillList[i];
}




defaultproperties
{
}