class GFxMultiplayer extends GFxMoviePlayer;

var array<UDKUIDataProvider_MapInfo> maps;

var array<GFxClikWidget> MapIcons;
var array<GFxObject>  MapName;
var array<GFxObject>  MapDesc;
var string Gamemode;

var int MapIndex;


var array<GFxClikWidget> MapBtns;
var GFxClikWidget PlayBtn,ModeBtn,HostBtn,ServersBtn;

function Init(optional LocalPlayer player)
{
Start();
} 

function GetMapDesc()
{
        MapDesc[0]=GetVariableObject("_root.MapDesc1");
    MapDesc[1]=GetVariableObject("_root.MapDesc2");
    MapDesc[2]=GetVariableObject("_root.MapDesc3");
    MapDesc[3]=GetVariableObject("_root.MapDesc4");
    MapDesc[4]=GetVariableObject("_root.MapDesc5");
}


event bool WidgetInitialized(name WidgetName, name WidgetPath, GFxObject Widget)
{    
    `log("wIDGET iNIT IS" @WidgetName);
    

    switch(WidgetName)
    {   

     case ('Solo'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        PlayBtn = GFxClikWidget(Widget);
        PlayBtn.AddEventListener('CLIK_click',PlayPress);
        break;

    case ('MapIcon1'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        MapIcons[0] = GFxClikWidget(Widget);
        break;

        case ('MapIcon2'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        MapIcons[1] = GFxClikWidget(Widget);
        break;


        case ('MapIcon3'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        MapIcons[2] = GFxClikWidget(Widget);
        break;


        case ('MapIcon4'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        MapIcons[3] = GFxClikWidget(Widget);
        break;

         case ('MapIcon5'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        MapIcons[4] = GFxClikWidget(Widget);
        break;

           case ('MapBtn1'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        MapBtns[0] = GFxClikWidget(Widget);
        MapBtns[0].AddEventListener('CLIK_click',Btn1Press);
        break;

        case ('MapBtn2'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        MapBtns[1] = GFxClikWidget(Widget);
        MapBtns[1].AddEventListener('CLIK_click',Btn2Press);
        break;


        case ('MapBtn3'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        MapBtns[2] = GFxClikWidget(Widget);
        MapBtns[2].AddEventListener('CLIK_click',Btn3Press);
        break;


        case ('MapBtn4'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        MapBtns[3] = GFxClikWidget(Widget);
        MapBtns[3].AddEventListener('CLIK_click',Btn4Press);
        break;

         case ('MapBtn5'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        MapBtns[4] = GFxClikWidget(Widget);
     MapBtns[4].AddEventListener('CLIK_click',Btn5Press);
        break;
    }
        ListMaps();
    return true;
}
  
function ListMaps()
{

    local array<UDKUIResourceDataProvider>    providers;
    local UDKUIResourceDataProvider          p;
    local int i;


    // populate the providers array
    class'UDKUIDataStore_MenuItems'.static.GetAllResourceDataProviders(class'UDKUIDataProvider_MapInfo', providers);


    // for convenience, filter the provider list into a list of _MapInfos
    foreach providers(p)
    {
        if ( UDKUIDataProvider_MapInfo(p) != none )
            maps.AddItem( UDKUIDataProvider_MapInfo(p) );
    }

    `log("Maps...");

        for (i = 0; i < 4; ++i)
        {
        //`log("Map:" @ maps[i].MapName);
        
        if(maps[i].PreviewImageMarkup != "")
        {
            `log("Map Icon:" @ maps[i].PreviewImageMarkup);
            MapIcons[0].SetString("source",maps[i].PreviewImageMarkup);
           // MapName[i].SetText(maps[i].MapName);
           
            `log("Map Desc:" @ maps[i].description);

        
        }

        if(maps[i].Description != "")
        {
            GetMapDesc();
          MapDesc[i].SetText(maps[i].description);

             //MapDesc[0].SetText("HMM");
        //MapIcons[i].SetString("source",maps[i].PreviewImageMarkup);
        
        }
        
    }



   

}

function PlayPress(GFxClikWidget.EventData ev)
{
ConsoleCommand("open "$maps[MapIndex].MapName);
}


function Btn1Press(GFxClikWidget.EventData ev)
{
Gamemode = "Softcore";
MapIndex = 0;
}


function Btn2Press(GFxClikWidget.EventData ev)
{
   MapIndex = 1;
   Gamemode = "Normal"; 
}

function Btn3Press(GFxClikWidget.EventData ev)
{
    
}

function Btn4Press(GFxClikWidget.EventData ev)
{
    
}

function Btn5Press(GFxClikWidget.EventData ev)
{
    
}


defaultproperties
{
    MovieInfo=SwfMovie'SURVIVORFlash.MPMenu'
WidgetBindings.Add((WidgetName="MapIcon1",WidgetClass=class'GFxClikWidget'))
WidgetBindings.Add((WidgetName="MapIcon2",WidgetClass=class'GFxClikWidget'))
WidgetBindings.Add((WidgetName="MapIcon3",WidgetClass=class'GFxClikWidget'))
WidgetBindings.Add((WidgetName="MapIcon4",WidgetClass=class'GFxClikWidget'))
WidgetBindings.Add((WidgetName="MapIcon5",WidgetClass=class'GFxClikWidget'))



WidgetBindings.Add((WidgetName="MapBtn1",WidgetClass=class'GFxClikWidget'))
WidgetBindings.Add((WidgetName="MapBtn2",WidgetClass=class'GFxClikWidget'))
WidgetBindings.Add((WidgetName="MapBtn3",WidgetClass=class'GFxClikWidget'))
WidgetBindings.Add((WidgetName="MapBtn4",WidgetClass=class'GFxClikWidget'))
WidgetBindings.Add((WidgetName="MapBtn5",WidgetClass=class'GFxClikWidget'))


WidgetBindings.Add((WidgetName="Solo",WidgetClass=class'GFxClikWidget'))
bCaptureInput = true;

}