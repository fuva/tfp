class GFxWoundMenu extends GFxMoviePlayer;

var GFxClikWidget HeadBtn,ChestBtn,lArmBtn,rArmBtn,lLegBtn,rLegBtn;
var tfpplayercontroller PC;

function Init(optional LocalPlayer player)
{
    PC = tfpplayercontroller(GetPC());
Start();
} 


event bool WidgetInitialized(name WidgetName, name WidgetPath, GFxObject Widget)
{    
    `log("wIDGET iNIT IS" @WidgetName);
    

    switch(WidgetName)
    {   

     case ('HeadBtn'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        HeadBtn = GFxClikWidget(Widget);
        HeadBtn.AddEventListener('CLIK_click',HeadPress);
        break;

         case ('ChestBtn'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        ChestBtn = GFxClikWidget(Widget);
        ChestBtn.AddEventListener('CLIK_click',ChestPress);
        break;

         case ('lArmBtn'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        lArmBtn = GFxClikWidget(Widget);
        lArmBtn.AddEventListener('CLIK_click',LeftArmPress);
        break;

         case ('rArmBtn'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        rArmBtn = GFxClikWidget(Widget);
        rArmBtn.AddEventListener('CLIK_click',RightArmPress);
        break;

         case ('lLegBtn'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        lLegBtn = GFxClikWidget(Widget);
        lLegBtn.AddEventListener('CLIK_click',LeftLegPress);
        break;

         case ('rLegBtn'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
        rLegBtn = GFxClikWidget(Widget);
        rLegBtn.AddEventListener('CLIK_click',RightLegPress);
        break;

   
    }

    return true;
}
 

function HeadPress(GFxClikWidget.EventData ev)
{
    PC.LookAtLimb("Head");


}

function ChestPress(GFxClikWidget.EventData ev)
{
    PC.LookAtLimb("Chest");
    

}


function LeftArmPress(GFxClikWidget.EventData ev)
{
    PC.LookAtLimb("Left Arm");
    

}


function RightArmPress(GFxClikWidget.EventData ev)
{
    PC.LookAtLimb("Right Arm");
    

}


function LeftLegPress(GFxClikWidget.EventData ev)
{
    PC.LookAtLimb("Left Leg");
    

}


function RightLegPress(GFxClikWidget.EventData ev)
{
    PC.LookAtLimb("Right Leg");
    

}





defaultproperties
{
    MovieInfo=SwfMovie'SURVIVORFlash.WoundMenu'
WidgetBindings.Add((WidgetName="HeadBtn",WidgetClass=class'GFxClikWidget'))
WidgetBindings.Add((WidgetName="ChestBtn",WidgetClass=class'GFxClikWidget'))
WidgetBindings.Add((WidgetName="lArmBtn",WidgetClass=class'GFxClikWidget'))
WidgetBindings.Add((WidgetName="rArmBtn",WidgetClass=class'GFxClikWidget'))
WidgetBindings.Add((WidgetName="lLegBtn",WidgetClass=class'GFxClikWidget'))



WidgetBindings.Add((WidgetName="rLegBtn",WidgetClass=class'GFxClikWidget'))
bCaptureInput = true;

}