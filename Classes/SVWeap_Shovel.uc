class SvWeap_Shovel extends MeleeWeaponBase;


DefaultProperties
{
	// Pickup staticmesh
	Begin Object Name=PickupMesh
		
	End Object
	EquipIndex = 6;
	HolsterIndex = 3;

	Begin Object class=AnimNodeSequence Name=MeshSequenceA
	End Object
	WeaponName = "Rusty Shovel"
	PlayerAnimSets(0)=AnimSet'CH_TFP_Male.Anims.Anim_Heavy'
	AttachmentClass=class'SVAttachment_Shovel'
	InstantHitDamageTypes(0)=class'SVDmgType_Sharp'
	  bCanBlock = true

	WeaponDurability = 100;
 
	InventoryGroup = 2;
    
    numWeaponTraces = 6;
	
}