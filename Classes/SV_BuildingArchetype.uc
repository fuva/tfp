class SV_BuildingArchetype extends KAssetSpawnable
	HideCategories(Attachment, Physics, Object,KActor)
	placeable;

// physical item representation

/*
=====================
Item Defininition and parameters first

==================
*/

var (Structure) string BuildName;
var (Structure) string Description; // long description of the item

var (UI) string AssetName; // 
var (UI) string Icon;

var (Structure) int Durability;
var (Structure) array<SV_ItemArchetype> recItems;
var (Structure) bool IsSkeletalMesh; // will this structure use an skeletal mesh?

var (Structure) StaticMesh StatusModel;
var (Structure) StaticMesh StatusModel2;

var (Structure) bool Bed,Campfire,IsStatic,Workbench;

// Building state, placeable or not

var bool Placeable;
var bool bPlaced;

var MaterialInstanceConstant StatusMat;
var MaterialInstanceConstant Mat;

// overall builded percentage
var int BuildPC;
VAR tfpplayercontroller PC;

/*
=========
campfire related variables
==========
*/
var ParticleSystemComponent FireEffect; 
var bool IsOn;
var int TimeTillShutdown; // after wood has been placed and the campfire is on
// theres a timer that will shut it down

auto state Initialize
{
Begin:
 

    PC = tfpplayercontroller( GetALocalPlayerController());
}

simulated event PostBeginPlay()
{
	super.PostBeginPlay();
	SetPhysics(PHYS_NONE);
    Mat = new(None) Class'MaterialInstanceConstant';
   // Mat.SetParent(StaticMeshComponent.GetMaterial(0));


    StatusMat = new(None) class'MaterialInstanceConstant';
    StatusMat.SetParent(MaterialInstanceConstant'SV_Buildings.Material.Mat_Building_Shader_INST');

     SetPhysicsMode(true);
   //StaticMeshComponent.SetMaterial(0,StatusMat);


}

function SetPlaced()
{
	bPlaced = true;
//	StaticMeshComponent.SetMaterial(0,Mat);
}


function CheckLocation(vector NewLoc)
{

local Vector groundLoc;
local vector NewPos;
local Vector groundNorm;
local vector checkloc;
local rotator FloorRot;
local actor actor;
local LinearColor LC;



	

	Trace(groundLoc, groundNorm, location - vect(0,0,500), Location + vect(0,0,500));
	NewPos.X = NewPos.X + NewLoc.X;
	NewPos.Y = NewPos.Y + NewLoc.Y;
	NewPos.Z = groundLoc.Z;
	FloorRot = rotator(groundLoc);
	FloorRot.Yaw = 0;
	FloorRot.Pitch = 0;
	
     SetLocation(newPos);
	 SetRotation(FloorRot);
   checkloc = location;
   checkloc.Z = checkloc.Z + 10; // remember to always check slightly a bit over the Z so terrain dont mess up the stuff
	Placeable = true;


	  ForEach VisibleCollidingActors(class'Actor', Actor, 1.0,checkloc, true,, TRUE)
      { 

       if(Actor.bStatic == true)
       {
       	Placeable = true;
       	break;
       }

       Placeable = false;
       StatusMat.SetVectorParameterValue('Color',LC);
	     break;
      }
	
}


simulated function TakeDamage(int Damage, Controller InstigatedBy, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{

   if(BuildPC != 100)
   {
   	BuildPC += 5;
   }

   `log( "SV_ BUILDING ARCHETYPE TOOK DAMAGE");


  if(BuildPC >= 50 && !IsSkeletalMesh) // if build percentage greather than 50 and its not skeletal
  {
    // StaticMeshComponent.SetStaticMesh(StatusModel);
     SetPhysics(PHYS_None);
  } 

     if(BuildPC >= 100 && !IsSkeletalMesh) // if build percentage greather than 50 and its not skeletal
     {
    // StaticMeshComponent.SetStaticMesh(StatusModel2);
     SetPhysics(PHYS_RigidBody);
     } 

}

/* 
used structure

used to check HOW to use the structure
*/

function Used(tfpPawn UsedPawn)
{
  local vector Loc;
  SkeletalMeshComponent.GetSocketWorldLocationAndRotation('UseSocket',loc,,);

  if(IsStatic)
  {
    return;
  }

    UsedPawn.UsedStructure(Bed,Campfire,Workbench);
    tfpplayercontroller(UsedPawn.Controller).UseLoc = Loc;



}


function SetPhysicsMode(bool val)
{
SkeletalMeshComponent.WakeRigidBody();


    skeletalmeshcomponent.SetHasPhysicsAssetInstance( val );
    skeletalmeshcomponent.InitRBPhys();
    skeletalmeshcomponent.MinDistFactorForKinematicUpdate = 0.f;


   //set rb & collision channels
   skeletalmeshcomponent.SetRBChannel(RBCC_GameplayPhysics);
   skeletalmeshcomponent.SetRBCollidesWithChannel(RBCC_Default, true);
   skeletalmeshcomponent.SetRBCollidesWithChannel(RBCC_Pawn, true);
   skeletalmeshcomponent.SetRBCollidesWithChannel(RBCC_Vehicle, true);
   skeletalmeshcomponent.SetRBCollidesWithChannel(RBCC_Untitled3, !val);
   skeletalmeshcomponent.SetRBCollidesWithChannel(RBCC_BlockingVolume,val);

      //update the skeleton
      skeletalmeshcomponent.ForceSkelUpdate();
      // Move into post so that we are hitting physics from last frame, rather than animated from this
    skeletalmeshcomponent.SetTickGroup(TG_PostAsyncWork);
      //Swap our collision components
      CollisionComponent = skeletalmeshcomponent;
      //Shrink the cylinder
      
      // Turn collision on for skelSkeletalMeshComponentcomp and off for cylinder

      skeletalmeshcomponent.SetActorCollision(val, val);
      skeletalmeshcomponent.SetTraceBlocking(val, val);
      //Set rigid body
      SetPhysics(PHYS_RigidBody);
      skeletalmeshcomponent.PhysicsWeight = 1.0;
    // Unfix the rigid body instance so it is dynamic
      skeletalmeshcomponent.PhysicsAssetInstance.SetAllBodiesFixed(false);
    skeletalmeshcomponent.SetBlockRigidBody(val);
      //just in case...
    skeletalmeshcomponent.bEnableFullAnimWeightBodies = val;
      skeletalmeshcomponent.bUpdateKinematicBonesFromAnimation=val;
      //skeletalmeshcomponent.SetRBLinearVelocity(Location, val);
     
      skeletalmeshcomponent.SetNotifyRigidBodyCollision(val);
    skeletalmeshcomponent.WakeRigidBody();
}



/*
======
NEW
Returns if the player is aiming at this object or not
======
*/



DefaultProperties
{
	bCollideActors = true;
	bWakeOnLevelStart = true;
	bDamageAppliesImpulse = true;
	//Network
	//bOnlyDirtyReplication=true
   RemoteRole=ROLE_SimulatedProxy
	
}



