class SV_ItemArchetype extends KActorspawnable
	HideCategories(Attachment, Physics, Object,KActor)
	placeable;

// physical item representation

/*
=====================
Item Defininition and parameters first

==================
*/

var (Item) string ItemName;
var (Item) string Description; // long description of the item
var (Item) string Type; // what type is this item, is it food? etc
var (Item) int Weight;// Weight in the inventory of this item

var (UI) string AssetName; // 
var (UI) string ItemIcon;



var (Weapon) bool Weapon; // is it a weapon?
var (Weapon) class<tfpWeapon> WeaponClass; // class of the weapon
var (Weapon) bool Heavy; // Heavy weapon?
var (Weapon) bool Medium; // Self
var (Weapon) bool Light;



//var () string DisplayName;
var (Item) int OnUseAmount; // Once we use this item how much will it heal thirst or hunger?
var (Item) int Durability;

var int Count;//stack or count of the item


//var SV_LootActor SpawnedActor;

var bool Equipped;

var tfpplayercontroller Player;


//function RemoveFromLootActor()
//{
//SpawnedActor.LootItems--;
//}

simulated event PostBeginPlay()
{
	super.PostBeginPlay();
	`log("I HAVE BEEN SPAWNED");

}

function Use(tfpPlayerController InstigatorController)
{
	local float EndTime;

     if(Type == "Food")
     {
     EndTime = InstigatorController.PlayUseAnim('Eat');
     }
 
      if(Type == "Drink")
      {
       EndTime =  InstigatorController.PlayUseAnim('Drink');
      }

      Player = InstigatorController;

      SetBase(Player.Pawn,,,'ItemSocket');

      SetTimer(EndTime,false,'DetachItem');
  

}

function DetachItem()
{
	Destroy();
}

function SetPhysState()
{
}


simulated event PostRenderFor(PlayerController PC, Canvas Canvas, vector CameraPosition, vector CameraDir)
{

 
 

}

DefaultProperties
{
	bCollideActors = true;
	bWakeOnLevelStart = true;
	bDamageAppliesImpulse = true;
	//Network
	//bOnlyDirtyReplication=true
   RemoteRole=ROLE_SimulatedProxy
	
}



