class SVWeaponAttachment extends UTWeaponAttachment;


var SkeletalMeshComponent MagComponent;
var name MagSocket;

event PostBeginPlay()
{
	super.PostBeginPlay();
	AttachMag();
}

simulated function AttachMag()
{

	
	
			// Weapon Mesh Shadow
			MagComponent.SetShadowParent(Mesh);

			Mesh.AttachComponent(MagComponent, MagSocket);
			//SkeletalMeshComponent.SetAnimTreeTemplate(OwnerPawn.Mesh.AnimTreeTemplate);
			//MagComponent.AnimSets = Mesh.AnimSet;
			MagComponent.SetParentAnimComponent(Mesh);
			//MagComponent.SetLightEnvironment(Mesh.LightEnvinronment);
}

DefaultProperties
{
	Begin Object Name=MagComp Class=SkeletalMeshComponent
	End Object;
	MagComponent = MagComp
	Components.Add(MagComp);
	MagSocket=MagBone;

}

