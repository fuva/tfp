class SV_Skills extends Object;

var(Skill) string requiredCharacter;//skilltrees are character specific
var(Skill) string SkillName;
var(Skill) string SkillDesc;
var(Skill) string requiredSkill; //What skill we require to unlock this one
var(Skill) int requiredSkillPoints;// How much skill points we need to unlock this skill
var(Skill) bool IncHealth;//Will this skill increase health;
var(Skill) bool IncStamina;
var(Skill) bool IncDefense;
var(Skill) float IncAmount;// amount this skill will increase
var(Skill) bool Unlocked;
var(Skill) string SpecTied; // To which spec is this skill tied to?
var(UI) string assetName;



defaultproperties
{


}