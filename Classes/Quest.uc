class Quest extends Object;


struct Objectives
{
    var string ObjName;
    var int ObjIndex;  
};

struct Required
{
    var string Name;
    var int Amount;
    var int MaxAmount;
    var string CurrentObjective;  
};

struct ExploreZone
{
    var string Name;
    var int Index;
};


var array<Objectives> QuestObjectives;
var array<ExploreZone> QuestExploreZones;


var string QuestName;
var string QuestDescription;
var string QuestObjective;
var bool bIsActiveQuest; // quest variables

var bool bCanEndQuest;
var int ExperienceReward;
var QuestManager QuestManager;


var bool HasExplorationTask;
var bool HasRecolectionTask;

var int ObjectiveIndex;

/*
======
reward vars
======
*/

var int XPReward;


/*
============
recolection vars
============
*/

var bool bHasItems;
var array<Required> RequiredItems;



function AddRequiredItem( string itemName,int Amount )
{
    local Required r; // a new, empty struct

    r.Name = itemName; // Fill in the name
    r.MaxAmount = Amount;
   

    RequiredItems.AddItem(r); // Add it to the array
}


function PickedUpItem( string newItemName )
{
    
    local Required item;
    local int i;
    local Sequence GameSeq;
    local array<SequenceObject> AllSeqEvents;
        local WorldInfo WorldInfo;
            WorldInfo = class'WorldInfo'.static.GetWorldInfo();
  
       `log("picked up item in da quest called ");
     
    // Go through all the required items looking for a match
    foreach RequiredItems(item)
    {
        `log(newItemName);
        // If the names match AND the player hasn't yet collected this item...
        if ( item.Name == newItemName)
        {
         
          
                bHasItems = true;
            
            // Flag this item as 'collected'
            `log("qhas items!");
                 GameSeq = WorldInfo.GetGameSequence();
                if(GameSeq != None)
                {         
                GameSeq.FindSeqObjectsByClass(class'SeqEvent_HasItems', true, AllSeqEvents); //ITERATE OVER ALL EVENTS OF SOME TYPE (YOU HAVE ONLY ONE)
                for(i=0; i<AllSeqEvents.Length; i++)
                {
                    if(SeqEvent_HasItems(AllSeqEvents[i]).quest == QuestName)
                    {
                
                   SeqEvent_HasItems(AllSeqEvents[i]).CheckActivate(WorldInfo, None); //TRIGGER IT
                   SeqEvent_HasItems(ALLSeqEvents[i]).Activated();
                    }
                }
            }
            // No need to go any further, stop the loop early. Could also return from here
            break;
        }
    }
    
}

/* recolection ends here */



function QuestBegin()
{
}


function AddObjective( string obj,int Index )
{
    local Objectives r; // a new, empty struct

    r.ObjName = obj;
    r.ObjIndex = Index;
    QuestObjectives.AddItem(r); // Add it to the array
}


/*
=======
explore shiz begins here
========
*/


function AddExploreZone( string zne,int zIndex )
{
    local ExploreZone r; // a new, empty struct
    local WorldInfo WorldInfo;
    local QuestLocDummy ZoneTrigger;

    r.Name = zne;
    r.Index = zIndex;
    QuestExploreZones.AddItem(r); // Add it to the array
   // PC2.UpdateQuestObjectives(obj,index);
    WorldInfo = class'WorldInfo'.static.GetWorldInfo();
    ForEach WorldInfo.AllActors(class'QuestLocDummy', ZoneTrigger) // search for all the quest triggers and ACTIVATE, the one that matches our quest
    {
        if(ZoneTrigger.QuestName == QuestName && ZoneTrigger.LocIndex == zIndex)
        {
            ZoneTrigger.IsActive = true;

        }

    }
}



defaultproperties
{

}