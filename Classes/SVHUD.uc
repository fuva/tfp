class SVHUD extends GFxMoviePlayer;

//var MeleeWeaponBase LastWeapon;
var float LastHealthpc,ThirstPC,HungerPC,EnergyPC,XPPC,MemoryPC,WeaponDPC,StaminaPC;
// Chat vars



var bool bShowMovie;
var tfpplayercontroller PC; 
var int ThirstVal;


// Standard Flash Objects
var GFxObject HealthbarMC,MemorybarMC,XPBarMC;
var GFxObject PlayerlevelTXT,ThirstB,HungerB;
var GFxObject EnergyB,WeaponDB,StaminaBar;
var GFxObject buff1,buff2,buff3,buff4;
var GFxObject bufftime1,bufftime2,bufftime3,bufftime4;
var GFxObject buffname1,buffname2,buffname3,buffname4;
var GFxObject debuff1txt,debuff2txt,debuff3txt,debuff4txt;
var GFxObject debuff1,debuff2,debuff3,debuff4;
var GFxObject Debuff1name,debuff2name,debuff3name;
var GFxObject Debuff4name;
var GFxObject Debuff1time,Debuff2Time,Debuff3Time;
var GFxObject Crosshair;
var GFxObject Tooltip;
var GFxObject TooltipItem;
var GFxObject TooltipIcon;
var GFxObject RootMC;
var GFxObject HFrame;

var GFxObject Durability;

// NEW STUFF FOR FROGGING NOTIFICATIONS

var GFxObject NTAction;
var GFxObject NTSpec;
var GFxObject NTXP;
var GFxClikWidget NTActionIcon;
var GFxClikWidget NTSpecIcon;
var GFxObject UpperElement;
var GFxObject Notification;
var GFxObject SpecXPBar;
var string ActionS,SpecS;




var int Level,SpecXP;
var string SpecIcon;

// weapon icon ui
var GFxClikWidget WeaponIcon;



var WorldInfo MyWorld;
var tfpPlayerController PlayerOwner;

var bool bUpdateTooltip;

function Hide(bool bShow)
{
LOCAL gfxObject ROOT;
Root = GetVariableObject("_root");
root.SetVisible(bShow);
}

function Init(optional LocalPlayer player)
{	
	super.Init(player);
    Start();
    Advance(0);

	

	PC = tfpplayercontroller(GetPC());
	//PC.InitHUD(self);

	HealthbarMC = GetVariableObject("_root.healthstats_mc.Healthbar_mc");
	// BUFFS & DEBUFFS 
	buff1 = GetVariableObject("_root.buff1_mc");
	buff2 = GetVariableObject("_root.buff2_mc");
	buff3 = GetVariableObject("_root.buff3_mc");
	buff1.SetVisible(false);
	buff2.SetVisible(false);
	buff3.SetVisible(false);
	debuff1 = GetVariableObject("_root.debuff1_mc");
	debuff1name = GetVariableObject("_root.debuff1_mc.debuffName1");
	debuff1time = GetVariableObject("_root.debuff1_mc.debuffTime1");
	debuff2 = GetVariableObject("_root.debuff2_mc");
	debuff2name = GetVariableObject("_root.debuff2_mc.debuffName2");
	debuff2time = GetVariableObject("_root.debuff2_mc.debuffTime2");
	debuff3 = GetVariableObject("_root.debuff3_mc");
	debuff3name = GetVariableObject("_root.debuff3_mc.debuffName3");
	debuff3time = GetVariableObject("_root.debuff3_mc.debuffTime3");
	debuff1.SetVisible(false);
	debuff2.SetVisible(false);
	debuff3.SetVisible(false);
	buffname1 = GetVariableObject("_root.buff1_mc.buffName1");
	buffname2 = GetVariableObject("_root.buff2_mc.buffName2");
	buffname3 = GetVariableObject("_root.buff3_mc.buffName3"); 
	bufftime1 = GetVariableObject("_root.buff1_mc.bufftime1"); 
	bufftime2 = GetVariableObject("_root.buff2_mc.bufftime2");  
	bufftime3 = GetVariableObject("_root.buff3_mc.bufftime3"); 
 // NOTIFICATION

Notification = GetVariableObject("_root.Notification");
    


	XPBarMC = GetVariableObject("_root.PlayerStats_mc.XP_mc");
	PlayerlevelTXT = GetVariableObject("_root.PlayerStats_mc.Leveltxt");
	EnergyB = GetVariableObject("_root.Playerneeds_mc.Energy_mc");
	ThirstB = GetVariableObject("_root.Playerneeds_mc.Thirst_mc");
	HungerB = GetVariableObject("_root.Playerneeds_mc.Hunger_mc");
	WeaponDB = GetVariableObject("_root.WeaponStats_mc.WpnDurability_mc");
	StaminaBar = GetVariableObject("_root.SprintStats_mc.SprintBar_mc");
    LastHealthpc = -201;
	ThirstPC = -201;
	EnergyPC = -201;
	HungerPC = -201;
	XPPC = -201;
	WeaponDPC = -201; // cache up all the floats
	StaminaPC = -201;
	
	TooltipIcon = GetVariableObject("_root.Tooltip2.Tooltip_MC.Icon");
	TooltipItem = GetVariableObject("_root.Tooltip2.Tooltip_MC.ItemName");
	Tooltip = GetVariableObject("_root.Tooltip2");
	
	RootMC = GetVariableObject("_root");
	HFrame = GetVariableObject("_root.HFrame");
	Crosshair = GetVariableObject("_root.Crosshair");
	// durability text
	Durability = GetVariableObject("_root.WeaponStats.WDTXT");
	


}

event bool WidgetInitialized(name WidgetName, name WidgetPath, GFxObject Widget)
{    


    switch(WidgetName)
    {                 
		case ('WeaponIcon'):
			WeaponIcon = GFxClikWidget(Widget);
			break;

			
			case ('ActionIcon'):
			NTActionIcon = GFxClikWidget(Widget);
			break;
			default:
			
			
    }
    return true;
}


function Shutdown()
{
self.Close(true);
}

function int roundNum(float NumIn) 
{
local int iNum;
local float fNum;

fNum = NumIn;
iNum = int(fNum);
fNum -= iNum;
if (fNum >= 0.5f) 
{
return (iNum + 1);
}
else 
{
return iNum;
}
}

// Function to return a percentage from a value and a maximum
function float getpercentage(int val, int max)
{
PC = tfpplayercontroller(GetPC());
if(PC.char != none)
{
return (float(val) / float(max)) * 100.0f;
}


}




function ToggleHelpFrame(bool val)
{
 if(val == true)
 {
  HFrame.SetVisible(true);
  //PC.PlaySound(OpenMenuSnd);
  
 }
 
 if(val == false)
 {
  HFrame.SetVisible(false);
 }
 
}








/*
================================================
ADD NOTIFICATION(STRING TXT,INT NUMB,INT FADETIME);

Function that allows to display"Notifications"
with a custom message,number (mostly earned XP)
and fade time 
================================================
*/



EXEC function AddNotification(string Action, string Spec,int XP,int SpecLevel,string Icon)
{

  ActionS = Action;
  SpecS = Spec;
  SpecIcon=Icon;
  SpecXP = XP;
  Level = SpecLevel;
  Notification.GotoAndPlay("fadeIn");




}

exec function InitUpperElement()
{
  UpperElement = GetVariableObject("_root.Notification");
  NTAction = GetVariableObject("_root.Notification.UpperElement.ActionTxt");
  NTSpec =GetVariableObject("_root.Notification.UpperElement.SpecTxt");
  NTAction.SetText(ActionS);
  NTSpec.SetText(SpeciCON);
  NTActionIcon.SetString("source","img://EditorResources.ActionCamPause");
  `log("Upper element called");
  
  
}

exec function InitLowerElement()
{
`log("I JUST GOT C0000LED"); // THE FUNCTION IS ACTUALLY CALLED
	NTXP = GetVariableObject("_root.Notification.LowerElement.XPTXT");
    NTXP.SetText("XP"   $   SpecXP);
	SpecXPBar = GetVariableObject("_Root.Notification.LowerElement.XPBar.Bar");
	SpecXPBar.SetFloat("_xscale",SpecXP);
}

exec function InitSpecLevel()
{
`log("I JUST GOT C0000LED TOOOOO");
NTXP.SetText("Level" $ Level);


}






/*
=======

tooltip, show and hide as well updating

=====
*/

function ToggleTooltip(bool val,SV_ItemArchetype Item)
{

  if(val == true && bUpdateTooltip == false)
  {
  	Tooltip.GotoAndPlay("show");
  	bUpdateTooltip = true; // the tooltip IS active so it is being updated
  }

  if(val == false && bUpdateTooltip == true)
  {
  	Tooltip.GotoAndPlay("hide");
  	bUpdateTooltip = false; // aint gonna update now
  }
 
 

}


function SetTooltipItem(string Icon,string ItemName,optional int Health)
{


 
TooltipItem.SetText(ItemName @Health);
TooltipIcon.SetString("source",Icon);
}



function AddBuffHUD(string BuffName,int BuffDuration,bool bDebuff)
{

PC = tfpplayercontroller(GetPC());

if(PC.char.Buffs == 1) // Buffs handling
{
 // buff 1 stuff
 buff1.SetVisible(true);
 buffname1.SetString("text",BuffName);
 bufftime1.SetText(BuffDuration);
}

  if(PC.char.Buffs == 2)
  {
   // buff 2 stuff
   buff2.SetVisible(true);
   buffname2.SetString("text",BuffName);
   bufftime2.SetText(BuffDuration);
  }
  
   if(PC.char.Buffs == 3)
   {
    // Buff 3 stuff
	buff3.SetVisible(true);
   
   }
 

}



/*
================================================
WEAPON ICON ON HUD AND DURABILITY

Changes the weapon icon in the UI to ..well the current weapon one
also shows durability trough the circle bar thing
================================================
*/

function SetWeaponIcon(string Icon)
{
local string testone;

testone = "img://EditorResources.ActionCamPause";
WeaponIcon.setstring("source",testone);

`Log("CALLED FUNCTION");
}








//Called every update Tick
simulated function TickHUD()
{
local tfppawn p;
p = tfpPawn(PC.Pawn);


    /*
    =============
    The client doesnt has access to sapitu character, neither his HUD
    So we just grab the vars from the pawn instead
    ==============
    */

     if(PC.Role < Role_Authority)
     {
        if(tfpPawn(pc.pawn).CharName != "")
        {
        HealthbarMC.SetFloat("_xscale", Pc.Pawn.Health);
        ThirstB.SetFloat("_yscale", tfpPawn(PC.Pawn).Thirst); 

        EnergyB.SetFloat("_yscale", tfpPawn(PC.Pawn).Energy );

        HungerB.SetFloat("_yscale",tfpPawn(PC.Pawn).hunger);
        }
     }


  /*
  ========================
  The server has full access to the spaitu character
  so there is no need to access the pawn variables since these are
  just for the client, or well the OWNING client
  =========================
  */

 if(PC.Role == Role_Authority) // server
 {
  HealthbarMC.SetFloat("_xscale", PC.Pawn.Health);

  ThirstB.SetFloat("_yscale", tfpPawn(pc.pawn).pchar.Thirst); //xscale

  EnergyB.SetFloat("_yscale", tfpPawn(pc.pawn).pchar.Energy );

  HungerB.SetFloat("_yscale",tfpPawn(pc.pawn).pchar.hunger);

  //`log("HUD:Server hunger is"$tfpPawn(PC.Pawn).pchar.Hunger);
 }


}

 
  





defaultproperties
{

	WidgetBindings.Add((WidgetName="WeaponIcon",WidgetClass=class'GFxClikWidget'))
	WidgetBindings.Add((WidgetName="ActionIcon",WidgetClass=class'GFxClikWidget'))

     WidgetBindings(0)={(WidgetName=buff1txt,WidgetClass=class'GFxClikWidget')}
	  WidgetBindings(1)={(WidgetName=buff2txt,WidgetClass=class'GFxClikWidget')}
	  WidgetBindings(2)={(WidgetName=buff2txt,WidgetClass=class'GFxClikWidget')}
	  


	
	
	MovieInfo = SwfMovie'SURVIVORFlash.HUD'
	
	bIgnoreMouseInput = false
    bDisplayWithHudOff=false
	bEnableGammaCorrection = false
	bPauseGameWhileActive = false
	bCaptureInput = false;
	bUpdateTooltip = false;;
	
}
