/**
 * Copyright 1998-2012 Epic Games, Inc. All Rights Reserved.
 */
class SVPlayerReplicationInfo extends PlayerReplicationInfo;
 // FOR THIS PLAYER
struct Character
{
	var string CharacterName;
	var int XP;
	var int XPGatheredForNextLevel;
    var int XPRequiredForNextLevel;
	var int Level;
	var int XPToCurrentLevel;
 

	var int Hunger,MaxHunger;
	var int Thirst,MaxThirst;
	var int Energy,MaxEnergy;
	var int HungerRate,ThirstRate;
	var int EnergyRate;



	var float Stamina;
	var float MaxStamina;
	var float StaminaRegRate;

	// after stats the inventory comes in
	var array<SV_ItemArchetype>  inventory;
	var array<SV_BuildingArchetype>  StructureList;
	var array<SV_Recipe>  Recipes;

};

var Repnotify Character Char;


simulated function GiveXP(int amount)
{
	`log(" CONTROLLER:ON GIVE XP");


   if(char.CharacterName != "") 
   {
	char.XP += amount;

	CalculateLevelProgress();

	while (char.XPGatheredForNextLevel >= char.XPRequiredForNextLevel && char.Level < 80)
	{
		char.Level++;
		
		// Recalculate level progress after leveling up
		CalculateLevelProgress();
	}
}
}


 private function CalculateLevelProgress()
{
	local int xpToCurrentLevel; // Total amount of XP gathered with current and previous levels
	if(char.CharacterName != "")
	{
	xpToCurrentLevel = 0.5*char.Level*(char.Level-1)*500;
	char.XPGatheredForNextLevel = char.XP - xpToCurrentLevel;
	char.XPRequiredForNextLevel = char.Level * 500;
    }
}

simulated function logxp()
{
		`log("wexpws" $char.xp);
}

simulated exec function createChur(string num)
{
	Char.CharacterName = num;
	Char.XP = 0;
	char.Level = 1;
    char.Hunger = 100;
    char.MaxHunger = 100;
    char.HungerRate = 5;
    char.Thirst = 100;
    char.MaxThirst = 100;
    char.ThirstRate = 5;
    char.Energy = 100;
    char.MaxEnergy = 100;
    char.EnergyRate = 5;

	char.Stamina = 100;
	char.MaxStamina = 100;
	//settimer(char.HungerRate,true,'HungerTimer');
	//settimer(char.ThirstRate,true,'ThirstTimer');
	//settimer(char.EnergyRate,true,'EnergyTimer');



}

/*
===============

CTryToSpawnStructure ( Item, Build)

Its a function where the client makes all the check whatever
it can spawn the structure or not
===============
*/

simulated function CTryToSpawnStructure(string Item,string Build)
{
	local int i;
	local tfpPawn tfp;
	
	for (i = 0; i <char.inventory.length && i < 32; i++)
    {
   	if(char.inventory[i].itemName == Item)
   	{
   		foreach WorldInfo.AllPawns(class'tfpPawn', tfp)
	    {
		if(tfp.PlayerReplicationInfo == self)
		{
		tfpPlayerController(tfp.Controller).CSpawnBuild(Build);
		return; // we got our thing now return
		break;
     	}
    	}
   	}   

}
}

simulated function SendDropToServer(string item)
{
	local int i;
	local tfpPawn tfp;
	
	for (i = 0; i <char.inventory.length && i < 32; i++)
    {
   	if(char.inventory[i].itemName == item)
   	{
   		foreach WorldInfo.AllPawns(class'tfpPawn', tfp)
	    {
		if(tfp.PlayerReplicationInfo == self)
		{
		tfpPlayerController(tfp.Controller).ServerSpawnItem(char.inventory[i].itemName,self);
		char.inventory.removeItem(char.inventory[i]);
		return; // we got our thing now return
		break;
   	}   
   }
}
}
}

simulated function GiveItem(SV_ItemArchetype Act)
{
	char.inventory.additem(act);
}







simulated function loginv()
{
	`log("CLIENT: Picked Item Was "$char.inventory[0].itemName);
}

simulated function logchar()
{
	`log(" Char name is"$char.charactername);
}

replication
{
	if (bNetDirty)
		Char;
}

simulated function AddItem(SV_ItemArchetype item)
{
	local tfpPawn tfp;
	foreach WorldInfo.AllPawns(class'tfpPawn', tfp)
	{
		if(tfp.PlayerReplicationInfo == self)
		{
			`log(" got player replication info");
		}
	}
    			//`log(" CLIENT: Received Item name" $item.itemName);
    	
    

	//char.inventory[char.inventory.Length].AddItem(Item);

	//`log(" added item was " $Item.itemName);
	//`log(" added item inventory " $Ichar.inventory[0].itemName);

}

simulated function test(sv_itemarchetype t)
{

	char.inventory.additem(t);
`log(t.itemName);
}




simulated function HungerTimer()
{
	//`log(" Item in inventory is"$char.inventory[0].itemName);
char.Hunger -= 10;
//`log(" added item inventory " $char.inventory[0].itemName);
}

simulated function ThirstTimer()
{
char.Thirst -= 10;
}

simulated function EnergyTimer()
{
char.Energy -= 10;
}

/*
===========
building
============
*/


function bool AddBuild(SV_BuildingArchetype b)
{
  local int i;
  char. StructureList.addItem(b);
  i = char.StructureList.find(B);
  if (i != INDEX_NONE) return false;
 char. StructureList.addItem(b);
 `log("added build into the client with name of " $b.BuildName);
  return true;
}




defaultproperties
{
	
}
