class TestPawn extends GamePawn
placeable;

simulated function PostBeginPlay()
{
	Super.PostBeginPlay();

	SpawnDefaultController();
	`log(">>>>>>>>>>>>>>> Spawned controller:"@Controller);
}

defaultproperties
{
	ControllerClass=class'TestAI'

	Begin Object Name=CollisionCylinder
		CollisionRadius=+0034.000000
		CollisionHeight=+0034.000000
		BlockZeroExtent=FALSE
	End Object

	Begin Object Class=SkeletalMeshComponent Name=NanoPawnMesh
	SkeletalMesh=SkeletalMesh'EngineMeshes.SkeletalCube'
	BlockZeroExtent=TRUE
	BlockNonZeroExtent=TRUE
	CollideActors=TRUE
	BlockRigidBody=TRUE
	RBChannel=RBCC_Pawn
	RBCollideWithChannels=(Default=TRUE,Pawn=TRUE,DeadPawn=TRUE,BlockingVolume=TRUE,EffectPhysics=TRUE,FracturedMeshPart=TRUE,SoftBody=TRUE)
	bIgnoreControllersWhenNotRendered=TRUE
	bUpdateSkelWhenNotRendered=FALSE
	MinDistFactorForKinematicUpdate=0.2
	bAcceptsStaticDecals=FALSE
	bAcceptsDynamicDecals=TRUE
	bAllowAmbientOcclusion=FALSE
	End Object
	Mesh=NanoPawnMesh
	Components.Add(NanoPawnMesh)
}