/**
 * Copyright 1998-2012 Epic Games, Inc. All Rights Reserved.
 */

class SVDmgType_Sharp extends UTDamageType
	abstract;

defaultproperties
{

	DamageWeaponClass=class'SVWeap_Machete'
	DamageWeaponFireMode=0

	KDamageImpulse=1500.0

	DamageCameraAnim=CameraAnim'Camera_FX.ShockRifle.C_WP_ShockRifle_Hit_Shake'

}
