class GFxCrafting extends GFxMoviePlayer;


var array<string> ItemList;
var array<SV_ItemSlot> invSlots;
var SV_ItemSlot ingSlot1,ingSlot2,ingSlot3,Outcome;
var GFxClikWidget CraftBtn;
var tfpplayercontroller P;
var GFxObject Count,Count2,Count3;
var string ItemToCraft;
var bool bOutcome;

function Init(optional LocalPlayer player)
{	
	super.Init(player);
    Start();
    Advance(0);
P = tfpplayercontroller(GetPC());

AddFocusIgnoreKey('I');
AddFocusIgnoreKey('Escape');
AddFocusIgnoreKey('i');
Start();
Advance(0);

Count = GetVariableObject("_root.Count1");
Count2 = GetVariableObject("_root.Count2");
Count3 = GetVariableObject("_root.Count3");

Count.SetText("");
Count2.SetText("");
Count3.SetText("");

P = tfpplayercontroller(GetPC());
   Populate();


}



event bool WidgetInitialized(name WidgetName, name WidgetPath, GFxObject Widget)
{

    

    switch(WidgetName)
    {             

    
		case ('CraftBtn'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    CraftBtn = GFxClikWidget(Widget);
		CraftBtn.AddEventListener('CLIK_Press',CraftIt);

		break;

		case ('slot0'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    invSlots[0] = SV_ItemSlot(Widget);
		break;

		case ('slot1'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    invSlots[1] = SV_ItemSlot(Widget);
		//CraftBtn.AddEventListener('CLIK_itemPress',InvPressed);
		break;

		case ('slot2'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    invSlots[2] = SV_ItemSlot(Widget);
		//CraftBtn.AddEventListener('CLIK_itemPress',InvPressed);
		break;

		case ('slot3'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    invSlots[3] = SV_ItemSlot(Widget);
		//CraftBtn.AddEventListener('CLIK_itemPress',InvPressed);
		break;

		case ('slot4'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    invSlots[4] = SV_ItemSlot(Widget);
		//CraftBtn.AddEventListener('CLIK_itemPress',InvPressed);
		break;

		case ('slot5'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    invSlots[5] = SV_ItemSlot(Widget);
		//CraftBtn.AddEventListener('CLIK_itemPress',InvPressed);
		break;

		case ('slot6'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    invSlots[6] = SV_ItemSlot(Widget);
		//CraftBtn.AddEventListener('CLIK_itemPress',InvPressed);
		break;

		case ('slot7'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    invSlots[7] = SV_ItemSlot(Widget);
		//CraftBtn.AddEventListener('CLIK_itemPress',InvPressed);
		break;

		case ('slot8'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    invSlots[8] = SV_ItemSlot(Widget);
		//CraftBtn.AddEventListener('CLIK_itemPress',InvPressed);
		break;

		case ('slot9'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    invSlots[9] = SV_ItemSlot(Widget);
		//CraftBtn.AddEventListener('CLIK_itemPress',InvPressed);
		break;


		case ('slot10'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    invSlots[10] = SV_ItemSlot(Widget);
		//CraftBtn.AddEventListener('CLIK_itemPress',InvPressed);
		break;

			case ('ingSlot0'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    ingSlot1 = SV_ItemSlot(Widget);
		ingSlot1.AddEventListener('CLIK_drop',ItemDropped);
		break;

			case ('ingSlot1'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    ingSlot2 = SV_ItemSlot(Widget);
		ingSlot2.AddEventListener('CLIK_drop',ItemDropped2);
		break;

			case ('ingSlot2'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    ingSlot3 = SV_ItemSlot(Widget);
		ingSlot3.AddEventListener('CLIK_drop',ItemDropped3);
		break;

			case ('Outcome'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
		Outcome = SV_ItemSlot(Widget);

        default:
		
            break;
    }
   
  
    return true;
}


function Populate()
{

	local int i;	


	for (i = 0; i < P.char.inventory.Length; i++)
	{
		  `log("items in inventory are"$p.char.inventory[i].itemName);
		invSlots[i].SetSlotData("item",p.char.inventory[i].assetName,p.char.inventory[i].itemName);

	}
}

function ItemDropped(gfxclikwidget.eventdata ev)
{
	  local GFxObject Data;
  local string Type;
  //Index = ev.index;

  Data = ev.target.GetObject("data");
  Type = Data.getstring("Item");
    ItemList.AddItem(Type);

  `log("item is "$Type);

   CheckOutcome();
      

	
}

function ItemDropped2(gfxclikwidget.eventdata ev)
{
	  local GFxObject Data;
  local string Type;

  //Index = ev.index;

  Data = ev.target.GetObject("data");
  Type = Data.getstring("Item");
    ItemList.AddItem(Type);

  `log("item is "$Type);

   CheckOutcome();
      

	
}


function ItemDropped3(gfxclikwidget.eventdata ev)
{
	  local GFxObject Data;
  local string Type;

  //Index = ev.index;

  Data = ev.target.GetObject("data");
  Type = Data.getstring("Item");

  ItemList.AddItem(Type);
    `log("item is "$Type);

   CheckOutcome();
      

	
}



function CheckOutcome()
{
  local GFxObject Data;
  local string Type;

  Data = ingSlot1.GetObject("data");
  Type = Data.getstring("Item");
   
    if(ItemList[0] != "") // there is a item
    {


    	if(ItemList[1] != "") // second slot has a item too
    	{


    		if(ItemList[2] != "") // third slot has an item too ! show up the income
    		{
    			ItemList.AddItem(Type);
    			ShowOutcome();
    		}
    	}
    }
}


function ShowOutcome()
{
	local int i;
	local int j;
	local int v;

	for (i = 0; i < P.itemDB.RecipeList.Length; ++i)
	{

	  	for (v = 0; v < P.itemDB.RecipeList[i].recItems.Length; ++v)
	    {

	  	  for (j = 0; j < ItemList.Length; ++j)
	      {

	      	if(P.itemDB.recipeList[i].recItems[v].itemName == ItemList[j]) // if an recipe matches all our items
            {
            	Outcome.SetSlotData("result",P.itemDB.recipeList[i].itemResult.assetName,P.itemDB.recipeList[i].itemResult.itemName);
                bOutcome = true;
                ItemToCraft = P.itemDB.recipeList[i].itemResult.itemName;
                break;
            }
          }
        }
    }


}

function CraftIt(GFxClikWidget.EventData ev)
{
	local SV_ItemArchetype itm;
	local tfpGame gm;
	gm = tfpGame(P.WorldInfo.Game);
	itm = gm.DB.GetItem(ItemToCraft);

  if(bOutcome)
  {
    P.CraftItem(itm);
    Close(true);
  }

}









defaultproperties
{   

WidgetBindings.Add((WidgetName="CraftBtn",WidgetClass=class'GFxClikWidget'))
     WidgetBindings.Add((WidgetName="slot0",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="slot1",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="slot2",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="slot3",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="slot4",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="slot5",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="slot6",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="slot7",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="slot8",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="slot9",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="slot10",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="ingSlot0",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="ingSlot1",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="ingSlot2",WidgetClass=class'SV_ItemSlot'))
     WidgetBindings.Add((WidgetName="Outcome",WidgetClass=class'SV_ItemSlot'))




	/** If TRUE, this movie player will be allowed to accept focus events. Defaults to TRUE */

	/** If TRUE, the game will pause while this scene is up */
	//bPauseGameWhileActive = true;

	/** If TRUE, only the LocalPlayerOwner's input can be directed here */

	/** If TRUE, this movie player will capture input */
	bCaptureInput = true;

	/** IF TRUE, this movie player will ignore mouse input */
	bDisplayWithHUDOFF = true;
	MovieInfo=SwfMovie'SURVIVORFlash.CraftingMenu'
	
	
}