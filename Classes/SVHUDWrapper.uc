/* This is the HUD wrapper. Its job is to instantiate the HUD and to tell it to update after each frame is rendered. */

class SVHudWrapper extends UTHUDBase;

/** Movie */
var SVHud   			HudMovie;
/** Class of Movie object */
var class<SVHud> 		HUDClass;

var GFxObject 			HudMovieSize;

var GFxInventory Inventory; // inventory movie

/** Multiplayer Menu **/

var GFxMultiplayer MPMovie;

/** Mouse Movie **/

var GFxMouse MouseMovie;

/** Building movie **/

var GFxBuilding BuildMovie;

var float SeekLength;// tooltip seek length;

var GFxQuest QuestMovie;

var GFxWoundMenu WoundMovie;

var GFxWoundStatus WStatusMovie;

var GFxCrafting CraftMovie;

VAR GFxCraftingUI CraftUIMovie;

var GFxCharacterFrame CharFrameMovie;

var GFxContextMenu ContextMovie;



simulated function PostBeginPlay()
{
	Super.PostBeginPlay();
	CreateHUDMovie(); 
TfpPlayerController(PlayerOwner).MusicSystem = Spawn(class'SV_MusicManager',tfpPlayerController(PlayerOwner));

}


simulated event Destroyed()
{
	Super.Destroyed();

	  if (HudMovie != none) 
	 {
   //Get rid of the memory usage of HudMovie
   HudMovie.Close(true);
   HudMovie = none;
   }
}


/*
============

Helper function to open new frames 
from the Context Menu

================
*/

function OpenNewFrame(name Frame)
{

	SetTimer(0.1,false,Frame);
	`log("OPENED NEW FRAME");
}


/*
==================
HUD Movie
HUD Initializations and close
===================
*/

function CreateHUDMovie()
{
	HudMovie = new class'SVHud';

	HudMovie.SetTimingMode(TM_Real);
	HudMovie.Init();
	HudMovie.Start();
	HudMovie.SetViewScaleMode(SM_ExactFit);
	HudMovie.SetAlignment(Align_TopLeft);
	
}

exec function CloseHUD()
{
HudMovie.Close(true);
   HudMovie = none;
}


/*
===============

Inventory

================
*/

EXEC function OpenInventory()
{
	 if(Inventory != none)
	 {
	 	Inventory.Close(true);
	 	Inventory = none;
	 	return;
	 	//ToggleMouse(false);
	 }
	 
	 if(Inventory == none)
	 {
	 
	   Inventory= new class'GFxInventory';
	   Inventory.Init();
	   Inventory.Start();
	   Inventory.InitMovie();
	  // ToggleMouse(true);
	 }
	 `log("Inventory Movie is "$Inventory);
       
	
}


/*
==============

Context Menu

============
*/

exec function ShowContextMenu()
{
	 if(ContextMovie != none)
	 {
	 	ContextMovie.Close(true);
	 	ContextMovie = none;
	 	return;
	 	//ToggleMouse(false);
	 }
	 
	 if(ContextMovie == none)
	 {
	 
	   //ContextMovie= new class'GFxContextMenu';
	     //ContextMovie.HUD = self;
	  // ContextMovie.Init();
	   //ContextMovie.Start();

	  // ToggleMouse(true);
	 }

	
}

exec function HideContextMenu()
{
	ContextMovie.CloseMenu();
}


/*
===============

Quest Frame

================
*/

function OpenQuestFrame()
{
	QuestMovie=new class'GFxQuest';
	QuestMovie.Init();
}


/*
=============

Multiplayer

=============
*/

exec function ToggleMP()
{
	MPMovie = new class'GFxMultiplayer';
	MPMovie.Init();
	MPMovie.Start();
	ToggleMouse(true);

}


/*
==============

Mouse Movie

==============
*/

exec function ToggleMouse(bool val)
{
	if(val == true)
	{
	  MouseMovie = new class'GFxMouse';
	  MouseMovie.SetTimingMode(TM_Game);
      MouseMovie.Init();
      MouseMovie.MyHUD = self;
    }

     if(val == false)
     {
     	MouseMovie.Close(true);
     }
}


/*
============
Building Movie
============
*/

exec function ToggleBuild()
{


	if(BuildMovie != none)
	{
		BuildMovie.Close(true);
		BuildMovie = none;
	}
	else
	   BuildMovie = new class'GFxBuilding';
	   BuildMovie.Init();
	   BuildMovie.Start();


	 //HUDMovie.Close(true);
}

/*
===========

Crafting Menu ( MENU NOT QTE)

=============
*/

exec function ToggleCrafting()
{
	 if(CraftMovie.bMovieIsOpen)
	 {
	 	//CraftMovie.Close(true);
	 	//ToggleMouse(false);
	 }

	 
	   CraftMovie= new class'GFxCrafting';
	   CraftMovie.Init();
	   CraftMovie.Start();
	   	CraftMovie.SetViewScaleMode(SM_ShowAll);
	    HudMovie.SetAlignment(Align_TopLeft);
	  // ToggleMouse(true);
       
	
}

/*
===========

Crafting User Interface
With QTES

=============
*/

exec function InitCraftingUI(optional string Item)
{
	 if(CraftUIMovie.bMovieIsOpen)
	 {
	 	CraftUIMovie.Close(true);
	 	//ToggleMouse(false);
	 }

	 
	   CraftUIMovie= new class'GFxCraftingUI';
	   CraftUIMovie.Init();
	   CraftUIMovie.Start();
	   CraftUIMovie.SetViewScaleMode(SM_ShowAll);
	  // ToggleMouse(true);
       
	
}

/*
===========
character frame
=============
*/

exec function ToggleCharFrame()
{
	if(CharFrameMovie.bMovieIsOpen)
	{
		CharFrameMovie.Close(true);
	}
	else
    
    CharFrameMovie = new class'GFxCharacterFrame';
    CharFrameMovie.Init();
    CharFrameMovie.Start();
    CharFrameMovie.SetViewScaleMode(SM_ShowAll);
}


/*
==========
WoundMovies

============
*/

exec function ToggleWoundStatus()
{
	WStatusMovie = new class'GFxWoundStatus';
	WstatusMovie.Init();
	WStatusMovie.Start();
}


simulated event PostRender()
{

 	local Actor HitActor;
	local Vector HitLocation, HitNormal, EyeLocation;
	local Rotator EyeRotation;
  local tfpPawn PawnOwner;

  	super.PostRender();


	if (HudMovie != none)
	{
		HudMovie.TickHud();
	}

	// Ensure the player owner is set
	if (PlayerOwner != None)
	{
		// Retrieve the player camera location and rotation
		PlayerOwner.GetActorEyesViewPoint(EyeLocation, EyeRotation);

		// Perform a trace to find what the player is looking at
  		// Use trace actors so that we can ignore unimportant objects
		ForEach TraceActors(class'Actor', HitActor, HitLocation, HitNormal, EyeLocation + Vector(EyeRotation) * 80, EyeLocation, Vect(1.f, 1.f, 1.f),, TRACEFLAG_Bullet)
		{
			// If the hit actor is the player owner, the player owner's pawn or the hit actor isn't visible
                   if(HitActor != PlayerOwner.Pawn) // if it isnt our pawn and neither world geometry
				   tfpplayercontroller(playerowner).bStarringAtActor = true;
                   tfpplayercontroller(PlayerOwner).StarringAtActor = HitActor;
                   DrawDebugLine(EyeLocation, HitLocation,255,0,0,true);
                  //`log("hit actor is"$hitactor);
        }
    }
}



/*
============
item tooltip
============
*/

simulated function DrawItemTooltip()
{
	local vector HitLocation, HitNormal, StartTrace, EndTrace;
	local actor HitActor;
	local tfpPawn PawnOwner;
	PawnOwner = tfpPawn(tfpPlayerController(PlayerOwner).Pawn);
	

	
	StartTrace = PawnOwner.Location;
	StartTrace.Z += PawnOwner.EyeHeight;

	EndTrace = StartTrace + SeekLength * vector(PlayerOwner.Rotation);
	HitActor = PawnOwner.Trace(HitLocation, HitNormal, EndTrace, StartTrace, true, vect(0,0,0),, TRACEFLAG_Bullet);

		if(SV_ItemArchetype(HitActor) != none ) // we are starring at a item
		{
          HUDMovie.ToggleTooltip(true,SV_ItemArchetype(HitActor));

		}


}

/*
=======
To Handle QTE / INPUT

==========
*/

function PassKeyPressed(name K)
{
	tfpplayercontroller(PlayerOwner).KeyPressed(k);
}

function PassKeyReleased(name KK)
{
	tfpplayercontroller(pLAYERoWNER).KeyReleased(KK);
}



defaultproperties
{
	SeekLength = 40;


}
