class SV_ArmorItem extends SV_ItemArchetype
	HideCategories(Attachment, Physics, Object,KActor)
	placeable;

// physical item representation

/*
=====================
Item Defininition and parameters first

==================
*/

var (Armor) int Upgrades,MaxUpgrades;
var bool Equipped;
var (Armor) name AttachmentSocket;
var(Armor) int ArmorIndex; // Int to know its location 0 = head ,1 = chest etc
var (Armor) bool HeadArmor, ChestArmor;
var (Armor) bool LArmArmor, RArmArmor;
var (Armor) bool LLegArmor,RLegArmor;
var (Armor) float Defense;
var (Armor) SkeletalMesh Model;
var (Armor) PhysicsAsset PhysAsset;


DefaultProperties
{
	bCollideActors = true;
	bWakeOnLevelStart = true;
	bDamageAppliesImpulse = true;
	//Network
	//bOnlyDirtyReplication=true
   RemoteRole=ROLE_SimulatedProxy
	
}



