class SVWeapon extends tfpWeapon;


var int MagAmmo; // ammo in our current magazine
var float Condition; // used to determine jam chance
var float JamChance;
var bool Jammed;

var bool Reloading;

var float IronSightSpread;
var float IronSightSpeed;

