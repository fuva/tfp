class SeqAct_AddRecolectionTask extends SequenceAction;
 
 var() string Quest; // Amount of XP this action will give
 var() string ItemToGet;
 var() int ItemAmount;

event Activated()
{
local tfpPlayerController PC;

PC = tfpPlayerController(GetWorldInfo().GetALocalPlayerController());

 PC.Quests.AddRecolectionTask(Quest,ItemToGet,ItemAmount);

}
	
DefaultProperties
 {
 	// Name that will apear in the Kismet Editor
 	ObjName="Add Recolection Task"
 	 	ObjCategory="Quests "

 }