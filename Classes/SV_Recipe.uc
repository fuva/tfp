class SV_Recipe extends Object
placeable;

var(Recipe) string RecipeName;
var(Recipe) string RecipeDesc;
var(Recipe) array<SV_ItemArchetype> recItems; // items needed to craft this recipe
var(Recipe) bool IsBuild;
var(Recipe) bool IsItem;
var(Recipe) SV_ItemArchetype ItemResult;
var(Recipe) SV_BuildingArchetype StructureResult;


defaultproperties
{

 

}