class GFxQuest extends GFxMoviePlayer;

var GFxObject NewMissionMC;
var GFxObject QuestTitle;
var GFxObject QuestDesc;
var GFxObject QuestObj;
var GFxObject Tag;
var GFxObject QuestFrame;
var array<GFxObject> Objectives;
var array<GFxObject> Status;
var int ObjectiveIndex;

struct Objective
{
	var string ObjName;
	var int Index;
	var bool IsRecolect;
	var bool IsExplore;
	var bool IsKill;

};

var array<Objective> QuestsObj;



function Init(optional LocalPlayer player)
{
	 super.Init(player);
    Start();
    Advance(0);

NewMissionMC = GetVariableObject("_root.NewMission");
QuestFrame = GetVariableObject("_root.QuestFrame");

NewMissionMC.GotoAndPlay("show");
QuestTitle = GetVariableObject("_root.QuestFrame.QFrame.Title");
QuestDesc = GetVariableObject("_root.QuestFrame.QFrame.Desc");
Objectives[0] = GetVariableObject("_root.QuestFrame.QFrame.Obj");
Objectives[1] = GetVariableObject("_root.QuestFrame.QFrame.Obj2");
Objectives[2] = GetVariableObject("_root.QuestFrame.QFrame.Obj3");

Status[0] = GetVariableObject("_root.QuestFrame.QFrame.Status");
Status[1] = GetVariableObject("_root.QuestFrame.QFrame.Status2");
Status[2] = GetVariableObject("_root.QuestFrame.QFrame.Status3");

}

exec function ShowQFrame()
{
	QuestFrame.GotoAndPlay("show");
}

function UpdateObjectives(string text,int Index)
{
     Objectives[Index].SetText(text);
}

function UpdateObjective(int oIndex, string obj)
{
	Objectives[oIndex].SetText(obj);
}

function UpdateObjectiveStatus(int oIndex, int n)
{
	Status[oIndex].SetText(n);
}



function AcquireQuest(Quest Q)
{
	QuestTitle.SetText(Q.QuestName);
	QuestDesc.SetText(Q.QuestDescription);
	Objectives[0].SetText(Q.QuestObjectives[0].OBjname);
	Objectives[1].SetText("");
	Objectives[2].SetText("");

}


DefaultProperties
{
	//defaults
	bCaptureInput = false;
	MovieInfo=SwfMovie'SURVIVORFlash.QuestFrame';
    
}

