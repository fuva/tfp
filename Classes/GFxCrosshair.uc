class GFxCrosshair extends GFxMoviePlayer;

var GFxObject NewMissionMC;
var GFxObject QuestTitle;
var GFxObject QuestDesc;
var GFxObject QuestObj;
var GFxObject Tag;
var GFxObject QuestFrame;
var array<GFxObject> Objectives;
var array<GFxObject> Status;
var int ObjectiveIndex;

struct Objective
{
	var string ObjName;
	var int Index;
	var bool IsRecolect;
	var bool IsExplore;
	var bool IsKill;

};

var array<Objective> QuestsObj;



function Init(optional LocalPlayer player)
{
	 super.Init(player);
    Start();
    Advance(0);

}



DefaultProperties
{
	//defaults
	bCaptureInput = false;
	MovieInfo=SwfMovie'SURVIVORFlash.QuestFrame';
    
}

