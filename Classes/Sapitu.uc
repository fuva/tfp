/**
 * Savegames Are Possible In The UDK. Well, sort of.
 *
 * By Michiel 'elmuerte' Hendriks for Epic Games, Inc.
 *
 * You are free to use this example as you see fit, as long as you
 * properly attribute the origin.
 */
class Sapitu extends Object dependsOn(SapituItem);



/**
 * Load a character using it's id
 *
 * @param charId
 *		The identifier for the character
 * @return The created character, or none if it could not be created/loaded
 */

 // DB = Database for loading character inventory and items

var SV_ItemDB DB;

function Init(SV_ItemDB NDB)
{
	DB = NDB;
}

function SapituCharacter loadCharacter(string charId)
{
	local SapituCharacter char;
	local SV_ItemArchetype inv;
	local int i;
	local array<string> chars;

	chars = getCharacters();
	if (chars.find(charId) == INDEX_NONE) return none;

	char = new(none, charId) class'SapituCharacter';
	if (char == none) return none;
	// load the inventory
    for (i = 0; i < char.inventoryRecords.length; ++i)
	{
		 inv = DB.GetItem(char.inventoryrecords[i]);
		 if(inv != none)
		 char.inventory.addItem(inv);
	}
	return char;
}

/**
 * Get a list of all existing character ids
 *
 * @return The list of character ids.
 */
function array<string> getCharacters()
{
	local array<string> res;
	local int i, idx;
	GetPerObjectConfigSections(class'SapituCharacter', res);
	// the result contains: "ObjectName ClassName" but we only need the ObjectNames
	for (i = 0; i < res.length; ++i)
	{
		idx = InStr(res[i], " ");
		if (idx != INDEX_NONE)
		{
			res[i] = left(res[i], idx);
		}
	}
	return res;
}

/**
 * Create a new character instance. None of it's fields have been initialized.
 * It is just created with a proper id, and not saved yet.
 *
 * @param charId
 *		Optionally character id to use. This must be unique.
 * @return The newly created character
 */
function SapituCharacter createCharacter(optional string charId = "Char_"$TimeStamp()$rand(100)$"_")
{
    // note: object names shouldn't end with a number
	local SapituCharacter char;
	charId -= " ";
	charId -= ":";
	charId -= "/";
	charId -= "-";
	char = new(none, charId) class'SapituCharacter';
	return char;
}

