class SvWeap_Machete extends MeleeWeaponBase;


DefaultProperties
{
	// Pickup staticmesh
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'SV_Machete.Mesh.SK_Machete'
		PhysicsAsset=PhysicsAsset'WP_Fireaxe.Mesh.SK_WP_Fireaxe_PhysicsN'
	End Object

	HolsterIndex = 0;
	EquipIndex = 4;
	
	WeaponIcon = "img://SV_Machete.Test"
	
	LightWeap = true;
	
	AttackStamina[0]=13
	AttackStamina[1]=19
	//FleshHitsound[0]=SoundCue'SV_Machete.Sounds.Machete_HitFlesh_I_Cue'
	//FleshHitsound[1]=SoundCue'SV_Machete.Sounds.Machete_HitFlesh_II_Cue'
	//FleshHitsound[2]=SoundCue'SV_Machete.Sounds.Machete_HitFlesh_III_Cue'
	//FleshHitsound[3]=SoundCue'SV_Machete.Sounds.Machete_HitFlesh_IV_Cue'
	
	//SwingSound[0]=SoundCue'SV_Sounds.Weapons.Swing_Air_I_Cue'

	Begin Object class=AnimNodeSequence Name=MeshSequenceA
	End Object
	WeaponName = "Machete"
	WeaponFireTypes(0)=EWFT_InstantHit

	PlayerAnimSets(0)=AnimSet'CH_TFP_Male.Anims.Anim_Light'
	InstantHitDamageTypes(0)=class'SVDmgType_Sharp'

	AttachmentClass=class'SVAttachment_Machete'
	  bCanBlock = true
	WeaponDurability = 100;
	InventoryGroup = 1;
	numWeaponTraces = 10;

}