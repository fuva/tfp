class SeqAct_GiveQuest extends SequenceAction;
 
 var() string Quest; // Amount of XP this action will give
 var() string Desc;
 var() String Objective;
 var() string Objective2;
 var() string Objective3;
 var() bool Activate;// Will the quest be activated right away?
 var() int XPReward;

var(Quest) array<string> Objectives;

event Activated()
{
local tfpPlayerController PC;

PC = tfpPlayerController(GetWorldInfo().GetALocalPlayerController());

 PC.GiveQuest(self);

}


	
DefaultProperties
 {
 	// Name that will apear in the Kismet Editor
 	 	ObjCategory="Quests"
 	ObjName="Give Quest"
    
 	// Name of the function that will be called when this action is triggered

 
 }