class GFxMouse extends GFxMoviePlayer;

var SVHUDWrapper MyHUD;

function Init(optional LocalPlayer LocalPlayer)
{
  // Initialize the ScaleForm movie
  Super.Init(LocalPlayer);

  Start();
  Advance(0);
}

event UpdateMousePosition(float X, float Y)
{
  local tfpplayerinput Input;

  
    Input = tfpplayerinput(MyHUD.PlayerOwner.PlayerInput);

    if (Input != None)
    {
     Input.SetMousePosition(X, Y);
    }
}


defaultproperties
{

  bDisplayWithHudOff=false
  TimingMode=TM_Game
  MovieInfo=SwfMovie'SURVIVORFlash.Mouse'
  bPauseGameWhileActive=false

}