class TestAI extends GameAIController;

var() Vector TempDest;
var vector nextlocation;
var UTWaterVolume vol;
var vector NavigationDestination;
VAR VECTOR _FINALDEST;
VAR VECTOR PATHDESTINATION;
var vector nextmovelocation;
var vector nextdest;

event PostBeginPlay()
{
	
	NavigationHandle = new(self) class'NavigationHandle';

	GotoState('Test');
	NavigationHandle.bDebugConstraintsAndGoalEvals = true;

super.PostBeginPlay();
}

event Possess(Pawn inPawn, bool bVehicleTransition)
{
    super.Possess(inPawn, bVehicleTransition);
    Pawn.SetMovementPhysics();
}

function bool FindNavMeshPath()
{
  // Clear cache and constraints (ignore recycling for the moment)
  NavigationHandle.PathConstraintList = none;
  NavigationHandle.PathGoalList = none;


  // Create constraints
  class'NavMeshGoal_Random'.static.FindRandom( NavigationHandle, 2048 );
  class'NavMeshPath_EnforceTwoWayEdges'.Static.EnforceTwoWayEdges( NavigationHandle );

  // Find path
  return NavigationHandle.FindPath();
}

auto State Test
{
    Begin:


    foreach ALLActors(class'UTWaterVolume', vol) 
  {
  // colliding X within 100 UU radius of someActor
     
       
     }

     if(vol != none)
     {
      GotoState('Moving');
     }
    
     if(FindNavMeshPath())
    {
        // Draw the path that the bot found
        FlushPersistentDebugLines();
        NavigationHandle.DrawPathCache(,true);
        
        // Grab the end goal point
        PathDestination = NavigationHandle.PathCache_GetGoalPoint();
        
        // While the bot is valid and has not reached the end goal move to the next
        // position along the path
        while(Pawn != none && !Pawn.ReachedPoint( PathDestination, none ))
        {
            if(NavigationHandle.GetNextMoveLocation( NextMoveLocation, Pawn.GetCollisionRadius() ))
            {
                if( !NavigationHandle.SuggestMovePreparation( NextMoveLocation, self))
                {
                    MoveTo(NextMoveLocation);
                }
            }
        }
    }
        Sleep( 0.f );
        GoTo('Begin');
}

state Moving
{
  function bool FindNavMeshPath()
    {
          // Clear cache and constraints (ignore recycling for the moment)
  NavigationHandle.PathConstraintList = none;
  NavigationHandle.PathGoalList = none;


  // Create constraints
  class'NavMeshGoal_Random'.static.FindRandom( NavigationHandle, 2048 );
  class'NavMeshPath_EnforceTwoWayEdges'.Static.EnforceTwoWayEdges( NavigationHandle );

  // Find path
  return NavigationHandle.FindPath();
    }
 
Begin:
    if( NavigationHandle.ActorReachable(vol))
    {
        // Si se puede llegar directamente hacerlo:
        MoveTo(vol.Location, vol, 32);
    }
    else if (FindNavMeshPath())
    {
        NavigationHandle.SetFinalDestination(vol.Location);
 
        if(NavigationHandle.GetNextMoveLocation(NextDest, Pawn.GetCollisionRadius()*5))
        {
            // Ir al siguiente punto del camino
            MoveTo(NextDest,none,128.0);
        }
        else
        {
            // No hay punto siguiente
            MoveToward(vol);
        }
    }
    else
    {
        // No se encuentra camino posible
        MoveToward(vol);
    }

}
