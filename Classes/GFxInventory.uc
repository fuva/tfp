class GFxInventory extends GFxMoviePlayer;

var tfpPlayerController PC;


var GFxClikWidget Drop,Use,InventoryList,Slot0,Slot1,Slot2;


var SoundCue ButtonClick,ButtonBack;
var SV_ItemArchetype CurrentItem;
var int Index;

function InitMovie()
{
PC = tfpplayercontroller(GetPC());

AddFocusIgnoreKey('I');
AddFocusIgnoreKey('Escape');
AddFocusIgnoreKey('i');



}

function UpdateInventory();




event bool WidgetInitialized(name WidgetName, name WidgetPath, GFxObject Widget)
{
    

    switch(WidgetName)
    {                 
		case ('sl'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    InventoryList = GFxClikWidget(Widget);
		InventoryList.AddEventListener('CLIK_itemPress',InvPressed);
		break;

		case ('DropItem'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    Drop = GFxClikWidget(Widget);
		Drop.AddEventListener('CLIK_Press',DropItem);
		break;

		case ('UseItem'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    Use = GFxClikWidget(Widget);
		Use.AddEventListener('CLIK_Press',UseItem);
		break;

		case ('slot0'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    slot0 = GFxClikWidget(Widget);
		slot0.AddEventListener('CLIK_Press',SetSlot0);
		break;

		case ('slot1'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    slot1 = GFxClikWidget(Widget);
		slot1.AddEventListener('CLIK_Press',SetSlot1);
		break;

		case ('slot2'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
	    slot2 = GFxClikWidget(Widget);
		slot2.AddEventListener('CLIK_Press',SetSlot2);
		break;
		
		
		
        default:
		
            break;
    }
    PopulateInventory(InventoryList);

    return true;
}

function UseItem(GFxClikWidget.EventData ev)
{
PC.UseItem(PC.char.inventory[Index]);
}

function DropItem(GFxClikWidget.EventData ev)
{
	PC.DropItem(CurrentItem);
}

function SetSlot0(GFxClikWidget.EventData ev)
{
	local class<tfpWeapon> WeaponClass; // class of the weapon
	if(PC.char.inventory[Index].Weapon == true)
	{
		tfpPawn(PC.Pawn).EquipWeapon(WeaponClass);
	}

}

function SetSlot1(GFxClikWidget.EventData ev)
{

}

function SetSlot2(GFxClikWidget.EventData ev)
{

}

function InvPressed(GFxClikWidget.EventData ev)
{
local SVPlayerReplicationInfo SVP;
Index = ev.index;

 if(PC.Role == Role_Authority) // server
 {
 CurrentItem = tfpPawn(PC.Pawn).pchar.inventory[Index];
 }
   else
   {
   CurrentItem = tfpPawn(PC.Pawn).inventory[Index];
   }

}






function PopulateInventory(GFxClikWidget Widget)
{
	local byte i;
    local GFxObject DataProvider;
    local GFxObject TempObj;

    pc = TFPplayercontroller(GetPC());
    DataProvider = CreateArray();

    if(PC.Role == Role_Authority) // server
    {
    switch(Widget)
    {

	case (InventoryList):
	    for (i = 0; i < tfpPawn(PC.Pawn).pchar.inventory.Length; i++)
	    {        
		TempObj = CreateObject("Object");
	    TempObj.SetString("label", tfpPawn(PC.Pawn).pchar.inventory[i].itemName); // this will be displayed in the list
		TempObj.SetString("itemicon", tfpPawn(PC.Pawn).pchar.inventory[i].itemIcon); // this will be displayed in the list
		//TempObj.SetString("icon", PC.char.inventory[i].itemIcon);
		DataProvider.SetElementObject(i, TempObj);
	    }
	    break;
	    default:
	    break;

    }  
    Widget.SetObject("dataProvider", DataProvider);  	
    }

    if(PC.Role < Role_Authority) // client 
    {
      switch(Widget)
      {
	    case (InventoryList):
	        for (i = 0; i < tfpPawn(PC.Pawn).itemsInInventory; i++)
	        {        
		    TempObj = CreateObject("Object");
	        TempObj.SetString("label", tfpPawn(PC.Pawn).inventory[i].itemName); // this will be displayed in the list
		    TempObj.SetString("itemicon", tfpPawn(PC.Pawn).inventory[i].itemIcon); // this will be displayed in the list
		    DataProvider.SetElementObject(i, TempObj);
	        }
	        break;
	        default:
	        break;

      } 
      Widget.SetObject("dataProvider", DataProvider);  	 
      }    


}




defaultproperties
{   
     WidgetBindings.Add((WidgetName="sl",WidgetClass=class'GFxClikWidget'))
	 WidgetBindings.Add((WidgetName="DropItem",WidgetClass=class'GFxClikWidget'))
	 WidgetBindings.Add((WidgetName="UseItem",WidgetClass=class'GFxClikWidget'))
	 WidgetBindings.Add((WidgetName="slot0",WidgetClass=class'GFxClikWidget'))
	 WidgetBindings.Add((WidgetName="slot1",WidgetClass=class'GFxClikWidget'))
	 WidgetBindings.Add((WidgetName="slot2",WidgetClass=class'GFxClikWidget'))



	 /** If TRUE, this movie player will be allowed to accept input events. Defaults to TRUE */
	bAllowInput = true;

	/** If TRUE, this movie player will be allowed to accept focus events. Defaults to TRUE */
	bAllowFocus = true;

	/** If TRUE, the game will pause while this scene is up */
	//bPauseGameWhileActive = true;

	/** If TRUE, only the LocalPlayerOwner's input can be directed here */
	bOnlyOwnerFocusable = true;

	/** If TRUE, this movie player will capture input */
	bCaptureInput = true;

	/** IF TRUE, this movie player will ignore mouse input */
	bIgnoreMouseInput = false;
	bDisplayWithHUDOFF = true;
	ButtonClick = SoundCue'SV_Sounds.MainMenuSounds.Menu_Btn_Cue'
	MovieInfo=SwfMovie'SURVIVORFlash.Inventory'
	
	
}