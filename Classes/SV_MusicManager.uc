class SV_MusicManager extends Info
	config(Game);

var float	MusicStartTime;			/** Time at which current track started playing */
var TFPplayerController PlayerOwner;	/** Owner of this MusicManager */

enum EMusicState
{
	MST_Ambient,
	MST_Tension,
	MST_Fight,
	MST_Neutral
};

var EMusicState CurrentState;		/** Current Music state (reflects which track is active). */

var AudioComponent CurrentTrack;	/** Track being ramped up, rather than faded out */
var AudioComponent TensionTrack;      /** random track tagged as tense that is played same for below*/
var AudioComponent AmbientTrack;
var AudioComponent FightTrack;

var bool AmbientPlaying,TensionPlaying,FightPlaying;

var int AmbientIndex,TensionIndex,FightIndex,CurrentIndex;

event PostBeginPlay()
{
	Super.PostBeginPlay();

	// start music on a short timer so we avoid the long initial tick that can make the music skip
	SetTimer(1.0, false, 'StartMusic');
	SetTimer(10, true ,'MusicTick');
}

function StartMusic()
{
	local SV_MapInfo UMI;
	local int RandIdx;

	UMI = SV_MapInfo(WorldInfo.GetMapInfo());
	// means the content folks have not set the map up with music yet
	if (UMI != None)
	{
	
		//CurrentTrack = MusicTracks[5];
//		LastBeat = 0;
		//CurrentState = MST_Ambient;
		//MusicStartTime = WorldInfo.TimeSeconds;
		RandIdx = Rand(UMI.TensionCues.Length);
		TensionTrack = CreateNewTrack(UMI.TensionCues[RandIdx]);
		TensionTrack.OnAudioFinished = TrackFinished;

		RandIdx = Rand(UMI.AmbientCues.Length);
		AmbientTrack = CreateNewTrack(UMI.AmbientCues[RandIdx]);
		AmbientTrack.OnAudioFinished = TrackFinished;

		RandIdx = Rand(UMI.FightCues.Length);
		FightTrack = CreateNewTrack(UMI.FightCues[RandIdx]);
		FightTrack.OnAudioFinished = TrackFinished;

    	PlayerOwner = TFPPlayerController(Owner);
    	CurrentState = MST_Neutral; // always start with no music at all


	}
}

/*
=========
FadeOtherTracks

Fades any track that is playing
============
*/

function FadeOutOtherTracks()
{
	if(AmbientPlaying)
	{
		AmbientTrack.FadeOut(4.0,0);
	}

	if(TensionPlaying)
	{
		 TensionTrack.FadeOut(4.0,0);
	}

	if(FightPlaying)
	{
		FightTrack.FadeOut(4.0,0);
	}

}

/*
====================
Track Finished
Called once a track has finished
playing
=============
*/

function TrackFinished(AudioComponent AC)
{
	if(AC == AmbientTrack)
	{
		AmbientPlaying = false;

	}

	   if(AC == TensionTrack)
	   {
	   	   TensionPlaying = false;

	   }

}

/* CreateNewTrack()
* Create a new AudioComponent to play MusicCue.
* @param MusicCue:  the sound cue to play
* @returns the new audio component
*/
function AudioComponent CreateNewTrack(SoundCue MusicCue)
{
	local AudioComponent AC;

	AC = CreateAudioComponent( MusicCue, false, true );

	// AC will be none if -nosound option used
	if ( AC != None )
	{
		AC.bAllowSpatialization = false;
		AC.bShouldRemainActiveIfDropped = true;
	}
	return AC;
}



/** ChangeTrack()
* @param NewState  New music state (track to ramp up).
*/
function ChangeTrack(EMusicState NewState)
{
	local SV_MapInfo UMI;
	local AudioComponent NewTrack;
	local int RandIdx;

	//`log( "MusicManager:  ChangeTrack: " $ NewState );

	if ( CurrentState == NewState )
	{
		//`log( "MusicManager:  ChangeTrack:  new and current state are the same" );
		return;
	}

	CurrentState = NewState;

	FadeOutOtherTracks(); // when we switch to a new track all the other ones
	                      // MUST fade out 

	UMI = SV_MapInfo(WorldInfo.GetMapInfo());
	// means the content folks have not set the map up with music yet
	if( UMI == none )
	{
		//`log( GetFuncName() @ "UMI is none" );
		return;
	}

	// select appropriate track
	Switch( NewState )
	{
		case MST_Ambient:
				if (AmbientTrack != None)
				{
					RandIdx = Rand(UMI.AmbientCues.Length);
					AmbientTrack = CreateNewTrack(UMI.AmbientCues[RandIdx]);
				    AmbientTrack.FadeIn(4.0,1.0);
				    AmbientPlaying = true;
				}
		
		
			break;

		 case MST_Tension:
				if (TensionTrack != None)
				{
					RandIdx = Rand(UMI.TensionCues.Length);
					TensionTrack = CreateNewTrack(UMI.TensionCues[RandIdx]);
					TensionTrack.FadeIn(4.0,1.0);
					TensionPlaying = true;
				}
			break;

		case MST_Fight:
				if (FightTrack != None)
				{

					RandIdx = Rand(UMI.FightCues.Length);
					FightTrack = CreateNewTrack(UMI.FightCues[RandIdx]);
					FightTrack.FadeIn(4.0,1.0);
					FightPlaying = true;
				}
			break;
	}
  
	MusicStartTime = WorldInfo.TimeSeconds;

}

function TriggerFightMusic()
{
	if(AmbientPlaying)
	{
		AmbientTrack.FadeOut(4.0,0.0);
	}

	if(TensionPlaying)
	{
		TensionTrack.FadeOut(4.0,0.0);
	}

	if(FightPlaying) // we are alredy ina  fight track
	{
		return;
	}
	   
	   else
	   {
		  ChangeTrack(MST_Fight);
	   }




}







function MusicTick()
{
local int MusicChance;
local Pawn P;
MusicChance = Rand(5);

	if(CurrentState == MST_Neutral)
	{
		if(MusicChance == 1)
		{
			ChangeTrack(MST_Ambient);
		}

	}

	if(CurrentState == MST_Fight) // if in fight state
	{
		`trace("If STATEMENT CALLED",`green);
		if(tfpPawn(PlayerOwner.Pawn).FightPawn != none) // and the pawn we are fighting is not none
		{
			`trace("if fightpawn not none",`green);
			if(tfpPawn(PlayerOwner.Pawn).FightPawn.Health <= 0) // If he is dead
			{

				ChangeTrack(MST_Neutral); // then we can switch to a normal track
				`trace("FIGHT ENDED",`red);
			}
		}

	}
	`trace("Chance was"$MusicChance,`red);

	  foreach WorldInfo.AllPawns(class'Pawn', P)
      {
           if (VSize(P.Location - PlayerOwner.Pawn.Location) < 500)
           {
              	if(P != PlayerOwner.Pawn && CurrentState != MST_Fight && P.Health >= 1) //skip our pawn
           	    {
           	    	`trace("Pawn Acquired is"$P,`red);
           	    	ChangeTrack(MST_Tension);

           	    }

           }
      }



}

DefaultProperties
{
	AmbientPlaying = false;
	TensionPlaying = false;
	FightPlaying = false;
}


