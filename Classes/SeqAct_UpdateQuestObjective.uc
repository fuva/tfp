class SeqAct_UpdateQuestObjective extends SequenceAction;
 
 var() string Quest; // Amount of XP this action will give
 var() int ObjectiveIndex;
 var() string ObjectiveName;
 var() bool UpdateStatus;
 var() int StatusNum;

event Activated()
{
local tfpPlayerController PC;

PC = tfpPlayerController(GetWorldInfo().GetALocalPlayerController());

 PC.Quests.UpdateObjective(Quest,ObjectiveIndex,ObjectiveName,UpdateStatus,StatusNum);

}


	
DefaultProperties
 {
 	// Name that will apear in the Kismet Editor
 	ObjName="Update Quest Objective"
 	ObjCategory="Quests"
 }