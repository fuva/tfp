
class QuestManager extends Actor;

var array<Quest> AllQuests;
var tfpPlayerController PC2;
//var string QuestName;

var bool bHasRemainingQuestTask;

function GetPCReference(tfpPlayerController PC)
{
PC2 = pc;
}


function Quest CreateQuest( string nameOfQuest,bool bBegin )
{
    local Quest q;
    q = new () class'Quest';
    q.QuestName = nameOfQuest;
	q.QuestManager = self;
	
	if(bBegin)
	{
		q.QuestBegin();

	}
  
	
    Register(q);

    return q;
		`log(AllQuests.length);
}



function Quest GetQuest( string nameOfQuest )
{
 local Quest q;
		 foreach AllQuests(q)
		 {
		 if(q.QuestName == nameOfQuest)
		 {
		  return q;
		 }
		 
		}

}

function AddQuestObjective(string Objective,int Index)
{
	local Quest q;
		 foreach AllQuests(q)
		 {
		 if(q.bIsActiveQuest)
		 {
		 q.AddObjective(Objective,Index);
		 //PC2.NotifyQuestObjective(Objective,Index);
		 }
	}
}





function PickedUpItem( string Item)
{
local Quest q;
		 foreach AllQuests(q)
		 {
		 if(q.bIsActiveQuest)
		 {
		 q.PickedUpItem(item);
		 }
	}
}

function AddRecolectionTask(string Qs,string Item, int Amount)
{
	local Quest q;
		 foreach AllQuests(q)
		 {
		 	q.AddRequiredItem(Item,Amount);
		 if(q.bIsActiveQuest && q.QuestName == qs)
		 {
		 q.AddRequiredItem(Item,Amount);
		 q.bHasItems = false;
		 }
	}
}



function AddExploreTask( string ZoneName,int ZoneIndex)
{
local Quest q;
		 foreach AllQuests(q)
		 {
		 if(q.bIsActiveQuest)
		 {
		q.AddExploreZone(ZoneName,ZoneIndex);
		 }
	}
}

function UpdateObjective( string Qst,int oIndex,string objName,bool bstatus,int statusn)
{
local Quest q;
local SVHUDWrapper HUD;
		 foreach AllQuests(q)
		 {
		 if(q.bIsActiveQuest)
		 {
		   
		   if(bstatus)
		   {
		   	HUD = SVHUDWrapper(PC2.MyHUD);
             HUD.QuestMovie.UpdateObjectiveStatus(oIndex,statusn);
		   }
		   else
		   {
		   	HUD = SVHUDWrapper(PC2.MyHUD);
		   	HUD.QuestMovie.UpdateObjective(oIndex,objName);
		   }
		 }
	}
}







function UpdateQuest( string Item)
{
local Quest q;
		 foreach AllQuests(q)
		 {
		 if(q.bIsActiveQuest)
		 {
		 //q.UpdateQuest();
		 }
	}
}

function UpdateQuestMarker(QuestLocDummy Marker)
{
local Quest q;
		 foreach AllQuests(q)
		 {
		 if(q.bIsActiveQuest)
		 {
		// q.UpdateQuestMarker();
		 }
	}
}






function EndQuest( string Quest)
{
local Quest q;
		 foreach AllQuests(q)
		 {
		 if(q.bIsActiveQuest)
		 {
		 q.bIsActiveQuest = false;
		 PC2.GiveXP(q.ExperienceReward);
		 }
	}
} 

function UpdateQuestObjective( string Objective)
{
local Quest q;
		 foreach AllQuests(q)
		 {
		 if(q.bIsActiveQuest)
		 {
		 q.AddObjective(Objective,0);
		 }
		 }
}




function QuestThink()
{
local Quest q;

		 foreach AllQuests(q)
		 {
		 if(q.bIsActiveQuest)
		 {
		
		// q.QuestThink();
		 }
	}


}


function Register(Quest ToAdd)
{
AllQuests.AddItem(ToAdd);
}


