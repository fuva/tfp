class SV_UseMarker extends Trigger
placeable;


simulated event Touch(actor Other, primitivecomponent Comp, Vector HitLoc, vector HitNorm)
{
local tfpPawn TouchPawn;
local tfpPlayerController PC;
 TouchPawn = tfpPawn(Other);

  if(TouchPawn != none)
  {
  PC = TouchPawn.GetPC();
  
   if(PC.IsInState('UsingStructure'))
   {
   	PC.GotoState('PlayerWalking');
   	TouchPawn.SetRotation(self.Rotation);
   }
 }
}

//}

defaultproperties
{

}