/**
 * Savegames Are Possible In The UDK. Well, sort of. An item in the inventory
 * of a SapituCharacter. This is an instance of a SapituItem, it contains fixed
 * values for certain properties. Everything that is constant for a inventory
 * item should be read through the SapituItem instance linked in this class.
 *
 * By Michiel 'elmuerte' Hendriks for Epic Games, Inc.
 *
 * You are free to use this example as you see fit, as long as you
 * properly attribute the origin.
 */
class SapituInventory extends Object config(Sapitu) perobjectconfig;

/**
 * Name of the SapituItem
 */
var config string ItemName;

/**
 * Quality of the item, between 0 and 100
 */
var config int quality;

/**
 * Actual level of this item
 */
var config int level;

/**
 * Actual required characterlevel of this item
 */
var config int characterLevel;

/**
 * Actual weight of this item
 */
var config int weight;

/**
 * Actual value of this item
 */
var config int value;

/**
 * The instatiated item
 */
var SapituItem item;
