class SvWeap_StoneSword extends MeleeWeaponBase;


DefaultProperties
{
	// Pickup staticmesh
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'SV_Machete.Mesh.SK_Machete'
	End Object

	HolsterIndex = 0;
	EquipIndex = 4;

	

	//FleshHitsound[0]=SoundCue'SV_Machete.Sounds.Machete_HitFlesh_I_Cue'
	//FleshHitsound[1]=SoundCue'SV_Machete.Sounds.Machete_HitFlesh_II_Cue'
	//FleshHitsound[2]=SoundCue'SV_Machete.Sounds.Machete_HitFlesh_III_Cue'
	//FleshHitsound[3]=SoundCue'SV_Machete.Sounds.Machete_HitFlesh_IV_Cue'
	
	//SwingSound[0]=SoundCue'SV_Sounds.Weapons.Swing_Air_I_Cue'

	Begin Object class=AnimNodeSequence Name=MeshSequenceA
	End Object
	WeaponName = "Stone Sword"
	WeaponFireTypes(0)=EWFT_InstantHit

	PlayerAnimSets(0)=AnimSet'CH_TFP_Male.Anims.Anim_Medium'
	InstantHitDamageTypes(0)=class'SVDmgType_Sharp'

	AttachmentClass=class'SVAttachment_StoneSword'
	WeaponDurability = 100;
	InventoryGroup = 1;
	NumWeaponTraces=14

}