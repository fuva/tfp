class GFxBuilding extends GFxMoviePlayer;

var tfpPlayerController PC;
var array<GFxObject> BuildingIcons;
var GFxClikWidget DownButton, UpButton,BDButton1,BDButton2,BDButton3;
var array<GFxObject> BuildingNames;

var int Index;
var int sindex;

var SoundCue ButtonClick,ButtonBack;

var bool IsClientOwner; // simple bool to see if this movie is open for the client or the server


function Init(optional LocalPlayer player)
{	



	super.Init(player);
    Start();
	Advance(0);
		PC = tfpplayercontroller(GetPC());
		
	BuildingIcons[0] = GetVariableObject("_root.BDIcon1");
	BuildingIcons[1] = GetVariableObject("_root.BDIcon2");
	BuildingIcons[2] = GetVariableObject("_root.BDIcon3");
	
	BuildingNames[0] = GetVariableObject("_root.BDName1");
	BuildingNames[1] = GetVariableObject("_root.BDName2");
	BuildingNames[2] = GetVariableObject("_root.BDName3");
	CheckPopulate();
	




}


event bool WidgetInitialized(name WidgetName, name WidgetPath, GFxObject Widget)
{ 

		//CheckPopulate(); 
switch(WidgetName)
{           


	case ('UpButton'):
	UpButton = GFxClikWidget(Widget);
	UpButton.AddEventListener('CLIK_Press',UpButtonClick);
	break;
	
	case ('DownButton'):
	DownButton = GFxClikWidget(Widget);
	DownButton.AddEventListener('CLIK_Press',DownButtonClick);
	break;
	
		case ('BDButton1'):
	BDButton1 = GFxClikWidget(Widget);
	BDButton1.AddEventListener('CLIK_Press',ButtonPress1);
	break;
	
			case ('BDButton2'):
	BDButton2 = GFxClikWidget(Widget);
	BDButton2.AddEventListener('CLIK_Press',ButtonPress2);
	break;
	
			case ('BDButton3'):
	BDButton3 = GFxClikWidget(Widget);
	BDButton3.AddEventListener('CLIK_Press',ButtonPress3);
	break;


	
	
	

	
}
return true;
}

function CheckPopulate()
{
	local svplayerreplicationinfo svp;
	local int i;

  
	PC = tfpplayercontroller(GetPC());
	svp = svplayerreplicationinfo(pc.playerreplicationinfo);
	if(svp.char.charactername != "")
	{

	`log("populated structure in the client");

    for (i = 0; i < svp.char.StructureList.Length; ++i)	
    {
    BuildingIcons[i].SetString("source",svp.char.StructureList[i].Icon);
    BuildingNames[i].SetText(svp.char.StructureList[i].BuildName);
    }
		//PopulateStructuresClient();
		  PC.SpawnBuild(svp.char.StructureList[0]);


	}
	else
	{
		PopulateStructuresServer();
	}


}
	
	
event bool FilterButtonInput(int ControllerId, name ButtonName, EInputEvent InputEvent)
{

  if(ButtonName == 'MouseWheelUp')
  {
    IncreaseIndex();
    return true;
  }

   if(ButtonName == 'MouseWheelDown')
   {
    DecreaseIndex();
    return true;
   }
   return false;
}

function IncreaseIndex()
{


}


function DecreaseIndex()
{

}
  

function PopulateStructuresServer()
{

local int i;
	`log("populated structure in the serva");
    for (i = 0; i < PC.char.StructureList.Length; ++i)	
    {
    BuildingIcons[i].SetString("source",pc.char.StructureList[i].Icon);
    BuildingNames[i].SetText(pc.char.StructureList[i].BuildName);
    }
    PC.SpawnBuild(pc.char.StructureList[0]);
    IsClientOwner = false;

}
simulated  function PopulateStructuresClient()
{

	local int i;
		local svplayerreplicationinfo svp;
		pc = TFPplayercontroller(GetPC());
	svp = svplayerreplicationinfo(pc.playerreplicationinfo);

	`log("populated structure in the client");

    for (i = 0; i < svp.char.StructureList.Length; ++i)	
    {
    BuildingIcons[i].SetString("source",svp.char.StructureList[i].Icon);
    BuildingNames[i].SetText(svp.char.StructureList[i].BuildName);
    }
    PC.SpawnBuild(pc.char.StructureList[0]);

}


function DownButtonClick(GFxClikWidget.EventData ev)
{
Index++;
sindex = ev.index;

PC.PlaySound(ButtonClick);

}

function UpButtonClick(GFxClikWidget.EventData ev)
{
Index--;

PC.PlaySound(ButtonClick);
`log("slot 1 called");
}

function ButtonPress1(GFxClikWidget.EventData ev);
function ButtonPress2(GFxClikWidget.EventData ev);
function ButtonPress3(GFxClikWidget.EventData ev);






defaultproperties
{  
  
	    bCaptureInput = FALSE;
	    WidgetBindings.Add((WidgetName="UpButton",WidgetClass=class'GFxClikWidget'))	
		WidgetBindings.Add((WidgetName="DownButton",WidgetClass=class'GFxClikWidget'))	
		WidgetBindings.Add((WidgetName="BDButton1",WidgetClass=class'GFxClikWidget'))	
		WidgetBindings.Add((WidgetName="BDButton2",WidgetClass=class'GFxClikWidget'))	
		WidgetBindings.Add((WidgetName="BDButton3",WidgetClass=class'GFxClikWidget'))	
		WidgetBindings.Add((WidgetName="BDName1",WidgetClass=class'GFxClikWidget'))
		WidgetBindings.Add((WidgetName="BDName2",WidgetClass=class'GFxClikWidget'))
		WidgetBindings.Add((WidgetName="BDName3",WidgetClass=class'GFxClikWidget'))
		ButtonClick = SoundCue'SV_Sounds.MainMenuSounds.Menu_Btn_Cue'
		MovieInfo=SwfMovie'SURVIVORFlash.BuildingUI'

	

}