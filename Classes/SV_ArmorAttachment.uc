class SV_ArmorAttachment extends KASSetSpawnable;


var SV_ArmorItem inventoryArmor; // 

simulated function AttachTo(Pawn OwnerPawn)
{
	if (OwnerPawn.Mesh != None)
	{
		if ( SkeletalMeshComponent != None )
		{
			// Weapon Mesh Shadow
			SetMeshAndPhysAsset(InventoryArmor.Model,inventoryArmor.PhysAsset);
			SkeletalMeshComponent.SetShadowParent(OwnerPawn.Mesh);

			OwnerPawn.Mesh.AttachComponent(SkeletalMeshComponent, inventoryArmor.AttachmentSocket);
			SkeletalMeshComponent.SetAnimTreeTemplate(OwnerPawn.Mesh.AnimTreeTemplate);
			SkeletalMeshComponent.AnimSets = OwnerPawn.Mesh.AnimSets;
			SkeletalMeshComponent.SetParentAnimComponent(OwnerPawn.Mesh);
			SkeletalMeshComponent.SetLightEnvironment(tfpPawn(OwnerPawn).LightEnvironment);
		}
	}
	else
	{
		return;
	}
}

/**
 * Detach weapon from skeletal mesh
 */
simulated function DetachFrom( SkeletalMeshComponent MeshCpnt )
{
	// Weapon Mesh Shadow
	if ( SkeletalMeshComponent != None )
	{
		SkeletalMeshComponent.SetShadowParent(None);
		SkeletalMeshComponent.SetLightEnvironment(None);
	}
	if ( MeshCpnt != None )
	{
		// detach weapon mesh from player skelmesh
		if ( SkeletalMeshComponent != None )
		{
			MeshCpnt.DetachComponent( SkeletalMeshComponent );
		}
	}
}


DefaultProperties
{
	//defaults
		
    
}

