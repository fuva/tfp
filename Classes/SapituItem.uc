/**
 * Savegames Are Possible In The UDK. Well, sort of. Database entry for
 * world items. These are referenced by SapituInventory instances. These
 * items are just base configurations, when an SapituInventory item is created
 * it will use the information in this class to produce an actual instance.
 *
 * By Michiel 'elmuerte' Hendriks for Epic Games, Inc.
 *
 * You are free to use this example as you see fit, as long as you
 * properly attribute the origin.
 */
class SapituItem extends UTUIResourceDataProvider perobjectconfig;

/**
 * Defines a range of values.
 */
struct MinMax
{
	var int min;
	var int max;
};

/**
 * The input for the formula to calculate the value based on the given level
 * and a random value. The maximum value would be:
 * 	<code>base + level*levelMult + randMult</code>
 */
struct ValueFormula
{
	var int base;
	var float levelMult;
	var int randMult;
};

/**
 * The name of this item to show ingame.
 */
var config string DisplayName;

/**
 * The description of the item
 */
var config string Description;

/**
 * Name of the texture to use for the icon
 */
var config string Icon;

/**
 * The level range this item can have.
 */
var config MinMax Level;

/**
 * The level the character has to have to equip this item.
 */
var config MinMax haracterLevel;

/**
 * The weight of this item
 */
var config ValueFormula Weight;

/**
 * The gold value of this item
 */
var config ValueFormula Value;


defaultproperties
{
	bSearchAllInis=true
}
