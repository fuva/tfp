
class SV_CuteableTree extends KActor placeable
ClassGroup(InteractivePlaceableObjects);


var() int HitsTillFall;// how many hits will the tree take until it falls down?, 
var () int HeightOfTree;
var () int StoredWood;
var int WoodSpawnChance;
var bool HasFall;
var bool HasValue;


simulated event PostBeginPlay()
{
SetPhysics(PHYS_NONE);
super.postbeginplay();
}



event TakeDamage (int DamageAmount, Controller EventInstigator, Object.Vector HitLocation, Object.Vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{

local Vector NewHitLocation;
local vector ImpulseToAdd;
local SV_ItemArchetype WoodToSpawn;
local SV_ItemArchetype WoodItem;
local MeleeWeaponBase HitWeapon;


HitsTillFall--;



if(HasFall)
{
    
     WoodItem = tfpGame(WorldInfo.Game).DB.GetItem("WoodChunk");
     WoodToSpawn = Spawn(class'SV_ItemArchetype',self,,HitLocation,,WoodItem);
     WoodToSpawn.SetPhysics(PHYS_RIGIDBODY);
}



if(HitsTillFall == 0)
{



NewHitLocation = HitLocation * vect(1,1,0) + Location * vect(0,0,1) + HeightOfTree * vect(0,0,1);
ImpulseToAdd = vector(EventInstigator.Pawn.Rotation);//

//addimpulse IMPULSE AND THEN LOCATION
 SetPhysics(PHYS_RigidBody);
 //ImpulseToAdd.Z=0;
 ImpulseToAdd.X = 0.2;
 ImpulseToAdd.Y = 0.2;
 StaticMeshComponent.SetRBAngularVelocity(ImpulseToAdd,true);

 //DrawDebugSphere(HitLocation,100,4,255,0,0,true);
  DrawDebugSphere(NewHitLocation,100,4,255,0,0,true);
  DrawDebugSphere(ImpulseToAdd,100,10,255,0,0,true);
  DrawDebugLine(Location, ImpulseToAdd,255,0,0,true);
  `log("iMPULSE TO ADD IS" $ImpulseToAdd);
  //DrawDebugSphere(Location,100,4,255,255,0,);
 `Log("taking damageeeeee");
 HasFall = true;
  }
}




event Tick(float DeltaTime)
{
  
}


defaultproperties
{
 

    bCanBeDamaged = true
    bBlockActors=true
    bCollideWorld=true
    bHidden=false
    bCollideActors=true
    bStatic=false
    bWakeOnLevelStart=true
    bDamageAppliesImpulse=true
    HasFall == false;
}
