class GFxSkillTree extends GFxMoviePlayer;

var GFxObject SkillTF;
var array<SV_ItemSlot> Skills;
var tfpplayercontroller pc;




function Init(optional LocalPlayer player)
{
	 super.Init(player);
    Start();
    Advance(0);
    PC = tfpplayercontroller(GetPC());
    InitSkills();




}

function InitSkills()
{
  local int i; 
   
      for (i = 0; i < PC.char.skills.Length; i++)
      { 
       Skills[i].SetSlotData("Skill",pc.char.SKILLS[i].assetName,pc.char.skills[i].skillName);
      }

    
}


event bool WidgetInitialized(name WidgetName, name WidgetPath, GFxObject Widget)
{

   switch(WidgetName)
   {  
	    case ('slot0'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
     Skills[0] = SV_ItemSlot(Widget);
     Skills[0].AddeventListener('CLIK_press',PressedSlot);
    break;

        case ('slot1'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
     Skills[1] = SV_ItemSlot(Widget);
     Skills[1].AddeventListener('CLIK_press',PressedSlot);
    break;

        case ('slot2'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
     Skills[2] = SV_ItemSlot(Widget);
     Skills[2].AddeventListener('CLIK_press',PressedSlot);
    break;

        case ('slot3'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
     Skills[3] = SV_ItemSlot(Widget);
     Skills[3].AddeventListener('CLIK_press',PressedSlot);
    break;

        case ('slot4'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
     Skills[4] = SV_ItemSlot(Widget);
     Skills[4].AddeventListener('CLIK_press',PressedSlot);
    break;

        case ('slot5'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
     Skills[5] = SV_ItemSlot(Widget);
     Skills[5].AddeventListener('CLIK_press',PressedSlot);
    break;

        case ('slot6'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
     Skills[6] = SV_ItemSlot(Widget);
     Skills[6].AddeventListener('CLIK_press',PressedSlot);
    break;

        case ('slot7'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
     Skills[7] = SV_ItemSlot(Widget);
     Skills[7].AddeventListener('CLIK_press',PressedSlot);
    break;

        case ('slot8'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
     Skills[8] = SV_ItemSlot(Widget);
     Skills[8].AddeventListener('CLIK_press',PressedSlot);
    break;

        case ('slot9'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
     Skills[9] = SV_ItemSlot(Widget);
     Skills[9].AddeventListener('CLIK_press',PressedSlot);
    break;

        case ('slot10'): // this assumes your Scrolling List has an instance name of 'sl' in Flash.
     Skills[10] = SV_ItemSlot(Widget);
     Skills[10].AddeventListener('CLIK_press',PressedSlot);
    break;

      
      }
   return true;
 
}

function PressedSlot(gfxclikwidget.eventdata ev)
{
	  local GFxObject Data;
  local string Type;
    local int i; 
   
 

  //Index = ev.index;

  Data = ev.target.GetObject("data");
  Type = Data.getstring("Item");

       for (i = 0; i < PC.char.skills.Length; i++)
       { 

         if(pc.char.skills[i].skillName == Type)
         {
         	SkillTF.SetText(pc.char.skills[i].skillDesc);
         	break;

         }
      }

	
}



DefaultProperties
{
	//defaults
	bCaptureInput = false;
	MovieInfo=SwfMovie'SURVIVORFlash.SkillTree';

	WidgetBindings.Add((WidgetName="slot1",WidgetClass=class'SV_ItemSlot'))
	WidgetBindings.Add((WidgetName="slot2",WidgetClass=class'SV_ItemSlot'))
	WidgetBindings.Add((WidgetName="slot3",WidgetClass=class'SV_ItemSlot'))
	WidgetBindings.Add((WidgetName="slot4",WidgetClass=class'SV_ItemSlot'))
	WidgetBindings.Add((WidgetName="slot5",WidgetClass=class'SV_ItemSlot'))
	WidgetBindings.Add((WidgetName="slot6",WidgetClass=class'SV_ItemSlot'))
	WidgetBindings.Add((WidgetName="slot7",WidgetClass=class'SV_ItemSlot'))
	WidgetBindings.Add((WidgetName="slot8",WidgetClass=class'SV_ItemSlot'))
	WidgetBindings.Add((WidgetName="slot9",WidgetClass=class'SV_ItemSlot'))

    
}

